package mypackage;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.component.LabelField;

public class Custom_HeaderField extends Manager {
	private Custom_LabelField title;
	private int fontsize;

	Custom_HeaderField(String text) {
		super(Manager.USE_ALL_WIDTH | Manager.NO_VERTICAL_SCROLL
				| Manager.NO_HORIZONTAL_SCROLL);
		if (Display.getWidth() > 480)
			fontsize = 40;
		else if (Display.getWidth() < 481 && Display.getWidth() > 320)
			fontsize = 30;
		else
			fontsize = 20;
		setBackground(Config_GlobalFunction.loadbackground(Display.getWidth()
				+ "_" + "footer_bar.png"));
		title = new Custom_LabelField(text, DrawStyle.ELLIPSIS
				| LabelField.USE_ALL_WIDTH | DrawStyle.LEFT);
		title.setFont(Font.getDefault().derive(Font.BOLD, fontsize));
		title.setFontColor(Color.WHITE);
		add(title);
	}

	protected void sublayout(int width, int height) {
		Field field = getField(0);
		layoutChild(field, 150, Font.getDefault().getHeight());
		setPositionChild(field, 5, (getPreferredHeight() - getField(0)
				.getHeight()) / 2);

		width = Math.min(width, getPreferredWidth());
		height = Math.min(height, getPreferredHeight());
		setExtent(width, height);
	}

	public int getPreferredHeight() {
		return getField(0).getHeight() + 6;
	}

	public int getPreferredWidth() {
		return Display.getWidth();
	}

	protected void paint(Graphics graphics) {
		int rectHeight = getPreferredHeight();
		int rectWidth = getPreferredWidth();
		graphics.drawRect(0, 0, rectWidth, rectHeight);
		super.paint(graphics);
	}
}
