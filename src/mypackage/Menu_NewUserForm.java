package mypackage;

import java.util.Vector;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.XYEdges;
import net.rim.device.api.ui.component.BasicEditField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.CheckboxField;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.Menu;
import net.rim.device.api.ui.component.SeparatorField;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;
import net.rim.device.api.ui.decor.Border;
import net.rim.device.api.ui.decor.BorderFactory;

public class Menu_NewUserForm extends MainScreen 
{
	private Database_Webservice webservice;
	private int fonttitle, fontinitial;
	private Custom_ButtonField interested;
	private int mCheckBoxesCount = Config_GlobalFunction.interestedtext.length;
	private boolean ischeck[] = new boolean[mCheckBoxesCount];

	public Menu_NewUserForm() {
		super(USE_ALL_WIDTH | NO_SYSTEM_MENU_ITEMS);

		for (int i = 0; i < ischeck.length; i++)
			ischeck[i] = false;

		if (Display.getWidth() > 480) {
			fonttitle = 43;
			fontinitial = 25;
		} else if (Display.getWidth() == 480) {
			fonttitle = 33;
			fontinitial = 20;
		} else if (Display.getWidth() < 480 && Display.getWidth() >= 320) {
			fonttitle = 23;
			fontinitial = 15;
		} else {
			fonttitle = 20;
			fontinitial = 10;
		}
		webservice = new Database_Webservice();
		add(new Custom_FormTopField());
		add(new Custom_FormTable());
	}

	public boolean onClose() {
		if (Config_GlobalFunction.Dialog(Config_GlobalFunction.alertmsgleave)) {
			System.exit(0);
			return true;
		} else
			return false;
	}

	protected void makeMenu(Menu menu, int instance) {

	}

	public class Custom_FormTopField extends Manager {
		private Custom_LabelField title;

		Custom_FormTopField() {
			super(Manager.USE_ALL_WIDTH | Manager.NO_VERTICAL_SCROLL
					| Manager.NO_HORIZONTAL_SCROLL);
			setBackground(Config_GlobalFunction.loadbackground(Display
					.getWidth() + "_" + "header_bar.png"));
			title = new Custom_LabelField(Config_GlobalFunction.formtitle,
					DrawStyle.ELLIPSIS | LabelField.USE_ALL_WIDTH
							| DrawStyle.LEFT | DrawStyle.VCENTER, Color.WHITE);
			title.setFont(Font.getDefault().derive(Font.BOLD, fonttitle));
			add(title);
		}

		protected void sublayout(int width, int height) {
			Field field = getField(0);
			layoutChild(field, getPreferredWidth() / 3,
					getPreferredHeight() - 10);
			setPositionChild(field, 5, (getPreferredHeight() - getField(0)
					.getHeight()) / 2);

			width = Math.min(width, getPreferredWidth());
			height = Math.min(height, getPreferredHeight());
			setExtent(width, height);
		}

		public int getPreferredHeight() {
			return Config_GlobalFunction.Bitmap("header_bar.png").getHeight();
		}

		public int getPreferredWidth() {
			return Display.getWidth();
		}

		public void paint(Graphics graphics) {
			int rectHeight = getPreferredHeight();
			int rectWidth = getPreferredWidth();

			graphics.drawRect(0, 0, rectWidth, rectHeight);
			super.paint(graphics);
		}
	}

	public class Custom_FormTable extends Manager {
		private LabelField namelabel, emaillabel, agelabel, genderlabel,
				joblabel, salarylabel, interestedlabel;
		private Custom_EditField name, email;
		private Custom_DropDownField age, gender, job, salary;
		private ButtonField submitbtn;
		private int initial = 0;
		private int labelwidth = getPreferredWidth() * 1 / 4,
				editwidth = getPreferredWidth() * 2 / 3;

		private Bitmap image = Config_GlobalFunction
				.Bitmap("dropdown-box-1.png");
		private Bitmap image_hover = Config_GlobalFunction
				.Bitmap("dropdown-box-1_active.png");

		public Custom_FormTable() {
			super(Manager.USE_ALL_WIDTH | Manager.VERTICAL_SCROLL);
			setBackground(Config_GlobalFunction
					.loadbackground("background.png"));

			namelabel = new LabelField(Config_GlobalFunction.namelabel,
					DrawStyle.ELLIPSIS | LabelField.USE_ALL_WIDTH
							| DrawStyle.RIGHT);
			add(namelabel);

			emaillabel = new LabelField(Config_GlobalFunction.emaillabel,
					DrawStyle.ELLIPSIS | LabelField.USE_ALL_WIDTH
							| DrawStyle.RIGHT);
			add(emaillabel);

			agelabel = new LabelField(Config_GlobalFunction.agelabel,
					DrawStyle.ELLIPSIS | LabelField.USE_ALL_WIDTH
							| DrawStyle.RIGHT);
			add(agelabel);

			genderlabel = new LabelField(Config_GlobalFunction.genderlabel,
					DrawStyle.ELLIPSIS | LabelField.USE_ALL_WIDTH
							| DrawStyle.RIGHT);
			add(genderlabel);

			joblabel = new LabelField(Config_GlobalFunction.joblabel,
					DrawStyle.ELLIPSIS | LabelField.USE_ALL_WIDTH
							| DrawStyle.RIGHT);
			add(joblabel);

			salarylabel = new LabelField(Config_GlobalFunction.salarylabel,
					DrawStyle.ELLIPSIS | LabelField.USE_ALL_WIDTH
							| DrawStyle.RIGHT);
			add(salarylabel);

			interestedlabel = new LabelField(
					Config_GlobalFunction.interestedlabel, DrawStyle.ELLIPSIS
							| LabelField.USE_ALL_WIDTH | DrawStyle.RIGHT);
			add(interestedlabel);

			name = new Custom_EditField(Field.FIELD_HCENTER
					| Field.FIELD_VCENTER | Field.FOCUSABLE
					| EditField.NO_NEWLINE, editwidth, 1);
			add(name);

			email = new Custom_EditField(Field.FIELD_HCENTER
					| Field.FIELD_VCENTER | Field.FOCUSABLE
					| EditField.NO_NEWLINE | BasicEditField.FILTER_EMAIL,
					editwidth, 1);
			add(email);

			age = new Custom_DropDownField("", Config_GlobalFunction.agetext,
					initial, Field.FOCUSABLE);
			add(age);

			gender = new Custom_DropDownField("",
					Config_GlobalFunction.gendertext, initial, Field.FOCUSABLE);
			add(gender);

			job = new Custom_DropDownField("", Config_GlobalFunction.jobtext,
					initial, Field.FOCUSABLE);
			add(job);

			salary = new Custom_DropDownField("",
					Config_GlobalFunction.salarytext, initial, Field.FOCUSABLE);
			add(salary);

			interested = new Custom_ButtonField(
					Config_GlobalFunction.interestedclicklabel, image,
					image_hover, image_hover, Color.BLACK, Field.FOCUSABLE
							| DrawStyle.LEFT | Field.FIELD_LEFT
							| ButtonField.CONSUME_CLICK) {
				protected void onUnfocus() {
					super.onUnfocus();
					interested.setColor(Color.BLACK);
					invalidate();
				}
			};
			interested.setChangeListener(new FieldChangeListener() {
				public void fieldChanged(Field field, int context) {
					Main.getUiApplication().pushScreen(new Interested());
				}
			});
			add(interested);

			submitbtn = new ButtonField(Config_GlobalFunction.submit,
					Field.FOCUSABLE | Field.FIELD_HCENTER | DrawStyle.VCENTER
							| DrawStyle.HCENTER | ButtonField.CONSUME_CLICK) {
				public int getPreferredWidth() {
					return image.getWidth() / 2;
				}
			};
			submitbtn.setChangeListener(new FieldChangeListener(){
				public void fieldChanged(Field field, int context) {
					if (Config_GlobalFunction.isConnected()) {
						if (!name.getText().trim().toString().equals("")) {
							if (!email.getText().trim().toString().equals("")) {
								if (!interested
										.getText()
										.equals(Config_GlobalFunction.interestedclicklabel)) {
									String names = name.getText();
									String emails = email.getText();
									String ages = Config_GlobalFunction.agetext[age
											.getSelectedIndex()];
									String genders = Config_GlobalFunction.gendertext[gender
											.getSelectedIndex()];
									String jobs = Config_GlobalFunction.jobtext[job
											.getSelectedIndex()];
									String salaries = Config_GlobalFunction.salarytext[salary
											.getSelectedIndex()];
									String interesteds = interested.getText()
											.toString();

									Config_GlobalFunction.Message(
											Config_GlobalFunction.surveysubmit,
											1);
									
									webservice.InsertSurvey(names, emails,
											ages, genders, jobs, salaries,
											interesteds);
									
									webservice.InsertFormSubmit(1, 1, 1, 1, 1);
									
									webservice.insertFontSize(fontinitial);
									
									Main.getUiApplication().invokeLater(
											new Runnable() {
												public void run() {
													Main.getUiApplication()
															.pushScreen(
																	new Main_AllLatestNews(
																			true));
													Main.getUiApplication()
															.popScreen(
																	Menu_NewUserForm.this);
												}
											}, 1 * 1000, false);
								} else
									Config_GlobalFunction.Message(
											Config_GlobalFunction.nointerested,
											1);
							} else
								Config_GlobalFunction.Message(
										Config_GlobalFunction.noemail, 1);
						} else
							Config_GlobalFunction.Message(
									Config_GlobalFunction.noname, 1);
					} else
						Config_GlobalFunction.Message(
								Config_GlobalFunction.nosubmit, 1);
				}				
			});
			add(submitbtn);
		}

		protected void sublayout(int width, int height) {
			Field field = getField(0);
			layoutChild(field, labelwidth, Font.getDefault().getHeight());
			setPositionChild(field, 5, 30);

			field = getField(1);
			layoutChild(field, labelwidth, Font.getDefault().getHeight());
			setPositionChild(field, 5, (image.getHeight() + 10) + 30);

			field = getField(2);
			layoutChild(field, labelwidth, Font.getDefault().getHeight());
			setPositionChild(field, 5, (image.getHeight() + 10) * 2 + 30);

			field = getField(3);
			layoutChild(field, labelwidth, Font.getDefault().getHeight());
			setPositionChild(field, 5, (image.getHeight() + 10) * 3 + 30);

			field = getField(4);
			layoutChild(field, labelwidth, Font.getDefault().getHeight());
			setPositionChild(field, 5, (image.getHeight() + 10) * 4 + 30);

			field = getField(5);
			layoutChild(field, labelwidth, Font.getDefault().getHeight());
			setPositionChild(field, 5, (image.getHeight() + 10) * 5 + 30);

			field = getField(6);
			layoutChild(field, labelwidth, Font.getDefault().getHeight());
			setPositionChild(field, 5, (image.getHeight() + 10) * 6 + 30);

			field = getField(7);
			layoutChild(field, editwidth, Font.getDefault().getHeight());
			setPositionChild(field, labelwidth + 2, 30);

			field = getField(8);
			layoutChild(field, editwidth, Font.getDefault().getHeight());
			setPositionChild(field, labelwidth + 2,
					(image.getHeight() + 10) + 30);

			field = getField(9);
			layoutChild(field, editwidth + 2, Font.getDefault().getHeight());
			setPositionChild(field, labelwidth + 2,
					(image.getHeight() + 10) * 2 + 30);

			field = getField(10);
			layoutChild(field, editwidth + 2, Font.getDefault().getHeight());
			setPositionChild(field, labelwidth + 2,
					(image.getHeight() + 10) * 3 + 30);

			field = getField(11);
			layoutChild(field, editwidth + 2, image.getHeight());
			setPositionChild(field, labelwidth + 2,
					(image.getHeight() + 10) * 4 + 30);

			field = getField(12);
			layoutChild(field, editwidth + 2, image.getHeight());
			setPositionChild(field, labelwidth + 2,
					(image.getHeight() + 10) * 5 + 30);

			field = getField(13);
			layoutChild(field, editwidth + 2, image.getHeight());
			setPositionChild(field, labelwidth + 2,
					(image.getHeight() + 10) * 6 + 30);

			field = getField(14);
			layoutChild(field, image.getWidth() / 2, Font.getDefault()
					.getHeight() + 15);
			setPositionChild(field, labelwidth + field.getWidth() + 2,
					(image.getHeight() + 10) * 5
							+ (getField(7).getHeight() + 10) * 2 + 30);

			width = Math.min(width, getPreferredWidth());
			height = Math.min(height, getPreferredHeight());
			setExtent(width, height);
		}

		public int getPreferredHeight() {
			return getField(7).getHeight() + getField(8).getHeight()
					+ getField(9).getHeight() + getField(10).getHeight()
					+ getField(11).getHeight() + getField(12).getHeight()
					+ getField(13).getHeight() + getField(14).getHeight() + 110;
		}

		public int getPreferredWidth() {
			return Display.getWidth();
		}

		protected void paint(Graphics graphics) {
			int rectHeight = getPreferredHeight();
			int rectWidth = getPreferredWidth();
			graphics.drawRect(0, 0, rectWidth, rectHeight);
			super.paint(graphics);
		}

		protected boolean touchEvent(TouchEvent event) {
			int eventCode = event.getEvent();
			if ((eventCode == TouchEvent.UNCLICK)
					|| (eventCode == TouchEvent.DOWN)) {
				int x = event.getX(1);
				int y = event.getY(1);

				if ((x >= 0) && (y >= 0) && (x < getWidth())
						&& (y < getHeight())) {
					int field = getFieldAtLocation(x, y);
					if (field >= 0) {
						return super.touchEvent(event);
					} else {
						if (eventCode == TouchEvent.UNCLICK) {
							Config_GlobalFunction.hideKeyboard();
						} else {
							setFocus();
						}
						return true;
					}
				}
			}
			return super.touchEvent(event);
		}
	}

	public class Interested extends PopupScreen {
		private Vector mCheckBoxes = new Vector();
		private CheckboxField cbf;

		public Interested() {
			super(new VerticalFieldManager(), NO_SYSTEM_MENU_ITEMS);
			setBackground(BackgroundFactory.createSolidTransparentBackground(
					0x000000, 230));
			setBorder(BorderFactory.createSimpleBorder(new XYEdges(),
					Border.STYLE_TRANSPARENT));

			CheckBoxListener mCBListener = new CheckBoxListener();

			add(new LabelField(Config_GlobalFunction.interestedmenulabel));
			add(new SeparatorField());

			for (int i = 0; i < mCheckBoxesCount; i++) {
				CheckboxField cb = new CheckboxField(
						Config_GlobalFunction.interestedtext[i], ischeck[i]);
				cb.setChangeListener(mCBListener);
				add(cb);
				mCheckBoxes.addElement(cb);
			}
		}

		class CheckBoxListener implements FieldChangeListener {
			boolean mProgrammatic = false;

			public void fieldChanged(Field field, int context) {
				if (!mProgrammatic) {
					mProgrammatic = true;
					CheckboxField cb = (CheckboxField) field;
					int index = mCheckBoxes.indexOf(cb);
					if (cb.getChecked()) {
						((CheckboxField) mCheckBoxes.elementAt(index))
								.setChecked(true);
						ischeck[index] = true;
					} else {
						((CheckboxField) mCheckBoxes.elementAt(index))
								.setChecked(false);
						ischeck[index] = false;
					}
					mProgrammatic = false;
				}
			}
		}

		private String getvalue() {
			String values = "";
			for (int i = 0; i < mCheckBoxesCount; i++) {
				cbf = (CheckboxField) mCheckBoxes.elementAt(i);
				if (cbf.getChecked()) {
					values += Config_GlobalFunction.interestedtext[i] + ", ";
				}
			}
			if (values.equals(""))
				values = Config_GlobalFunction.interestedclicklabel;
			else
				values = values.substring(0, values.length() - 2);
			return values;
		}

		public boolean keyDown(int keycode, int status) {
			if (Keypad.key(keycode) == Keypad.KEY_ESCAPE) {
				Main.getUiApplication().popScreen(this);
				interested.setText(getvalue());
				return true;
			}
			return super.keyDown(keycode, status);
		}
	}
}
