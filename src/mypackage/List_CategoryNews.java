package mypackage;

public class List_CategoryNews 
{
	int _NewsCatID;
	String _NewsCatNames;

	public List_CategoryNews() {
	}

	public List_CategoryNews(int NewCatID) {
		this._NewsCatID = NewCatID;
	}

	public List_CategoryNews(int NewsCatID, String NewsCatNames) {
		this._NewsCatID = NewsCatID;
		this._NewsCatNames = NewsCatNames;
	}

	public int getCatID() {
		return this._NewsCatID;
	}

	public void SetCatID(int NewsCatID) {
		this._NewsCatID = NewsCatID;
	}

	public String getNewCatName() {
		return this._NewsCatNames;
	}

	public void setNewsCatName(String NewsCategoryNames) {
		this._NewsCatNames = NewsCategoryNames;
	}
}
