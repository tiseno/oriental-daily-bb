package mypackage;

import java.util.Vector;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.ListField;
import net.rim.device.api.ui.component.ListFieldCallback;
import net.rim.device.api.ui.component.Menu;
import net.rim.device.api.ui.container.MainScreen;

public class Main_Comments extends MainScreen {
	private Database_Webservice webservice;
	private Custom_CommentsListField listfield;
	private Custom_ButtonField downloadbtn, refreshbtn, backbtn, newsbtn,
			otherbtn;
	private Custom_EditField comments, name;

	public Main_Comments(int newsid, Vector content) {
		super(USE_ALL_WIDTH | NO_SYSTEM_MENU_ITEMS);
		webservice = new Database_Webservice();
		add(new Custom_TopField(this, -1, -1, "", 2, 0));
		add(new Custom_CommentsBottom());
		add(new Custom_HeaderField(Config_GlobalFunction.comments));
		add(new Custom_CommentsField(newsid));

		listfield = new Custom_CommentsListField(content);
		add(listfield);
		if (content.size() != 0)
			listfield.setSize(content.size());
		else
			listfield.setSize(1);
	}

	protected void makeMenu(Menu menu, int instance) {

	}

	public class Custom_TopField extends Manager {
		private Bitmap download = Config_GlobalFunction
				.Bitmap("btn_download.png");
		private Bitmap downloadactive = Config_GlobalFunction
				.Bitmap("btn_download_active.png");
		private Bitmap refresh = Config_GlobalFunction
				.Bitmap("icon_refresh.png");
		private Bitmap refreshactive = Config_GlobalFunction
				.Bitmap("icon_refresh_active.png");
		private Bitmap back = Config_GlobalFunction.Bitmap("btn_back.png");
		private Bitmap backctive = Config_GlobalFunction
				.Bitmap("btn_back_active.png");
		private Bitmap news = Config_GlobalFunction.Bitmap("icon_news.png");
		private Bitmap newsactive = Config_GlobalFunction
				.Bitmap("icon_news_active.png");
		private Bitmap home = Config_GlobalFunction.Bitmap("icon_home.png");

		private BitmapField homeimage;
		private Custom_LabelField title;
		private int left, right, fontsize, position;
		private Database_Webservice webservice;

		Custom_TopField(final MainScreen mainscreen, final int position,
				final int catsid, final String header, int left, int right) {
			super(Manager.USE_ALL_WIDTH | Manager.NO_VERTICAL_SCROLL
					| Manager.NO_HORIZONTAL_SCROLL);
			this.left = left;
			this.right = right;
			this.position = position;

			if (Display.getWidth() > 480)
				fontsize = 43;
			else if (Display.getWidth() < 481 && Display.getWidth() > 320)
				fontsize = 33;
			else
				fontsize = 23;

			webservice = new Database_Webservice();
			setBackground(Config_GlobalFunction.loadbackground(Display
					.getWidth() + "_" + "header_bar.png"));

			if (left == 1) {
				newsbtn = new Custom_ButtonField(news, newsactive, newsactive);
				newsbtn.setChangeListener(new FieldChangeListener() {
					public void fieldChanged(Field field, int context) {
						Main.getUiApplication().pushScreen(
								new Menu_PopupMenu(position));
					}
				});
				add(newsbtn);
			} else if (left == 2) {
				backbtn = new Custom_ButtonField(back, backctive, backctive);
				backbtn.setChangeListener(new FieldChangeListener() {
					public void fieldChanged(Field field, int context) {
						Main.getUiApplication().popScreen(mainscreen);
					}
				});
				add(backbtn);
			}

			if (position != 0) {
				title = new Custom_LabelField(Config_GlobalFunction.maintitle,
						DrawStyle.ELLIPSIS | LabelField.USE_ALL_WIDTH
								| DrawStyle.VCENTER | Field.FOCUSABLE
								| DrawStyle.LEFT | ButtonField.CONSUME_CLICK,
						Color.WHITE) {
					protected boolean navigationClick(int status, int time) {
						Main.getUiApplication().pushScreen(
								new Custom_LoadingScreen(1));
						Main.getUiApplication().invokeLater(new Runnable() {
							public void run() {
								Main.getUiApplication().pushScreen(
										new Main_AllLatestNews());
							}
						}, 1 * 1000, false);
						return true;
					}
				};
			} else {
				title = new Custom_LabelField(Config_GlobalFunction.maintitle,
						DrawStyle.ELLIPSIS | LabelField.USE_ALL_WIDTH
								| DrawStyle.HCENTER | DrawStyle.VCENTER,
						Color.WHITE);
			}
			title.setFont(Font.getDefault().derive(Font.BOLD, fontsize));
			add(title);

			if (right == 1) {
				downloadbtn = new Custom_ButtonField(download, downloadactive,
						downloadactive);
				downloadbtn.setChangeListener(new FieldChangeListener() {
					public void fieldChanged(Field field, int context) {
						if (Config_GlobalFunction
								.Dialog(Config_GlobalFunction.alertdownload)) {
							if (Config_GlobalFunction.isConnected()) {
								Config_GlobalFunction.Message(
										Config_GlobalFunction.downloading, 10);
								Main.getUiApplication().invokeLater(
										new Runnable() {
											public void run() {
												webservice.UpdateAllCatNews(
														false, -1);
											}
										}, 1 * 1000, false);

							} else
								Config_GlobalFunction.Message(
										Config_GlobalFunction.nowifi, 1);
						} else
							Config_GlobalFunction.CloseDialog();
					}
				});
				add(downloadbtn);
			} else if (right == 2) {
				refreshbtn = new Custom_ButtonField(refresh, refreshactive,
						refreshactive);
				refreshbtn.setChangeListener(new FieldChangeListener() {
					public void fieldChanged(Field field, int context) {
						if (Config_GlobalFunction.isConnected()) {
							Config_GlobalFunction.Message(
									Config_GlobalFunction.refreshing, 10);
							Main.getUiApplication().invokeLater(new Runnable() {
								public void run() {
									webservice.refreshCatNewsindex(catsid,
											position, header);
								}
							});
						} else
							Config_GlobalFunction.Message(
									Config_GlobalFunction.nowifi, 1);
					}
				});
				add(refreshbtn);
			}

			if (position != 0) {
				homeimage = new BitmapField(home);
				add(homeimage);
			}
		}

		protected void sublayout(int width, int height) {
			int Height = (getPreferredHeight() - back.getHeight()) / 2;
			Field field = getField(0);

			if (left == 1) {
				layoutChild(field, back.getWidth(), back.getHeight());
				setPositionChild(field, 5, Height);
			} else if (left == 2) {
				layoutChild(field, news.getWidth(), news.getHeight());
				setPositionChild(field, 5, Height);
			}

			field = getField(1);
			if (position != 0) {
				layoutChild(field, getPreferredWidth() / 3,
						getPreferredHeight() - 10);
				setPositionChild(field, (getPreferredWidth()
						- getField(1).getWidth() + homeimage.getBitmapWidth())
						/ 2 + homeimage.getBitmapWidth(),
						(getPreferredHeight() - getField(1).getHeight()) / 2);

				field = getField(getFieldCount() - 1);
				layoutChild(field, homeimage.getBitmapWidth(),
						homeimage.getBitmapHeight());
				setPositionChild(
						field,
						(getPreferredWidth() - getField(1).getWidth() + homeimage
								.getBitmapWidth()) / 2,
						(getPreferredHeight() - homeimage.getBitmapHeight()) / 2);
			} else {
				layoutChild(field, getPreferredWidth() / 3,
						getPreferredHeight() - 10);
				setPositionChild(field, (getPreferredWidth() - getField(1)
						.getWidth()) / 2, (getPreferredHeight() - getField(1)
						.getHeight()) / 2);
			}

			field = getField(2);
			if (right == 1) {
				layoutChild(field, download.getWidth(), download.getHeight());
				setPositionChild(field,
						getPreferredWidth() - (download.getWidth() + 5), Height);
			} else if (right == 2) {
				layoutChild(field, refresh.getWidth(), refresh.getHeight());
				setPositionChild(field,
						getPreferredWidth() - (refresh.getWidth() + 5), Height);
			}

			width = Math.min(width, getPreferredWidth());
			height = Math.min(height, getPreferredHeight());
			setExtent(width, height);
		}

		public int getPreferredHeight() {
			return Config_GlobalFunction.Bitmap("header_bar.png").getHeight();
		}

		public int getPreferredWidth() {
			return Display.getWidth();
		}

		public void paint(Graphics graphics) {
			int rectHeight = getPreferredHeight();
			int rectWidth = getPreferredWidth();

			graphics.drawRect(0, 0, rectWidth, rectHeight);
			super.paint(graphics);
		}

		protected boolean navigationMovement(int dx, int dy, int status,
				int time) {
			int focusIndex = getFieldWithFocusIndex();

			while (dy > 0) {
				Field f = otherbtn;
				if (f.isFocusable()) {
					f.setFocus();
					dy--;
				}
			}

			while (dy < 0) {
				return false;
			}

			while (dx > 0) {
				focusIndex++;

				if (focusIndex >= getFieldCount()) {
					return false;
				} else {
					Field f = getField(focusIndex);

					if (f.isFocusable()) {
						f.setFocus();
						dx--;
					}
				}
			}

			while (dx < 0) {
				focusIndex--;

				if (focusIndex < 0) {
					return false;
				} else {
					Field f = getField(focusIndex);

					if (f.isFocusable()) {
						f.setFocus();
						dx++;
					}
				}
			}

			return true;
		}
	}

	public class Custom_CommentsBottom extends Manager {
		private Bitmap other = Config_GlobalFunction.Bitmap("icon_other.png");
		private Bitmap otheractive = Config_GlobalFunction
				.Bitmap("icon_other_active.png");

		Custom_CommentsBottom() {
			super(Manager.USE_ALL_WIDTH | Manager.NO_VERTICAL_SCROLL
					| Manager.NO_HORIZONTAL_SCROLL);
			setBackground(Config_GlobalFunction.loadbackground(Display
					.getWidth() + "_" + "footer_bar.png"));
			otherbtn = new Custom_ButtonField(other, otheractive, otheractive);
			otherbtn.setChangeListener(new FieldChangeListener() {
				public void fieldChanged(Field field, int context) {
					Main.getUiApplication().pushScreen(
							new Custom_LoadingScreen(1));
					Main.getUiApplication().invokeLater(new Runnable() {
						public void run() {
							Main.getUiApplication().pushScreen(
									new Menu_Others());
						}
					}, 1 * 1000, false);
				}
			});
			add(otherbtn);
		}

		protected void sublayout(int width, int height) {
			int Height = (getPreferredHeight() - other.getHeight()) / 2;

			Field field = getField(0);
			layoutChild(field, other.getWidth(), other.getHeight());
			setPositionChild(field, (getPreferredWidth() - getField(0)
					.getWidth()) / 2, Height);

			width = Math.min(width, getPreferredWidth());
			height = Math.min(height, getPreferredHeight());
			setExtent(width, height);
		}

		public int getPreferredHeight() {
			return Config_GlobalFunction.Bitmap("footer_bar.png").getHeight();
		}

		public int getPreferredWidth() {
			return Display.getWidth();
		}

		protected void paint(Graphics graphics) {
			int rectHeight = getPreferredHeight();
			int rectWidth = getPreferredWidth();
			graphics.drawRect(0, 0, rectWidth, rectHeight);
			super.paint(graphics);
		}

		protected boolean navigationMovement(int dx, int dy, int status,
				int time) {
			int focusIndex = getFieldWithFocusIndex();

			while (dy > 0) {
				Field f = comments;
				if (f.isFocusable()) {
					f.setFocus();
					dy--;
				}
			}

			while (dy < 0) {
				Field f = backbtn;
				if (f.isFocusable()) {
					f.setFocus();
					dy++;
				}
			}

			while (dx > 0) {
				focusIndex++;

				if (focusIndex >= getFieldCount()) {
					return false;
				} else {
					Field f = getField(focusIndex);

					if (f.isFocusable()) {
						f.setFocus();
						dx--;
					}
				}
			}

			while (dx < 0) {
				focusIndex--;

				if (focusIndex < 0) {
					return false;
				} else {
					Field f = getField(focusIndex);

					if (f.isFocusable()) {
						f.setFocus();
						dx++;
					}
				}
			}

			return true;
		}
	}

	public class Custom_CommentsField extends Manager {
		private Custom_LabelField namelabel;
		private ButtonField postbtn;

		Custom_CommentsField(final int newsid) {
			super(Manager.USE_ALL_WIDTH | Manager.NO_VERTICAL_SCROLL
					| Manager.NO_HORIZONTAL_SCROLL);

			setBackground(Config_GlobalFunction
					.loadbackground("background.png"));
			comments = new Custom_EditField(Field.FIELD_HCENTER
					| Field.FIELD_VCENTER | Field.FOCUSABLE,
					getPreferredWidth() - 10, 3);
			add(comments);

			namelabel = new Custom_LabelField(Config_GlobalFunction.namelabel,
					DrawStyle.ELLIPSIS | LabelField.USE_ALL_WIDTH
							| DrawStyle.RIGHT | Field.FIELD_LEFT);
			namelabel.setFontColor(Color.BLACK);
			add(namelabel);

			name = new Custom_EditField(Field.FIELD_HCENTER
					| Field.FIELD_VCENTER | Field.FOCUSABLE,
					getPreferredWidth() / 2, 1);
			add(name);

			postbtn = new ButtonField(Config_GlobalFunction.comments,
					DrawStyle.HCENTER | Field.FIELD_RIGHT) {
				public int getPreferredWidth() {
					return Display.getWidth() / 5;
				}

				public int getPreferredHeight() {
					return Font.getDefault().getHeight() + 5;
				}
			};
			postbtn.setChangeListener(new FieldChangeListener() {
				public void fieldChanged(Field field, int context) {
					if (Config_GlobalFunction.isConnected()) {
						if (!comments.getText().trim().toString().equals("")) {
							String commentnametext;
							if (name.getText().trim().toString().equals(""))
								commentnametext = Config_GlobalFunction.commentposter;
							else
								commentnametext = name.getText().toString();
							String commenttext = comments.getText().toString();
							Config_GlobalFunction.Message(
									Config_GlobalFunction.commentapprove, 1);
							webservice.InsertComment(newsid, commentnametext,
									commenttext, 0);
							Main.getUiApplication().popScreen(
									Main_Comments.this);
						} else {
							Config_GlobalFunction.Message(
									Config_GlobalFunction.nocomment, 1);
						}
					} else
						Config_GlobalFunction.Message(
								Config_GlobalFunction.nopostcomment, 1);
				}
			});
			add(postbtn);
		}

		protected void sublayout(int width, int height) {
			Field field = getField(0);
			layoutChild(field, getPreferredWidth() - 10, Font.getDefault()
					.getHeight() * 3);
			setPositionChild(field, 5, 5);

			field = getField(1);
			layoutChild(field, getPreferredWidth() / 5, Font.getDefault()
					.getHeight());
			setPositionChild(field, 5, getField(0).getHeight() + 10);

			field = getField(2);
			layoutChild(field, getPreferredWidth() / 2, Font.getDefault()
					.getHeight());
			setPositionChild(field, getField(1).getWidth() + 7, getField(0)
					.getHeight() + 10);

			field = getField(3);
			layoutChild(field, Display.getWidth() / 5, Font.getDefault()
					.getHeight() + 15);
			setPositionChild(field, (getField(1).getWidth()
					+ getField(2).getWidth() + 12), getField(0).getHeight() + 7);

			width = Math.min(width, getPreferredWidth());
			height = Math.min(height, getPreferredHeight());
			setExtent(width, height);
		}

		public int getPreferredHeight() {
			return getField(0).getHeight() + getField(3).getHeight() + 10;
		}

		public int getPreferredWidth() {
			return Display.getWidth();
		}

		protected void paint(Graphics graphics) {
			int rectHeight = getPreferredHeight();
			int rectWidth = getPreferredWidth();
			graphics.setColor(Color.WHITE);
			graphics.drawRect(0, 0, rectWidth, rectHeight);
			super.paint(graphics);
		}

		protected boolean touchEvent(TouchEvent event) {
			int eventCode = event.getEvent();
			if ((eventCode == TouchEvent.UNCLICK)
					|| (eventCode == TouchEvent.DOWN)) {
				int x = event.getX(1);
				int y = event.getY(1);

				if ((x >= 0) && (y >= 0) && (x < getWidth())
						&& (y < getHeight())) {
					int field = getFieldAtLocation(x, y);
					if (field >= 0) {
						return super.touchEvent(event);
					} else {
						if (eventCode == TouchEvent.UNCLICK) {
							Config_GlobalFunction.hideKeyboard();
						} else {
							setFocus();
						}
						return true;
					}
				}
			}
			return super.touchEvent(event);
		}
	}

	public class Custom_CommentsListField extends ListField {
		private String[] comment, date, poster;
		private List_NewsComment newscomment;

		private Vector content = null;
		private ListCallback callback = null;

		public Custom_CommentsListField(Vector content) {
			this.content = content;
			if (content.size() != 0) {
				comment = new String[content.size()];
				date = new String[content.size()];
				poster = new String[content.size()];

				for (int i = 0; i < content.size(); i++) {
					newscomment = (List_NewsComment) content.elementAt(i);
					comment[i] = newscomment.getCommentContent();
					date[i] = newscomment.getCommentDate();
					poster[i] = newscomment.getUserName();
				}
			} else {
				comment = new String[1];
				poster = new String[1];
				date = new String[1];

				comment[0] = Config_GlobalFunction.commentgmcontent;
				poster[0] = Config_GlobalFunction.commentgmname;
				date[0] = Config_GlobalFunction.date;
			}
			initCallbackListening();
		}

		private void initCallbackListening() {
			callback = new ListCallback();
			this.setCallback(callback);
			this.setRowHeight(-2);
		}

		private class ListCallback implements ListFieldCallback {
			public ListCallback() {
			}

			public void drawListRow(ListField listField, Graphics graphics,
					int index, int y, int width) {
				Vector text = Config_GlobalFunction.wrap(comment[index],
						Display.getWidth() - 10);
				for (int i = 0; i < text.size(); i++) {
					int liney = y + (i * Font.getDefault().getHeight());
					graphics.drawText(
							(String) text.elementAt(i),
							5,
							liney + 3,
							DrawStyle.TOP | DrawStyle.LEFT | DrawStyle.ELLIPSIS,
							Display.getWidth() - 10);
				}

				if (text.size() == 2) {
					graphics.setColor(Color.GRAY);
					graphics.drawText(date[index], 5, y
							+ Font.getDefault().getHeight() + 3);

					graphics.setColor(Color.RED);
					graphics.drawText(poster[index], Display.getWidth()
							- Font.getDefault().getAdvance(poster[index]) - 5,
							y + Font.getDefault().getHeight() + 3);
					setRowHeight(index, getRowHeight() + 9);
				} else if (text.size() == 3) {
					graphics.setColor(Color.GRAY);
					graphics.drawText(date[index], 5, y
							+ Font.getDefault().getHeight() * 2 + 3);

					graphics.setColor(Color.RED);
					graphics.drawText(poster[index], Display.getWidth()
							- Font.getDefault().getAdvance(poster[index]) - 5,
							y + Font.getDefault().getHeight() * 2 + 3);
					setRowHeight(index, getRowHeight() * 15 / 10 + 9);
				}

				graphics.setColor(Color.BLACK);
				graphics.drawRect(0, y, width, getRowHeight(index));
			}

			public Object get(ListField listField, int index) {
				return content.elementAt(index);
			}

			public int getPreferredWidth(ListField listField) {
				return Display.getWidth();
			}

			public int indexOfList(ListField listField, String prefix, int start) {
				return content.indexOf(prefix, start);
			}
		}
	}
}
