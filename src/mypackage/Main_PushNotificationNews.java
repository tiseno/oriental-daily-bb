package mypackage;

import java.util.Vector;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransport;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.Menu;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.util.MathUtilities;

public class Main_PushNotificationNews extends MainScreen {
	private Custom_FontField slider;
	private boolean isSliderVisible = false, isoutdated = false;
	private BitmapField image;
	private Custom_LabelField titlelabel, reporterlabel, authorlabel,
			contentlabel, datelabel, commentnolabel, imagelabel;
	private int font, fontmin, fontmax = 30, catid;
	private String title, author, content, date, imagepath, cattitle;
	private Database_Webservice webservice;
	private List_News news;
	private Custom_ButtonField closebtn;
	private String get_CountComment_SOAP_ACTION = "urn:OrientalServices1#responseGetCountComment";
	private String get_CountComment_METHOD_NAME = "GetCountComment";

	public Main_PushNotificationNews(final int newsid) {
		super(USE_ALL_WIDTH | NO_SYSTEM_MENU_ITEMS);

		if (Display.getWidth() > 480)
			fontmin = 25;
		else if (Display.getWidth() == 480)
			fontmin = 20;
		else if (Display.getWidth() < 480 && Display.getWidth() >= 320)
			fontmin = 15;
		else
			fontmin = 10;

		webservice = new Database_Webservice();
		webservice.LoadAllNews();
		font = webservice.ReadFontSize();
		add(new Custom_PushTopField());
		add(new Custom_NewsDetailBottom(newsid));

		for (int i = 0; i < webservice.news.size(); i++) {
			news = (List_News) webservice.news.elementAt(i);
			if (newsid == news.getID()) {
				isoutdated = true;
				break;
			} else
				continue;
		}

		if (isoutdated) {
			webservice.LoadSingleNews(newsid);
			news = (List_News) webservice.news.elementAt(0);
			title = news.getNtitle();
			author = news.getAuthor();
			content = news.getNintrotext() + news.getFullText();
			date = news.getNArticalD();
			imagepath = news.getImagePath().toString();
			cattitle = news.getNewCatName();
			catid = news.getCatID();

			add(new Custom_HeaderField(cattitle));
			add(new Custom_NewsDetailField(title, author, content, date,
					imagepath, catid, "1"));
		} else {
			if (Config_GlobalFunction.isConnected()) {
				Config_GlobalFunction
						.Message(Config_GlobalFunction.getting, 10);
				Main.getUiApplication().invokeLater(new Runnable() {
					public void run() {
						webservice.UpdateAllCatNews(true, newsid);
						Main.getUiApplication().popScreen(
								Main_PushNotificationNews.this);
					}
				}, 1 * 500, false);
			} else {
				Config_GlobalFunction.Message(Config_GlobalFunction.nowifi, 1);
				Main.getUiApplication().pushScreen(new Main_AllLatestNews());
			}
		}

		slider = new Custom_FontField(
				Bitmap.getBitmapResource("slider_thumb_normal.png"),
				Bitmap.getBitmapResource("slider_progress_normal.png"),
				Bitmap.getBitmapResource("slider_base_normal.png"),
				Bitmap.getBitmapResource("slider_thumb_focused.png"),
				Bitmap.getBitmapResource("slider_progress_focused.png"),
				Bitmap.getBitmapResource("slider_base_focused.png"),
				Bitmap.getBitmapResource("slider_thumb_pressed.png"),
				Bitmap.getBitmapResource("slider_progress_pressed.png"),
				Bitmap.getBitmapResource("slider_base_pressed.png"), fontmax,
				font - fontmin, 5, 5, FOCUSABLE);

		Main.getUiApplication().invokeLater(new Runnable() {
			public void run() {
				if (Config_GlobalFunction.isConnected())
					GetCountCommentNews(newsid);
				else
					commentnolabel.setText(Config_GlobalFunction.commentlabel
							+ "0");
			}
		}, 1 * 500, false);
	}

	public boolean onClose() {
		if (!slider.isVisible()) {
			if (Config_GlobalFunction
					.Dialog(Config_GlobalFunction.alertmsgpushleave)) {
				close();
				return true;
			} else
				return false;
		} else {
			webservice.UpdateFontSize(font);
			delete(slider);
			isSliderVisible = !isSliderVisible;
			return false;
		}
	}

	protected void makeMenu(Menu menu, int instance) {

	}

	private void setSliderVisible(boolean isVisible) {
		if (isVisible != isSliderVisible) {
			if (isVisible) {
				insert(slider, 2);
			} else {
				webservice.UpdateFontSize(font);
				delete(slider);
			}
			isSliderVisible = isVisible;
			invalidate();
		}
	}

	public class Custom_PushTopField extends Manager {
		private Bitmap close = Config_GlobalFunction.Bitmap("icon_close.png");
		private Bitmap closeactive = Config_GlobalFunction
				.Bitmap("icon_close_active.png");

		private Custom_LabelField title;
		private int fontsize;

		Custom_PushTopField() {
			super(Manager.USE_ALL_WIDTH | Manager.NO_VERTICAL_SCROLL
					| Manager.NO_HORIZONTAL_SCROLL);

			if (Display.getWidth() > 480)
				fontsize = 43;
			else if (Display.getWidth() < 481 && Display.getWidth() > 320)
				fontsize = 33;
			else
				fontsize = 23;

			setBackground(Config_GlobalFunction.loadbackground(Display
					.getWidth() + "_" + "header_bar.png"));

			title = new Custom_LabelField(Config_GlobalFunction.pushtitle,
					DrawStyle.ELLIPSIS | LabelField.USE_ALL_WIDTH
							| DrawStyle.VCENTER | DrawStyle.HCENTER,
					Color.WHITE);

			title.setFont(Font.getDefault().derive(Font.BOLD, fontsize));
			add(title);

			closebtn = new Custom_ButtonField(close, closeactive, closeactive);
			closebtn.setChangeListener(new FieldChangeListener() {
				public void fieldChanged(Field field, int context) {
					if (!slider.isVisible()) {
						if (Config_GlobalFunction
								.Dialog(Config_GlobalFunction.alertmsgpushleave))
							close();
						else
							Config_GlobalFunction.CloseDialog();
					} else {
						webservice.UpdateFontSize(font);
						Main_PushNotificationNews.this.delete(slider);
						isSliderVisible = !isSliderVisible;
					}
				}
			});
			add(closebtn);
		}

		protected void sublayout(int width, int height) {
			int Height = (getPreferredHeight() - close.getHeight()) / 2;
			Field field = getField(0);
			layoutChild(field, getPreferredWidth() / 2,
					getPreferredHeight() - 10);
			setPositionChild(field, (getPreferredWidth() - getField(0)
					.getWidth()) / 2, (getPreferredHeight() - getField(0)
					.getHeight()) / 2);

			field = getField(1);
			layoutChild(field, close.getWidth(), close.getHeight());
			setPositionChild(field, getPreferredWidth()
					- (close.getWidth() + 5), Height);

			width = Math.min(width, getPreferredWidth());
			height = Math.min(height, getPreferredHeight());
			setExtent(width, height);
		}

		public int getPreferredHeight() {
			return Config_GlobalFunction.Bitmap("header_bar.png").getHeight();
		}

		public int getPreferredWidth() {
			return Display.getWidth();
		}

		public void paint(Graphics graphics) {
			int rectHeight = getPreferredHeight();
			int rectWidth = getPreferredWidth();

			graphics.drawRect(0, 0, rectWidth, rectHeight);
			super.paint(graphics);
		}
	}

	public class Custom_NewsDetailBottom extends Manager {
		private Bitmap font = Config_GlobalFunction.Bitmap("icon_font.png");
		private Bitmap fontactive = Config_GlobalFunction
				.Bitmap("icon_font_active.png");
		//private Bitmap share = Config_GlobalFunction.Bitmap("icon_share.png");
		//private Bitmap shareactive = Config_GlobalFunction
				//.Bitmap("icon_share_active.png");
		private Bitmap comment = Config_GlobalFunction
				.Bitmap("icon_comment.png");
		private Bitmap commentactive = Config_GlobalFunction
				.Bitmap("icon_comment_active.png");

		private Custom_ButtonField fontbtn,/* sharebtn,*/ commentbtn;

		Custom_NewsDetailBottom(final int newsid) {
			super(Manager.USE_ALL_WIDTH | Manager.NO_VERTICAL_SCROLL
					| Manager.NO_HORIZONTAL_SCROLL);
			setBackground(Config_GlobalFunction.loadbackground(Display
					.getWidth() + "_" + "footer_bar.png"));

			fontbtn = new Custom_ButtonField(font, fontactive, fontactive);
			fontbtn.setChangeListener(new FieldChangeListener() {
				public void fieldChanged(Field field, int context) {
					setSliderVisible(!isSliderVisible);
				}
			});
			add(fontbtn);

			/*sharebtn = new Custom_ButtonField(share, shareactive, shareactive);
			sharebtn.setChangeListener(new FieldChangeListener() {
				public void fieldChanged(Field field, int context) {
					if (Config_GlobalFunction.isConnected()) {

						String NEXT_URL = "http://www.facebook.com/connect/login_success.html;ConnectionTimeout=60000;deviceside=true;apn=facebook.com";
						String APPLICATION_ID = "338017939616672";
						String APPLICATION_SECRET = "f807f09099306b8cbfb56fc9667f135e";
						String[] PERMISSIONS = Facebook.Permissions.USER_DATA_PERMISSIONS;

						ApplicationSettings as = new ApplicationSettings(
								NEXT_URL, APPLICATION_ID, APPLICATION_SECRET,
								PERMISSIONS);

						Facebook fb = Facebook.getInstance(as);

						try {
							User user = fb.getCurrentUser();
							user.publishStatus(Config_GlobalFunction.sharelink
									+ newsid + Database_Webservice.ht_params);
						} catch (FacebookException e) {
						}
					} else
						Config_GlobalFunction.Message(
								Config_GlobalFunction.nosharenews, 1);
				}
			});
			add(sharebtn);*/

			commentbtn = new Custom_ButtonField(comment, commentactive,
					commentactive);
			commentbtn.setChangeListener(new FieldChangeListener() {
				public void fieldChanged(Field field, int context) {
					if (Config_GlobalFunction.isConnected()) {
						Main.getUiApplication().pushScreen(
								new Custom_LoadingScreen(3));
						Main.getUiApplication().invokeLater(new Runnable() {
							public void run() {
								webservice.GetCommentNews(newsid, true);
							}
						}, 1 * 1000, false);
					} else
						Config_GlobalFunction.Message(
								Config_GlobalFunction.noaccesscomment, 1);
				}
			});
			add(commentbtn);
		}

		protected void sublayout(int width, int height) {
			int Height = (getPreferredHeight() - font.getHeight()) / 2;

			Field field = getField(0);
			layoutChild(field, font.getWidth(), font.getHeight());
			setPositionChild(field, getGap(), Height);

			/*field = getField(1);
			layoutChild(field, share.getWidth(), share.getHeight());
			setPositionChild(field, getPreferredWidth() / getFieldCount()
					+ getGap(), Height);*/

			field = getField(1);
			layoutChild(field, comment.getWidth(), comment.getHeight());
			setPositionChild(field, getPreferredWidth() / getFieldCount()
					+ getGap(), Height);

			width = Math.min(width, getPreferredWidth());
			height = Math.min(height, getPreferredHeight());
			setExtent(width, height);
		}

		public int getPreferredHeight() {
			return Config_GlobalFunction.Bitmap("footer_bar.png").getHeight();
		}

		public int getPreferredWidth() {
			return Display.getWidth();
		}

		protected void paint(Graphics graphics) {
			int rectHeight = getPreferredHeight();
			int rectWidth = getPreferredWidth();
			graphics.drawRect(0, 0, rectWidth, rectHeight);
			super.paint(graphics);
		}

		private int getGap() {
			return ((getPreferredWidth() / getFieldCount()) - font.getWidth()) / 2;
		}

		protected boolean navigationMovement(int dx, int dy, int status,
				int time) {
			int focusIndex = getFieldWithFocusIndex();

			while (dy > 0) {
				if (!slider.isVisible()) {
					Field f = contentlabel;
					if (f.isFocusable()) {
						f.setFocus();
						dy--;
					}
				} else {
					Field f = slider;
					if (f.isFocusable()) {
						f.setFocus();
						dy--;
					}
				}				
			}

			while (dy < 0) {
				Field f = closebtn;
				if (f.isFocusable()) {
					f.setFocus();
					dy++;
				}
			}

			while (dx > 0) {
				focusIndex++;

				if (focusIndex >= getFieldCount()) {
					return false;
				} else {
					Field f = getField(focusIndex);

					if (f.isFocusable()) {
						f.setFocus();
						dx--;
					}
				}
			}

			while (dx < 0) {
				focusIndex--;

				if (focusIndex < 0) {
					return false;
				} else {
					Field f = getField(focusIndex);

					if (f.isFocusable()) {
						f.setFocus();
						dx++;
					}
				}
			}

			return true;
		}
	}

	public class Custom_NewsDetailField extends Manager {
		private String imagepaths;
		private Bitmap localimage = Config_GlobalFunction
				.Bitmap("image_base.png");
		private Util_LazyLoader loader;

		Custom_NewsDetailField(String title, String author, String content,
				String date, final String imagepath, int catid,
				String commentsno) {
			super(Manager.USE_ALL_WIDTH | Manager.VERTICAL_SCROLL);
			this.imagepaths = imagepath;
			setBackground(Config_GlobalFunction
					.loadbackground("background.png"));

			titlelabel = new Custom_LabelField(title, LabelField.USE_ALL_WIDTH
					| DrawStyle.LEFT);
			titlelabel.setFont(Font.getDefault().derive(Font.PLAIN, font));
			titlelabel.setFontColor(Color.BLACK);
			add(titlelabel);

			if (!imagepath.equals("no picture")) {
				image = new BitmapField(localimage, Field.FIELD_HCENTER
						| Field.FIELD_VCENTER | Field.FOCUSABLE
						| ButtonField.CONSUME_CLICK) {
					protected boolean navigationClick(int status, int time) {
						popupImage();
						return true;
					}

					protected void onFocus(int direction) {
						super.onFocus(direction);
						imagelabel.setFontColor(0x540604);
						this.invalidate();
					}

					public void onUnfocus() {
						super.onUnfocus();
						imagelabel.setFontColor(0x000000);
						this.invalidate();
					}
				};
				add(image);
				imagelabel = new Custom_LabelField(
						Config_GlobalFunction.imagelabel, DrawStyle.ELLIPSIS
								| LabelField.USE_ALL_WIDTH | DrawStyle.HCENTER,
						Color.GRAY);
				imagelabel.setFont(Font.getDefault().derive(Font.PLAIN, font));
				add(imagelabel);

				new Thread() {
					public void run() {
						if (Config_GlobalFunction.isConnected()) {
							loader = new Util_LazyLoader(imagepath,
									new Util_BitmapDowloadListener() {
										public void ImageDownloadCompleted(
												Bitmap bmp) {
											synchronized (UiApplication
													.getEventLock()) {
												image.setBitmap(bmp);
												invalidate();
											}
										}
									});
							loader.run();
						}
					}
				}.start();
			}

			if (!author.equals("empty")) {
				if (catid != 7) {
					authorlabel = new Custom_LabelField(
							Config_GlobalFunction.authorlabel + author,
							DrawStyle.ELLIPSIS | LabelField.USE_ALL_WIDTH
									| DrawStyle.LEFT);
					authorlabel.setFontColor(Color.RED);
					authorlabel.setFont(Font.getDefault().derive(Font.PLAIN,
							font));
					add(authorlabel);
				} else {
					reporterlabel = new Custom_LabelField(
							Config_GlobalFunction.reporterlabel + author,
							DrawStyle.ELLIPSIS | LabelField.USE_ALL_WIDTH
									| DrawStyle.RIGHT);
					reporterlabel.setFontColor(Color.RED);
					reporterlabel.setFont(Font.getDefault().derive(Font.PLAIN,
							font));
					add(reporterlabel);
				}
			}

			contentlabel = new Custom_LabelField(
					content,
					LabelField.USE_ALL_WIDTH | DrawStyle.LEFT | Field.FOCUSABLE,
					Color.BLACK, Color.BLACK);
			contentlabel.setFont(Font.getDefault().derive(Font.PLAIN, font));
			add(contentlabel);

			datelabel = new Custom_LabelField(date, DrawStyle.ELLIPSIS
					| LabelField.USE_ALL_WIDTH | DrawStyle.RIGHT);
			datelabel.setFontColor(Color.GRAY);
			datelabel.setFont(Font.getDefault().derive(Font.PLAIN, font));
			add(datelabel);

			commentnolabel = new Custom_LabelField(
					Config_GlobalFunction.commentlabel + commentsno,
					DrawStyle.ELLIPSIS | LabelField.USE_ALL_WIDTH
							| DrawStyle.RIGHT | Field.FOCUSABLE, Color.RED,
					Color.RED);
			commentnolabel.setFont(Font.getDefault().derive(Font.PLAIN, font));
			add(commentnolabel);
		}

		private void popupImage() {
			imagepaths = imagepaths.replace(
					imagepaths.charAt(imagepaths.length() - 5), 'M');
			Bitmap image = Util_ImageLoader.loadImage(imagepaths);
			Dialog d = new Dialog("",
					new String[] { Config_GlobalFunction.back },
					new int[] { Dialog.CANCEL }, Dialog.CANCEL, image);
			if (d.doModal() == Dialog.CANCEL)
				d.close();
		}

		protected void sublayout(int width, int height) {
			Field field = getField(0);
			layoutChild(field, getPreferredWidth() - 10,
					titlelabel.getContentHeight());
			setPositionChild(field, 5, 5);

			if (!author.equals("empty")) {
				if (!imagepath.equals("no picture")) {
					field = getField(1);
					layoutChild(field, image.getBitmapWidth(),
							image.getBitmapHeight());
					setPositionChild(field, 5, getField(0).getHeight() + 10);

					field = getField(2);
					layoutChild(field, image.getBitmapWidth(), Font
							.getDefault().getHeight());
					setPositionChild(field, 5, getField(0).getHeight()
							+ getField(1).getHeight() + 13);

					field = getField(3);
					if (catid == 7) {
						layoutChild(field, (getPreferredWidth() - 10) / 2,
								reporterlabel.getContentHeight());
						setPositionChild(field, getPreferredWidth()
								- reporterlabel.getContentWidth() - 5,
								getField(0).getHeight()
										+ getField(1).getHeight()
										+ getField(2).getHeight() + 18);
					} else {

						layoutChild(field, (getPreferredWidth() - 10) / 2,
								authorlabel.getContentHeight());
						setPositionChild(field, 5, getField(0).getHeight()
								+ getField(1).getHeight()
								+ getField(2).getHeight() + 18);
					}

					field = getField(4);
					layoutChild(field, getPreferredWidth() - 10, Font
							.getDefault().getHeight());
					setPositionChild(field, 5, getField(0).getHeight()
							+ getField(1).getHeight() + getField(2).getHeight()
							+ getField(3).getHeight() + 23);

					field = getField(5);
					layoutChild(field, (getPreferredWidth() - 10) / 2,
							datelabel.getContentHeight());
					setPositionChild(field,
							getPreferredWidth() - datelabel.getContentWidth()
									- 5, getField(0).getHeight()
									+ getField(1).getHeight()
									+ getField(2).getHeight()
									+ getField(3).getHeight()
									+ getField(4).getHeight() + 28);

					field = getField(6);
					layoutChild(field, (getPreferredWidth() - 10) / 2,
							commentnolabel.getContentHeight());
					setPositionChild(field, getPreferredWidth()
							- (commentnolabel.getContentWidth() + 5),
							getField(0).getHeight() + getField(1).getHeight()
									+ getField(2).getHeight()
									+ getField(3).getHeight()
									+ getField(4).getHeight()
									+ getField(5).getHeight() + 33);
				} else {
					field = getField(1);
					if (catid == 7) {
						layoutChild(field, (getPreferredWidth() - 10) / 2,
								reporterlabel.getContentHeight());
						setPositionChild(field, getPreferredWidth()
								- reporterlabel.getContentWidth() - 5,
								getField(0).getHeight() + 5);
					} else {
						layoutChild(field, (getPreferredWidth() - 10) / 2,
								authorlabel.getContentHeight());
						setPositionChild(field, 5, getField(0).getHeight() + 5);
					}

					field = getField(2);
					layoutChild(field, getPreferredWidth() - 10, Font
							.getDefault().getHeight());
					setPositionChild(field, 5, getField(0).getHeight()
							+ getField(1).getHeight() + 10);

					field = getField(3);
					layoutChild(field, (getPreferredWidth() - 10) / 2,
							datelabel.getContentHeight());
					setPositionChild(field,
							getPreferredWidth() - datelabel.getContentWidth()
									- 5, getField(0).getHeight()
									+ getField(1).getHeight()
									+ getField(2).getHeight() + 15);

					field = getField(4);
					layoutChild(field, (getPreferredWidth() - 10) / 2,
							commentnolabel.getContentHeight());
					setPositionChild(field, getPreferredWidth()
							- (commentnolabel.getContentWidth() + 5),
							getField(0).getHeight() + getField(1).getHeight()
									+ getField(2).getHeight()
									+ getField(3).getHeight() + 20);
				}
			} else {
				if (!imagepath.equals("no picture")) {
					field = getField(1);
					layoutChild(field, image.getBitmapWidth(),
							image.getBitmapHeight());
					setPositionChild(field, 5, getField(0).getHeight() + 10);

					field = getField(2);
					layoutChild(field, image.getBitmapWidth(), Font
							.getDefault().getHeight());
					setPositionChild(field, 5, getField(0).getHeight()
							+ getField(1).getHeight() + 13);

					field = getField(3);
					layoutChild(field, getPreferredWidth() - 10, Font
							.getDefault().getHeight());
					setPositionChild(field, 5, getField(0).getHeight()
							+ getField(1).getHeight() + getField(2).getHeight()
							+ 18);

					field = getField(4);
					layoutChild(field, (getPreferredWidth() - 10) / 2,
							datelabel.getContentHeight());
					setPositionChild(field,
							getPreferredWidth() - datelabel.getContentWidth()
									- 5, getField(0).getHeight()
									+ getField(1).getHeight()
									+ getField(2).getHeight()
									+ getField(3).getHeight() + 23);

					field = getField(5);
					layoutChild(field, (getPreferredWidth() - 10) / 2,
							commentnolabel.getContentHeight());
					setPositionChild(field, getPreferredWidth()
							- (commentnolabel.getContentWidth() + 5),
							getField(0).getHeight() + getField(1).getHeight()
									+ getField(2).getHeight()
									+ getField(3).getHeight()
									+ getField(4).getHeight() + 28);
				} else {
					field = getField(1);
					layoutChild(field, getPreferredWidth() - 10, Font
							.getDefault().getHeight());
					setPositionChild(field, 5, getField(0).getHeight() + 10);

					field = getField(2);
					layoutChild(field, (getPreferredWidth() - 10) / 2,
							datelabel.getContentHeight());
					setPositionChild(field,
							getPreferredWidth() - datelabel.getContentWidth()
									- 5, getField(0).getHeight()
									+ getField(1).getHeight() + 15);

					field = getField(3);
					layoutChild(field, (getPreferredWidth() - 10) / 2,
							commentnolabel.getContentHeight());
					setPositionChild(field, getPreferredWidth()
							- (commentnolabel.getContentWidth() + 5),
							getField(0).getHeight() + getField(1).getHeight()
									+ getField(2).getHeight() + 20);
				}
			}
			width = Math.min(width, getPreferredWidth());
			height = Math.min(height, getPreferredHeight());
			setExtent(width, height);
		}

		public int getPreferredHeight() {
			if (!author.equals("empty")) {
				if (!imagepath.equals("no picture")) {
					return getField(0).getHeight() + getField(1).getHeight()
							+ getField(2).getHeight() + getField(3).getHeight()
							+ getField(4).getHeight() + getField(5).getHeight()
							+ getField(6).getHeight() + 38;
				} else {
					return getField(0).getHeight() + getField(1).getHeight()
							+ getField(2).getHeight() + getField(3).getHeight()
							+ getField(4).getHeight() + 30;
				}
			} else {
				if (!imagepath.equals("no picture")) {
					return getField(0).getHeight() + getField(1).getHeight()
							+ getField(2).getHeight() + getField(3).getHeight()
							+ getField(4).getHeight() + getField(5).getHeight()
							+ 33;
				} else {
					return getField(0).getHeight() + getField(1).getHeight()
							+ getField(2).getHeight() + getField(3).getHeight()
							+ 25;
				}
			}
		}

		public int getPreferredWidth() {
			return Display.getWidth();
		}

		protected void paint(Graphics graphics) {
			int rectHeight = getPreferredHeight();
			int rectWidth = getPreferredWidth();
			graphics.drawRect(0, 0, rectWidth, rectHeight);
			super.paint(graphics);
		}
	}

	public class Custom_FontField extends Field {
		private final static int STATE_NORMAL = 0;
		private final static int STATE_FOCUSED = 1;
		private final static int STATE_PRESSED = 2;

		private final static int NUM_STATES = 3;

		private Bitmap[] _thumb = new Bitmap[NUM_STATES];
		private Bitmap[] _progress = new Bitmap[NUM_STATES];
		private Bitmap[] _base = new Bitmap[NUM_STATES];
		private Bitmap[] _progressTile = new Bitmap[NUM_STATES];
		private Bitmap[] _baseTile = new Bitmap[NUM_STATES];

		private int _thumbWidth;
		private int _thumbHeight;
		private int _progressHeight;
		private int _baseWidth;
		private int _baseHeight;

		private int _leftCapWidth;
		private int _rightCapWidth;

		private boolean _focused;
		private boolean _pressed;

		private int _numValues;
		private int _currentValue;

		private int _preferredHeight;

		private int _baseY;
		private int _progressY;
		private int _thumbY;
		private int _trackWidth;

		private int _rop;

		public Custom_FontField(Bitmap normalThumb, Bitmap normalProgress,
				Bitmap normalBase, Bitmap focusedThumb, Bitmap focusedProgress,
				Bitmap focusedBase, int numValues, int initialValue,
				int leftCapWidth, int rightCapWidth) {
			this(normalThumb, normalProgress, normalBase, focusedThumb,
					focusedProgress, focusedBase, null, null, null, numValues,
					initialValue, leftCapWidth, rightCapWidth, 0);
		}

		public Custom_FontField(Bitmap normalThumb, Bitmap normalProgress,
				Bitmap normalBase, Bitmap focusedThumb, Bitmap focusedProgress,
				Bitmap focusedBase, int numValues, int initialValue,
				int leftCapWidth, int rightCapWidth, long style) {
			this(normalThumb, normalProgress, normalBase, focusedThumb,
					focusedProgress, focusedBase, null, null, null, numValues,
					initialValue, leftCapWidth, rightCapWidth, style);
		}

		public Custom_FontField(Bitmap normalThumb, Bitmap normalProgress,
				Bitmap normalBase, Bitmap focusedThumb, Bitmap focusedProgress,
				Bitmap focusedBase, Bitmap pressedThumb,
				Bitmap pressedProgress, Bitmap pressedBase, int numValues,
				int initialValue, int leftCapWidth, int rightCapWidth) {
			this(normalThumb, normalProgress, normalBase, focusedThumb,
					focusedProgress, focusedBase, pressedThumb,
					pressedProgress, pressedBase, numValues, initialValue,
					leftCapWidth, rightCapWidth, 0);
		}

		public Custom_FontField(Bitmap normalThumb, Bitmap normalProgress,
				Bitmap normalBase, Bitmap focusedThumb, Bitmap focusedProgress,
				Bitmap focusedBase, Bitmap pressedThumb,
				Bitmap pressedProgress, Bitmap pressedBase, int numValues,
				int initialValue, int leftCapWidth, int rightCapWidth,
				long style) {
			super(style);

			if (numValues < 2 || initialValue >= numValues) {
				throw new IllegalArgumentException("invalid value parameters");
			}
			if (normalThumb == null || normalProgress == null
					|| normalBase == null || focusedThumb == null
					|| focusedProgress == null || focusedBase == null) {
				throw new IllegalArgumentException(
						"thumb, normal, focused  are required");
			}

			_thumbWidth = normalThumb.getWidth();
			_thumbHeight = normalThumb.getHeight();
			_progressHeight = normalProgress.getHeight();
			_baseWidth = normalBase.getWidth();
			_baseHeight = normalBase.getHeight();

			if (focusedThumb.getWidth() != _thumbWidth
					|| focusedThumb.getHeight() != _thumbHeight
					|| focusedProgress.getHeight() != _progressHeight
					|| focusedBase.getHeight() != _baseHeight) {
				throw new IllegalArgumentException(
						"all base bitmaps and all progress bitmaps must be the same height");
			}
			if (pressedThumb != null && pressedProgress != null
					&& pressedBase != null) {
				if (pressedThumb.getWidth() != _thumbWidth
						|| pressedThumb.getHeight() != _thumbHeight
						|| pressedProgress.getHeight() != _progressHeight
						|| pressedBase.getHeight() != _baseHeight) {
					throw new IllegalArgumentException(
							"all base bitmaps and all progress bitmaps must be the same height");
				}
			}

			_leftCapWidth = leftCapWidth;
			_rightCapWidth = rightCapWidth;

			_rop = Graphics.ROP_SRC_COPY;

			initBitmaps(normalThumb, normalProgress, normalBase, STATE_NORMAL);
			initBitmaps(focusedThumb, focusedProgress, focusedBase,
					STATE_FOCUSED);

			if (pressedThumb != null && pressedProgress != null
					&& pressedBase != null) {
				initBitmaps(pressedThumb, pressedProgress, pressedBase,
						STATE_PRESSED);
			} else {
				_thumb[STATE_PRESSED] = _thumb[STATE_FOCUSED];
				_progress[STATE_PRESSED] = _progress[STATE_FOCUSED];
				_base[STATE_PRESSED] = _base[STATE_FOCUSED];
				_progressTile[STATE_PRESSED] = _progressTile[STATE_FOCUSED];
				_baseTile[STATE_PRESSED] = _baseTile[STATE_FOCUSED];
			}

			_preferredHeight = Math.max(_thumbHeight,
					Math.max(_progressHeight, _baseHeight));

			_numValues = numValues;
			setValue(initialValue);
		}

		public void initBitmaps(Bitmap thumb, Bitmap progress, Bitmap base,
				int state) {
			if (progress.getWidth() <= _leftCapWidth
					|| base.getWidth() <= _rightCapWidth) {
				throw new IllegalArgumentException();
			}

			if (thumb.hasAlpha() || progress.hasAlpha() || base.hasAlpha()) {
				_rop = Graphics.ROP_SRC_ALPHA;
			}

			_thumb[state] = thumb;
			_progress[state] = progress;
			_base[state] = base;

			int[] argbCopyBuffer;

			int progressTileWidth = progress.getWidth() - _leftCapWidth;
			int progressTileHeight = progress.getHeight();

			Bitmap progressTile = new Bitmap(progressTileWidth,
					progressTileHeight);

			argbCopyBuffer = new int[progressTileWidth * progressTileHeight];
			progress.getARGB(argbCopyBuffer, 0, progressTileWidth,
					_leftCapWidth, 0, progressTileWidth, progressTileHeight);
			progressTile.setARGB(argbCopyBuffer, 0, progressTileWidth, 0, 0,
					progressTileWidth, progressTileHeight);

			int baseTileWidth = base.getWidth() - _rightCapWidth;
			int baseTileHeight = base.getHeight();

			Bitmap baseTile = new Bitmap(baseTileWidth, baseTileHeight);

			argbCopyBuffer = new int[baseTileWidth * baseTileHeight];
			base.getARGB(argbCopyBuffer, 0, baseTileWidth, 0, 0, baseTileWidth,
					baseTileHeight);
			baseTile.setARGB(argbCopyBuffer, 0, baseTileWidth, 0, 0,
					baseTileWidth, baseTileHeight);

			_progressTile[state] = progressTile;
			_baseTile[state] = baseTile;
		}

		public void setValue(int newValue) {
			if (newValue < 0 || newValue >= _numValues) {
				throw new IllegalArgumentException();
			}
			_currentValue = newValue;
			fieldChangeNotify(FieldChangeListener.PROGRAMMATIC);
			invalidate();
		}

		public int getValue() {
			return _currentValue;
		}

		public int getPreferredWidth() {
			return Integer.MAX_VALUE;
		}

		public int getPreferredHeight() {
			return _preferredHeight;
		}

		protected void layout(int width, int height) {
			width = Math.min(width, getPreferredWidth());
			height = Math.min(height, getPreferredHeight());

			_progressY = (height - _progressHeight) / 2;
			_baseY = (height - _baseHeight) / 2;
			_thumbY = (height - _thumbHeight) / 2;

			_trackWidth = width - _leftCapWidth - _rightCapWidth;

			setExtent(width, height);
		}

		public void paint(Graphics g) {
			int contentWidth = getContentWidth();

			int thumbX = _leftCapWidth + (_trackWidth - _thumbWidth)
					* _currentValue / (_numValues - 1);
			int transitionX = thumbX + _thumbWidth / 2;

			int currentState = _pressed ? STATE_PRESSED
					: (_focused ? STATE_FOCUSED : STATE_NORMAL);

			Bitmap thumb = _thumb[currentState];
			Bitmap progress = _progress[currentState];
			Bitmap base = _base[currentState];
			Bitmap progressTile = _progressTile[currentState];
			Bitmap baseTile = _baseTile[currentState];

			g.drawBitmap(0, _progressY, _leftCapWidth, _progressHeight,
					progress, 0, 0);
			g.tileRop(_rop, _leftCapWidth, _progressY, transitionX
					- _leftCapWidth, _progressHeight, progressTile, 0, 0);

			g.drawBitmap(contentWidth - _rightCapWidth, _baseY, _rightCapWidth,
					_baseHeight, base, _baseWidth - _rightCapWidth, 0);
			g.tileRop(_rop, transitionX, _baseY, contentWidth - transitionX
					- _rightCapWidth, _baseHeight, baseTile, 0, 0);

			g.drawBitmap(thumbX, _thumbY, _thumbWidth, _thumbHeight, thumb, 0,
					0);
		}

		protected void drawFocus(Graphics g, boolean on) {

		}

		protected void onFocus(int direction) {
			_focused = true;
			invalidate();
			super.onFocus(direction);
		}

		protected void onUnfocus() {
			_focused = false;
			invalidate();
			super.onUnfocus();
		}

		protected boolean touchEvent(TouchEvent message) {
			int event = message.getEvent();
			switch (event) {

			case TouchEvent.CLICK:
			case TouchEvent.DOWN:
				if (touchEventOutOfBounds(message)) {
					return false;
				}

			case TouchEvent.MOVE:
				_pressed = true;
				setValueByTouchPosition(message.getX(1));
				fieldChangeNotify(0);
				return true;

			case TouchEvent.UNCLICK:
			case TouchEvent.UP:
				_pressed = false;
				invalidate();
				return true;

			default:
				return false;
			}
		}

		private boolean touchEventOutOfBounds(TouchEvent message) {
			int x = message.getX(1);
			int y = message.getY(1);
			return (x < 0 || y < 0 || x > getWidth() || y > getHeight());
		}

		private void setValueByTouchPosition(int x) {
			_currentValue = MathUtilities.clamp(0, (x - _leftCapWidth)
					* _numValues / _trackWidth, _numValues - 1);
			font = getValue() + fontmin;
			titlelabel.setFont(Font.getDefault().derive(Font.PLAIN, font));
			contentlabel.setFont(Font.getDefault().derive(Font.PLAIN, font));
			datelabel.setFont(Font.getDefault().derive(Font.PLAIN, font));
			commentnolabel.setFont(Font.getDefault().derive(Font.PLAIN, font));
			if (!author.equals("empty")) {
				if (catid == 7) {
					reporterlabel.setFont(Font.getDefault().derive(Font.PLAIN,
							font));
				} else
					authorlabel.setFont(Font.getDefault().derive(Font.PLAIN,
							font));
			}
			if (!imagepath.equals("no picture"))
				imagelabel.setFont(Font.getDefault().derive(Font.PLAIN, font));
			invalidate();
		}

		protected boolean navigationMovement(int dx, int dy, int status,
				int time) {
			if (_pressed) {
				if (dx > 0 || dy > 0) {
					incrementValue();
				} else {
					decrementValue();
				}
				fieldChangeNotify(0);
				font = getValue() + fontmin;
				titlelabel.setFont(Font.getDefault().derive(Font.PLAIN, font));
				contentlabel
						.setFont(Font.getDefault().derive(Font.PLAIN, font));
				datelabel.setFont(Font.getDefault().derive(Font.PLAIN, font));
				commentnolabel.setFont(Font.getDefault().derive(Font.PLAIN,
						font));
				if (!author.equals("empty")) {
					if (catid == 7) {
						reporterlabel.setFont(Font.getDefault().derive(
								Font.PLAIN, font));
					} else
						authorlabel.setFont(Font.getDefault().derive(
								Font.PLAIN, font));
				}

				if (!imagepath.equals("no picture"))
					imagelabel.setFont(Font.getDefault().derive(Font.PLAIN,
							font));
				return true;
			}
			return super.navigationMovement(dx, dy, status, time);
		}

		private void incrementValue() {
			if (_currentValue + 1 < _numValues) {
				_currentValue++;
				invalidate();
			}
		}

		private void decrementValue() {
			if (_currentValue > 0) {
				_currentValue--;
				invalidate();
			}
		}

		protected boolean invokeAction(int action) {
			if (action == ACTION_INVOKE) {
				togglePressed();
				return true;
			}
			return false;
		}

		protected boolean keyChar(char key, int status, int time) {
			if (key == Characters.SPACE || key == Characters.ENTER) {
				togglePressed();
				return true;
			}
			return false;
		}

		protected boolean trackwheelClick(int status, int time) {
			togglePressed();
			return true;
		}

		private void togglePressed() {
			_pressed = !_pressed;
			invalidate();
		}
	}

	public void GetCountCommentNews(final int gCommentNewsID) {
		SoapObject request = new SoapObject(Database_Webservice.NAMESPACE,
				get_CountComment_METHOD_NAME);
		request.addProperty("itemid", new Integer(gCommentNewsID));

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.bodyOut = request;
		envelope.dotNet = false;

		HttpTransport ht = new HttpTransport(Database_Webservice.URL
				+ Database_Webservice.ht_params);
		ht.debug = true;

		try {
			ht.call(get_CountComment_SOAP_ACTION, envelope);

			SoapObject result = (SoapObject) envelope.bodyIn;

			RetrieveCountCommentFromSoap(result);
		} catch (Exception ex) {
		}
	}

	private void RetrieveCountCommentFromSoap(SoapObject soap) {
		Vector vector = (Vector) soap.getProperty(0);

		SoapObject getPropertyD = (SoapObject) vector.elementAt(0);

		String commentnumber = getPropertyD.getProperty("count").toString();
		fieldChangeNotify(0);
		commentnolabel.setText(Config_GlobalFunction.commentlabel
				+ commentnumber);
	}
}