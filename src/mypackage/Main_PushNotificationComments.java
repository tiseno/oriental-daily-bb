package mypackage;

import java.util.Vector;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.ListField;
import net.rim.device.api.ui.component.ListFieldCallback;
import net.rim.device.api.ui.component.Menu;
import net.rim.device.api.ui.container.MainScreen;

public class Main_PushNotificationComments extends MainScreen {
	private Database_Webservice webservice;
	private Custom_CommentsListField listfield;

	public Main_PushNotificationComments(int newsid, Vector content) {
		super(USE_ALL_WIDTH | NO_SYSTEM_MENU_ITEMS);
		webservice = new Database_Webservice();
		add(new Custom_PushTopField(this));
		add(new Custom_HeaderField(Config_GlobalFunction.comments));
		add(new Custom_CommentsField(newsid));

		listfield = new Custom_CommentsListField(content);
		add(listfield);
		if (content.size() != 0)
			listfield.setSize(content.size());
		else
			listfield.setSize(1);
	}

	protected void makeMenu(Menu menu, int instance) {

	}

	public class Custom_PushTopField extends Manager {
		private Bitmap close = Config_GlobalFunction.Bitmap("icon_close.png");
		private Bitmap closeactive = Config_GlobalFunction
				.Bitmap("icon_close_active.png");

		private Custom_ButtonField closebtn;
		private Custom_LabelField title;
		private int fontsize;

		Custom_PushTopField(final MainScreen mainscreen) {
			super(Manager.USE_ALL_WIDTH | Manager.NO_VERTICAL_SCROLL
					| Manager.NO_HORIZONTAL_SCROLL);

			if (Display.getWidth() > 480)
				fontsize = 43;
			else if (Display.getWidth() < 481 && Display.getWidth() > 320)
				fontsize = 33;
			else
				fontsize = 23;

			setBackground(Config_GlobalFunction.loadbackground(Display
					.getWidth() + "_" + "header_bar.png"));

			title = new Custom_LabelField(Config_GlobalFunction.pushtitle,
					DrawStyle.ELLIPSIS | LabelField.USE_ALL_WIDTH
							| DrawStyle.VCENTER | DrawStyle.HCENTER,
					Color.WHITE);

			title.setFont(Font.getDefault().derive(Font.BOLD, fontsize));
			add(title);

			closebtn = new Custom_ButtonField(close, closeactive, closeactive);
			closebtn.setChangeListener(new FieldChangeListener() {
				public void fieldChanged(Field field, int context) {
					Main.getUiApplication().popScreen(mainscreen);
				}
			});
			add(closebtn);
		}

		protected void sublayout(int width, int height) {
			int Height = (getPreferredHeight() - close.getHeight()) / 2;
			Field field = getField(0);
			layoutChild(field, getPreferredWidth() / 2,
					getPreferredHeight() - 10);
			setPositionChild(field, (getPreferredWidth() - getField(0)
					.getWidth()) / 2, (getPreferredHeight() - getField(0)
					.getHeight()) / 2);

			field = getField(1);
			layoutChild(field, close.getWidth(), close.getHeight());
			setPositionChild(field, getPreferredWidth()
					- (close.getWidth() + 5), Height);

			width = Math.min(width, getPreferredWidth());
			height = Math.min(height, getPreferredHeight());
			setExtent(width, height);
		}

		public int getPreferredHeight() {
			return Config_GlobalFunction.Bitmap("header_bar.png").getHeight();
		}

		public int getPreferredWidth() {
			return Display.getWidth();
		}

		public void paint(Graphics graphics) {
			int rectHeight = getPreferredHeight();
			int rectWidth = getPreferredWidth();

			graphics.drawRect(0, 0, rectWidth, rectHeight);
			super.paint(graphics);
		}
	}

	public class Custom_CommentsField extends Manager {
		private Custom_LabelField namelabel;
		private ButtonField postbtn;
		private Custom_EditField comments, name;

		Custom_CommentsField(final int newsid) {
			super(Manager.USE_ALL_WIDTH | Manager.NO_VERTICAL_SCROLL
					| Manager.NO_HORIZONTAL_SCROLL);

			setBackground(Config_GlobalFunction
					.loadbackground("background.png"));
			comments = new Custom_EditField(Field.FIELD_HCENTER
					| Field.FIELD_VCENTER | Field.FOCUSABLE,
					getPreferredWidth() - 10, 3);
			add(comments);

			namelabel = new Custom_LabelField(Config_GlobalFunction.namelabel,
					DrawStyle.ELLIPSIS | LabelField.USE_ALL_WIDTH
							| DrawStyle.RIGHT | Field.FIELD_LEFT);
			namelabel.setFontColor(Color.BLACK);
			add(namelabel);

			name = new Custom_EditField(Field.FIELD_HCENTER
					| Field.FIELD_VCENTER | Field.FOCUSABLE,
					getPreferredWidth() / 2, 1);
			add(name);

			postbtn = new ButtonField(Config_GlobalFunction.comments,
					DrawStyle.HCENTER | Field.FIELD_RIGHT) {
				public int getPreferredWidth() {
					return Display.getWidth() / 5;
				}

				public int getPreferredHeight() {
					return Font.getDefault().getHeight() + 5;
				}
			};
			postbtn.setChangeListener(new FieldChangeListener() {
				public void fieldChanged(Field field, int context) {
					if (Config_GlobalFunction.isConnected()) {
						if (!comments.getText().trim().toString().equals("")) {
							String commentnametext;
							if (name.getText().trim().toString().equals(""))
								commentnametext = Config_GlobalFunction.commentposter;
							else
								commentnametext = name.getText().toString();
							String commenttext = comments.getText().toString();
							Config_GlobalFunction.Message(
									Config_GlobalFunction.commentapprove, 1);
							webservice.InsertComment(newsid, commentnametext,
									commenttext, 0);
							Main.getUiApplication().popScreen(
									Main_PushNotificationComments.this);
						} else {
							Config_GlobalFunction.Message(
									Config_GlobalFunction.nocomment, 1);
						}
					} else
						Config_GlobalFunction.Message(
								Config_GlobalFunction.nopostcomment, 1);
				}
			});
			add(postbtn);
		}

		protected void sublayout(int width, int height) {
			Field field = getField(0);
			layoutChild(field, getPreferredWidth() - 10, Font.getDefault()
					.getHeight() * 3);
			setPositionChild(field, 5, 5);

			field = getField(1);
			layoutChild(field, getPreferredWidth() / 5, Font.getDefault()
					.getHeight());
			setPositionChild(field, 5, getField(0).getHeight() + 10);

			field = getField(2);
			layoutChild(field, getPreferredWidth() / 2, Font.getDefault()
					.getHeight());
			setPositionChild(field, getField(1).getWidth() + 7, getField(0)
					.getHeight() + 10);

			field = getField(3);
			layoutChild(field, Display.getWidth() / 5, Font.getDefault()
					.getHeight() + 15);
			setPositionChild(field, (getField(1).getWidth()
					+ getField(2).getWidth() + 12), getField(0).getHeight() + 7);

			width = Math.min(width, getPreferredWidth());
			height = Math.min(height, getPreferredHeight());
			setExtent(width, height);
		}

		public int getPreferredHeight() {
			return getField(0).getHeight() + getField(3).getHeight() + 10;
		}

		public int getPreferredWidth() {
			return Display.getWidth();
		}

		protected void paint(Graphics graphics) {
			int rectHeight = getPreferredHeight();
			int rectWidth = getPreferredWidth();
			graphics.setColor(Color.WHITE);
			graphics.drawRect(0, 0, rectWidth, rectHeight);
			super.paint(graphics);
		}

		protected boolean touchEvent(TouchEvent event) {
			int eventCode = event.getEvent();
			if ((eventCode == TouchEvent.UNCLICK)
					|| (eventCode == TouchEvent.DOWN)) {
				int x = event.getX(1);
				int y = event.getY(1);

				if ((x >= 0) && (y >= 0) && (x < getWidth())
						&& (y < getHeight())) {
					int field = getFieldAtLocation(x, y);
					if (field >= 0) {
						return super.touchEvent(event);
					} else {
						if (eventCode == TouchEvent.UNCLICK) {
							Config_GlobalFunction.hideKeyboard();
						} else {
							setFocus();
						}
						return true;
					}
				}
			}
			return super.touchEvent(event);
		}
	}

	public class Custom_CommentsListField extends ListField {
		private String[] comment, date, poster;
		private List_NewsComment newscomment;

		private Vector content = null;
		private ListCallback callback = null;

		public Custom_CommentsListField(Vector content) {
			this.content = content;
			if (content.size() != 0) {
				comment = new String[content.size()];
				date = new String[content.size()];
				poster = new String[content.size()];

				for (int i = 0; i < content.size(); i++) {
					newscomment = (List_NewsComment) content.elementAt(i);
					comment[i] = newscomment.getCommentContent();
					date[i] = newscomment.getCommentDate();
					poster[i] = newscomment.getUserName();
				}
			} else {
				comment = new String[1];
				poster = new String[1];
				date = new String[1];

				comment[0] = Config_GlobalFunction.commentgmcontent;
				poster[0] = Config_GlobalFunction.commentgmname;
				date[0] = Config_GlobalFunction.date;
			}
			initCallbackListening();
		}

		private void initCallbackListening() {
			callback = new ListCallback();
			this.setCallback(callback);
			this.setRowHeight(-2);
		}

		private class ListCallback implements ListFieldCallback {
			public ListCallback() {
			}

			public void drawListRow(ListField listField, Graphics graphics,
					int index, int y, int width) {
				Vector text = Config_GlobalFunction.wrap(comment[index],
						Display.getWidth() - 10);
				for (int i = 0; i < text.size(); i++) {
					int liney = y + (i * Font.getDefault().getHeight());
					graphics.drawText(
							(String) text.elementAt(i),
							5,
							liney + 3,
							DrawStyle.TOP | DrawStyle.LEFT | DrawStyle.ELLIPSIS,
							Display.getWidth() - 10);
				}

				if (text.size() == 2) {
					graphics.setColor(Color.GRAY);
					graphics.drawText(date[index], 5, y
							+ Font.getDefault().getHeight() + 3);

					graphics.setColor(Color.RED);
					graphics.drawText(poster[index], Display.getWidth()
							- Font.getDefault().getAdvance(poster[index]) - 5,
							y + Font.getDefault().getHeight() + 3);
					setRowHeight(index, getRowHeight() + 9);
				} else if (text.size() == 3) {
					graphics.setColor(Color.GRAY);
					graphics.drawText(date[index], 5, y
							+ Font.getDefault().getHeight() * 2 + 3);

					graphics.setColor(Color.RED);
					graphics.drawText(poster[index], Display.getWidth()
							- Font.getDefault().getAdvance(poster[index]) - 5,
							y + Font.getDefault().getHeight() * 2 + 3);
					setRowHeight(index, getRowHeight() * 15 / 10 + 9);
				}

				graphics.setColor(Color.BLACK);
				graphics.drawRect(0, y, width, getRowHeight(index));
			}

			public Object get(ListField listField, int index) {
				return content.elementAt(index);
			}

			public int getPreferredWidth(ListField listField) {
				return Display.getWidth();
			}

			public int indexOfList(ListField listField, String prefix, int start) {
				return content.indexOf(prefix, start);
			}
		}
	}
}
