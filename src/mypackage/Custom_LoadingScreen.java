package mypackage;

import net.rim.device.api.system.Display;
import net.rim.device.api.system.GIFEncodedImage;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.XYEdges;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;
import net.rim.device.api.ui.decor.Border;
import net.rim.device.api.ui.decor.BorderFactory;

public class Custom_LoadingScreen extends PopupScreen {
	private HorizontalFieldManager image;
	private Util_AnimateGifField anmtFldCycle = null;
	private GIFEncodedImage gifImgCycle;

	public Custom_LoadingScreen(int time) {
		super(new HorizontalFieldManager());
		Background bg = BackgroundFactory.createSolidTransparentBackground(
				Color.BLACK, 190);
		setBackground(bg);
		setBorder(BorderFactory.createSimpleBorder(new XYEdges(),
				Border.STYLE_TRANSPARENT));
		gifImgCycle = (GIFEncodedImage) GIFEncodedImage
				.getEncodedImageResource(Display.getWidth()
						+ "_LoadingSpinner.agif");
		anmtFldCycle = new Util_AnimateGifField(gifImgCycle,
				Field.FIELD_HCENTER);
		image = new HorizontalFieldManager(USE_ALL_WIDTH) {
			protected void sublayout(int maxWidth, int maxHeight) {
				super.sublayout(Display.getWidth(), Display.getHeight());
				setExtent(Display.getWidth(), Display.getHeight());
			}
		};
		int paddingtop = (Display.getHeight() - anmtFldCycle.getBitmapHeight() - Display
				.getHeight() / 10) / 2;
		int paddingleft = (Display.getWidth() - anmtFldCycle.getBitmapWidth() - Display
				.getWidth() / 10) / 2;
		image.setPadding(paddingtop, 0, 0, paddingleft);
		image.add(anmtFldCycle);
		add(image);

		Main.getUiApplication().invokeLater(new Runnable() {
			public void run() {
				Main.getUiApplication().popScreen(Custom_LoadingScreen.this);
			}
		}, time * 1000, false);
	}

	public boolean keyDown(int keycode, int status) {
		if (Keypad.key(keycode) == Keypad.KEY_ESCAPE) {
			Main.getUiApplication().popScreen(this);
			return true;
		}
		return super.keyDown(keycode, status);
	}
}