package mypackage;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.component.BasicEditField;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.Menu;
import net.rim.device.api.ui.container.MainScreen;

public class Menu_Newspaper extends MainScreen {
	private Database_Webservice webservice;
	private Custom_ButtonField financebtn, specialbtn, forumbtn, discussionbtn,
			otherbtn;
	private Custom_ButtonField downloadbtn, refreshbtn, backbtn, newsbtn;
	private Custom_EditField name, cname, email, address, hp, remark;

	public Menu_Newspaper() {
		super(USE_ALL_WIDTH | NO_SYSTEM_MENU_ITEMS);

		webservice = new Database_Webservice();
		add(new Custom_TopField(this, -1, -1, "", 1, 0));
		add(new Custom_BottomField(this, -1));
		add(new Custom_HeaderField(Config_GlobalFunction.newspaper));
		add(new Custom_NewspaperTable());
	}

	protected void makeMenu(Menu menu, int instance) {

	}

	public boolean keyDown(int keycode, int status) {
		if (Keypad.key(keycode) == Keypad.KEY_ESCAPE) {
			Main.getUiApplication().popScreen(this);
			return true;
		}
		return super.keyDown(keycode, status);
	}
	
	public class Custom_TopField extends Manager {
		private Bitmap download = Config_GlobalFunction
				.Bitmap("btn_download.png");
		private Bitmap downloadactive = Config_GlobalFunction
				.Bitmap("btn_download_active.png");
		private Bitmap refresh = Config_GlobalFunction
				.Bitmap("icon_refresh.png");
		private Bitmap refreshactive = Config_GlobalFunction
				.Bitmap("icon_refresh_active.png");
		private Bitmap back = Config_GlobalFunction.Bitmap("btn_back.png");
		private Bitmap backctive = Config_GlobalFunction
				.Bitmap("btn_back_active.png");
		private Bitmap news = Config_GlobalFunction.Bitmap("icon_news.png");
		private Bitmap newsactive = Config_GlobalFunction
				.Bitmap("icon_news_active.png");
		private Bitmap home = Config_GlobalFunction.Bitmap("icon_home.png");

		private BitmapField homeimage;
		private Custom_LabelField title;
		private int left, right, fontsize, position;
		private Database_Webservice webservice;

		Custom_TopField(final MainScreen mainscreen, final int position,
				final int catsid, final String header, int left, int right) {
			super(Manager.USE_ALL_WIDTH | Manager.NO_VERTICAL_SCROLL
					| Manager.NO_HORIZONTAL_SCROLL);
			this.left = left;
			this.right = right;
			this.position = position;

			if (Display.getWidth() > 480)
				fontsize = 43;
			else if (Display.getWidth() < 481 && Display.getWidth() > 320)
				fontsize = 33;
			else
				fontsize = 23;

			webservice = new Database_Webservice();
			setBackground(Config_GlobalFunction.loadbackground(Display
					.getWidth() + "_" + "header_bar.png"));

			if (left == 1) {
				newsbtn = new Custom_ButtonField(news, newsactive, newsactive);
				newsbtn.setChangeListener(new FieldChangeListener() {
					public void fieldChanged(Field field, int context) {
						Main.getUiApplication().pushScreen(
								new Menu_PopupMenu(position));
					}
				});
				add(newsbtn);
			} else if (left == 2) {
				backbtn = new Custom_ButtonField(back, backctive, backctive);
				backbtn.setChangeListener(new FieldChangeListener() {
					public void fieldChanged(Field field, int context) {
						Main.getUiApplication().popScreen(mainscreen);
					}
				});
				add(backbtn);
			}

			if (position != 0) {
				title = new Custom_LabelField(Config_GlobalFunction.maintitle,
						DrawStyle.ELLIPSIS | LabelField.USE_ALL_WIDTH
								| DrawStyle.VCENTER | Field.FOCUSABLE
								| DrawStyle.LEFT | ButtonField.CONSUME_CLICK,
						Color.WHITE) {
					protected boolean navigationClick(int status, int time) {
						Main.getUiApplication().pushScreen(
								new Custom_LoadingScreen(1));
						Main.getUiApplication().invokeLater(new Runnable() {
							public void run() {
								Main.getUiApplication().pushScreen(
										new Main_AllLatestNews());
							}
						}, 1 * 1000, false);
						return true;
					}
				};
			} else {
				title = new Custom_LabelField(Config_GlobalFunction.maintitle,
						DrawStyle.ELLIPSIS | LabelField.USE_ALL_WIDTH
								| DrawStyle.HCENTER | DrawStyle.VCENTER,
						Color.WHITE);
			}
			title.setFont(Font.getDefault().derive(Font.BOLD, fontsize));
			add(title);

			if (right == 1) {
				downloadbtn = new Custom_ButtonField(download, downloadactive,
						downloadactive);
				downloadbtn.setChangeListener(new FieldChangeListener() {
					public void fieldChanged(Field field, int context) {
						if (Config_GlobalFunction
								.Dialog(Config_GlobalFunction.alertdownload)) {
							if (Config_GlobalFunction.isConnected()) {
								Config_GlobalFunction.Message(
										Config_GlobalFunction.downloading, 10);
								Main.getUiApplication().invokeLater(
										new Runnable() {
											public void run() {
												webservice.UpdateAllCatNews(
														false, -1);
											}
										}, 1 * 1000, false);

							} else
								Config_GlobalFunction.Message(
										Config_GlobalFunction.nowifi, 1);
						} else
							Config_GlobalFunction.CloseDialog();
					}
				});
				add(downloadbtn);
			} else if (right == 2) {
				refreshbtn = new Custom_ButtonField(refresh, refreshactive,
						refreshactive);
				refreshbtn.setChangeListener(new FieldChangeListener() {
					public void fieldChanged(Field field, int context) {
						if (Config_GlobalFunction.isConnected()) {
							Config_GlobalFunction.Message(
									Config_GlobalFunction.refreshing, 10);
							Main.getUiApplication().invokeLater(new Runnable() {
								public void run() {
									webservice.refreshCatNewsindex(catsid,
											position, header);
								}
							});
						} else
							Config_GlobalFunction.Message(
									Config_GlobalFunction.nowifi, 1);
					}
				});
				add(refreshbtn);
			}

			if (position != 0) {
				homeimage = new BitmapField(home);
				add(homeimage);
			}
		}

		protected void sublayout(int width, int height) {
			int Height = (getPreferredHeight() - back.getHeight()) / 2;
			Field field = getField(0);

			if (left == 1) {
				layoutChild(field, back.getWidth(), back.getHeight());
				setPositionChild(field, 5, Height);
			} else if (left == 2) {
				layoutChild(field, news.getWidth(), news.getHeight());
				setPositionChild(field, 5, Height);
			}

			field = getField(1);
			if (position != 0) {
				layoutChild(field, getPreferredWidth() / 3,
						getPreferredHeight() - 10);
				setPositionChild(field, (getPreferredWidth()
						- getField(1).getWidth() + homeimage.getBitmapWidth())
						/ 2 + homeimage.getBitmapWidth(),
						(getPreferredHeight() - getField(1).getHeight()) / 2);

				field = getField(getFieldCount() - 1);
				layoutChild(field, homeimage.getBitmapWidth(),
						homeimage.getBitmapHeight());
				setPositionChild(
						field,
						(getPreferredWidth() - getField(1).getWidth() + homeimage
								.getBitmapWidth()) / 2,
						(getPreferredHeight() - homeimage.getBitmapHeight()) / 2);
			} else {
				layoutChild(field, getPreferredWidth() / 3,
						getPreferredHeight() - 10);
				setPositionChild(field, (getPreferredWidth() - getField(1)
						.getWidth()) / 2, (getPreferredHeight() - getField(1)
						.getHeight()) / 2);
			}

			field = getField(2);
			if (right == 1) {
				layoutChild(field, download.getWidth(), download.getHeight());
				setPositionChild(field,
						getPreferredWidth() - (download.getWidth() + 5), Height);
			} else if (right == 2) {
				layoutChild(field, refresh.getWidth(), refresh.getHeight());
				setPositionChild(field,
						getPreferredWidth() - (refresh.getWidth() + 5), Height);
			}

			width = Math.min(width, getPreferredWidth());
			height = Math.min(height, getPreferredHeight());
			setExtent(width, height);
		}

		public int getPreferredHeight() {
			return Config_GlobalFunction.Bitmap("header_bar.png").getHeight();
		}

		public int getPreferredWidth() {
			return Display.getWidth();
		}

		public void paint(Graphics graphics) {
			int rectHeight = getPreferredHeight();
			int rectWidth = getPreferredWidth();

			graphics.drawRect(0, 0, rectWidth, rectHeight);
			super.paint(graphics);
		}

		protected boolean navigationMovement(int dx, int dy, int status,
				int time) {
			int focusIndex = getFieldWithFocusIndex();

			while (dy > 0) {
				Field f = financebtn;
				if (f.isFocusable()) {
					f.setFocus();
					dy--;
				}
			}

			while (dy < 0) {
				return false;
			}

			while (dx > 0) {
				focusIndex++;

				if (focusIndex >= getFieldCount()) {
					return false;
				} else {
					Field f = getField(focusIndex);

					if (f.isFocusable()) {
						f.setFocus();
						dx--;
					}
				}
			}

			while (dx < 0) {
				focusIndex--;

				if (focusIndex < 0) {
					return false;
				} else {
					Field f = getField(focusIndex);

					if (f.isFocusable()) {
						f.setFocus();
						dx++;
					}
				}
			}

			return true;
		}
	}

	public class Custom_BottomField extends Manager {
		private Bitmap finance = Config_GlobalFunction
				.Bitmap("icon_economy.png");
		private Bitmap financeactive = Config_GlobalFunction
				.Bitmap("icon_economy_active.png");
		private Bitmap special = Config_GlobalFunction.Bitmap("icon_title.png");
		private Bitmap specialactive = Config_GlobalFunction
				.Bitmap("icon_title_active.png");
		private Bitmap forum = Config_GlobalFunction.Bitmap("icon_forum.png");
		private Bitmap forumactive = Config_GlobalFunction
				.Bitmap("icon_forum_active.png");
		private Bitmap discussion = Config_GlobalFunction
				.Bitmap("icon_discussion.png");
		private Bitmap discussionactive = Config_GlobalFunction
				.Bitmap("icon_discussion_active.png");
		private Bitmap other = Config_GlobalFunction.Bitmap("icon_other.png");
		private Bitmap otheractive = Config_GlobalFunction
				.Bitmap("icon_other_active.png");

		Custom_BottomField(final MainScreen mainscreen, final int thisid) {
			super(Manager.USE_ALL_WIDTH | Manager.NO_VERTICAL_SCROLL
					| Manager.NO_HORIZONTAL_SCROLL);
			setBackground(Config_GlobalFunction.loadbackground(Display
					.getWidth() + "_" + "footer_bar.png"));
			if (thisid != Config_GlobalFunction.menu_financecat) {
				financebtn = new Custom_ButtonField(finance, financeactive,
						financeactive);
				financebtn.setChangeListener(new FieldChangeListener() {
					public void fieldChanged(Field field, int context) {
						Main.getUiApplication().pushScreen(
								new Custom_LoadingScreen(1));
						Main.getUiApplication().invokeLater(new Runnable() {
							public void run() {
								Main.getUiApplication()
										.pushScreen(
												new Main_ParticularCategoryAllNews(
														Config_GlobalFunction.menu_financecat,
														2,
														Config_GlobalFunction.menu_finance));
							}
						}, 1 * 1000, false);
					}
				});
			} else
				financebtn = new Custom_ButtonField(financeactive,
						financeactive, financeactive);
			add(financebtn);

			if (thisid != Config_GlobalFunction.menu_specialcat) {
				specialbtn = new Custom_ButtonField(special, specialactive,
						specialactive);
				specialbtn.setChangeListener(new FieldChangeListener() {
					public void fieldChanged(Field field, int context) {
						Main.getUiApplication().pushScreen(
								new Custom_LoadingScreen(1));
						Main.getUiApplication().invokeLater(new Runnable() {
							public void run() {
								Main.getUiApplication()
										.pushScreen(
												new Main_ParticularCategoryAllNews(
														Config_GlobalFunction.menu_specialcat,
														9,
														Config_GlobalFunction.menu_special));
							}
						}, 1 * 1000, false);
					}
				});
			} else
				specialbtn = new Custom_ButtonField(specialactive,
						specialactive, specialactive);
			add(specialbtn);

			if (thisid != Config_GlobalFunction.menu_forumcat) {
				forumbtn = new Custom_ButtonField(forum, forumactive,
						forumactive);
				forumbtn.setChangeListener(new FieldChangeListener() {
					public void fieldChanged(Field field, int context) {
						Main.getUiApplication().pushScreen(
								new Custom_LoadingScreen(1));
						Main.getUiApplication().invokeLater(new Runnable() {
							public void run() {
								Main.getUiApplication()
										.pushScreen(
												new Main_ParticularCategoryAllNews(
														Config_GlobalFunction.menu_forumcat,
														-1,
														Config_GlobalFunction.menu_forum));
							}
						}, 1 * 1000, false);
					}
				});
			} else
				forumbtn = new Custom_ButtonField(forumactive, forumactive,
						forumactive);
			add(forumbtn);

			if (thisid != Config_GlobalFunction.menu_discussioncat) {
				discussionbtn = new Custom_ButtonField(discussion,
						discussionactive, discussionactive);
				discussionbtn.setChangeListener(new FieldChangeListener() {
					public void fieldChanged(Field field, int context) {
						Main.getUiApplication().pushScreen(
								new Custom_LoadingScreen(1));
						Main.getUiApplication().invokeLater(new Runnable() {
							public void run() {
								Main.getUiApplication()
										.pushScreen(
												new Main_ParticularCategoryAllNews(
														Config_GlobalFunction.menu_discussioncat,
														5,
														Config_GlobalFunction.menu_discussion));
							}
						}, 1 * 1000, false);
					}
				});
			} else
				discussionbtn = new Custom_ButtonField(discussionactive,
						discussionactive, discussionactive);
			add(discussionbtn);

			if (thisid != 20) {
				otherbtn = new Custom_ButtonField(other, otheractive,
						otheractive);
				otherbtn.setChangeListener(new FieldChangeListener() {
					public void fieldChanged(Field field, int context) {
						Main.getUiApplication().pushScreen(
								new Custom_LoadingScreen(1));
						Main.getUiApplication().invokeLater(new Runnable() {
							public void run() {
								Main.getUiApplication().pushScreen(
										new Menu_Others());
							}
						}, 1 * 1000, false);
					}
				});
			} else
				otherbtn = new Custom_ButtonField(otheractive, otheractive,
						otheractive);
			add(otherbtn);
		}

		protected void sublayout(int width, int height) {
			int Height = (getPreferredHeight() - finance.getHeight()) / 2;

			Field field = getField(0);
			layoutChild(field, finance.getWidth(), finance.getHeight());
			setPositionChild(field, getGap(), Height);
			field = getField(1);
			layoutChild(field, special.getWidth(), special.getHeight());
			setPositionChild(field, getPreferredWidth() / getFieldCount()
					+ getGap(), Height);

			field = getField(2);
			layoutChild(field, forum.getWidth(), forum.getHeight());
			setPositionChild(field, getPreferredWidth() * 2 / getFieldCount()
					+ getGap(), Height);

			field = getField(3);
			layoutChild(field, discussion.getWidth(), discussion.getHeight());
			setPositionChild(field, getPreferredWidth() * 3 / getFieldCount()
					+ getGap(), Height);

			field = getField(4);
			layoutChild(field, other.getWidth(), other.getHeight());
			setPositionChild(field, getPreferredWidth() * 4 / getFieldCount()
					+ getGap(), Height);

			width = Math.min(width, getPreferredWidth());
			height = Math.min(height, getPreferredHeight());
			setExtent(width, height);
		}

		public int getPreferredHeight() {
			return Config_GlobalFunction.Bitmap("footer_bar.png").getHeight();
		}

		public int getPreferredWidth() {
			return Display.getWidth();
		}

		protected void paint(Graphics graphics) {
			int rectHeight = getPreferredHeight();
			int rectWidth = getPreferredWidth();
			graphics.drawRect(0, 0, rectWidth, rectHeight);
			super.paint(graphics);
		}

		private int getGap() {
			return ((getPreferredWidth() / getFieldCount()) - finance
					.getWidth()) / 2;
		}

		protected boolean navigationMovement(int dx, int dy, int status,
				int time) {
			int focusIndex = getFieldWithFocusIndex();

			while (dy > 0) {
				Field f = cname;
				if (f.isFocusable()) {
					f.setFocus();
					dy--;
				}
			}

			while (dy < 0) {
				Field f = newsbtn;
				if (f.isFocusable()) {
					f.setFocus();
					dy++;
				}
			}

			while (dx > 0) {
				focusIndex++;

				if (focusIndex >= getFieldCount()) {
					return false;
				} else {
					Field f = getField(focusIndex);

					if (f.isFocusable()) {
						f.setFocus();
						dx--;
					}
				}
			}

			while (dx < 0) {
				focusIndex--;

				if (focusIndex < 0) {
					return false;
				} else {
					Field f = getField(focusIndex);

					if (f.isFocusable()) {
						f.setFocus();
						dx++;
					}
				}
			}

			return true;
		}
	}

	public class Custom_NewspaperTable extends Manager {
		private LabelField namelabel, cnamelabel, emaillabel, addresslabel,
				hplabel, remarklabel;		
		private ButtonField submitbtn, cancelbtn;
		private int labelwidth = getPreferredWidth() * 1 / 3,
				editwidth = getPreferredWidth() / 2;

		public Custom_NewspaperTable() {
			super(Manager.USE_ALL_WIDTH | Manager.VERTICAL_SCROLL);
			setBackground(Config_GlobalFunction
					.loadbackground("background.png"));

			cnamelabel = new LabelField(Config_GlobalFunction.cnamelabel,
					DrawStyle.ELLIPSIS | LabelField.USE_ALL_WIDTH
							| DrawStyle.RIGHT);
			add(cnamelabel);

			namelabel = new LabelField(Config_GlobalFunction.enamelabel,
					DrawStyle.ELLIPSIS | LabelField.USE_ALL_WIDTH
							| DrawStyle.RIGHT);
			add(namelabel);

			addresslabel = new LabelField(Config_GlobalFunction.addresslabel,
					DrawStyle.ELLIPSIS | LabelField.USE_ALL_WIDTH
							| DrawStyle.RIGHT);
			add(addresslabel);

			emaillabel = new LabelField(Config_GlobalFunction.pemaillabel,
					DrawStyle.ELLIPSIS | LabelField.USE_ALL_WIDTH
							| DrawStyle.RIGHT);
			add(emaillabel);

			hplabel = new LabelField(Config_GlobalFunction.hplabel,
					DrawStyle.ELLIPSIS | LabelField.USE_ALL_WIDTH
							| DrawStyle.RIGHT);
			add(hplabel);

			remarklabel = new LabelField(Config_GlobalFunction.remarklabel,
					DrawStyle.ELLIPSIS | LabelField.USE_ALL_WIDTH
							| DrawStyle.RIGHT);
			add(remarklabel);

			cname = new Custom_EditField(Field.FIELD_HCENTER
					| Field.FIELD_VCENTER | Field.FOCUSABLE
					| EditField.NO_NEWLINE, editwidth, 1);
			add(cname);

			name = new Custom_EditField(Field.FIELD_HCENTER
					| Field.FIELD_VCENTER | Field.FOCUSABLE
					| EditField.NO_NEWLINE, editwidth, 1);
			add(name);

			address = new Custom_EditField(Field.FIELD_HCENTER
					| Field.FIELD_VCENTER | Field.FOCUSABLE
					| EditField.NO_NEWLINE, editwidth, 1);
			add(address);

			email = new Custom_EditField(Field.FIELD_HCENTER
					| Field.FIELD_VCENTER | Field.FOCUSABLE
					| EditField.NO_NEWLINE | BasicEditField.FILTER_EMAIL,
					editwidth, 1);
			add(email);

			hp = new Custom_EditField(Field.FIELD_HCENTER | Field.FIELD_VCENTER
					| Field.FOCUSABLE | EditField.NO_NEWLINE
					| BasicEditField.FILTER_PHONE, editwidth, 1);
			add(hp);

			remark = new Custom_EditField(Field.FIELD_HCENTER
					| Field.FIELD_VCENTER | Field.FOCUSABLE
					| EditField.NO_NEWLINE, editwidth, 3);
			add(remark);

			submitbtn = new ButtonField(Config_GlobalFunction.submit,
					Field.FOCUSABLE | Field.FIELD_HCENTER | DrawStyle.VCENTER
							| DrawStyle.HCENTER | ButtonField.CONSUME_CLICK) {
				public int getPreferredWidth() {
					return editwidth * 2 / 5;
				}
			};
			submitbtn.setChangeListener(new FieldChangeListener() {
				public void fieldChanged(Field field, int context) {
					if (Config_GlobalFunction.isConnected()) {
						if (!name.getText().trim().toString().equals("")) {
							if (!address.getText().trim().toString().equals("")) {
								if (!email.getText().trim().toString()
										.equals("")) {
									if (!hp.getText().trim().toString()
											.equals("")) {
										String cnametext = "";
										if (cname.getText().trim().toString()
												.equals(""))
											cnametext = Config_GlobalFunction.commentposter;
										else
											cnametext = cname.getText()
													.toString();

										String enametext = name.getText()
												.toString();
										String addresstext = address.getText()
												.toString();
										String emailtext = email.getText()
												.toString();
										String hptext = hp.getText().toString();
										String remarktext = remark.getText()
												.toString();
										Config_GlobalFunction
												.Message(
														Config_GlobalFunction.newspapersubmit,
														1);
										webservice.InsertOrder("Newspaper",
												cnametext, enametext,
												addresstext, emailtext, hptext,
												remarktext);
										Main.getUiApplication().pushScreen(
												new Custom_LoadingScreen(1));
										Main.getUiApplication().invokeLater(
												new Runnable() {
													public void run() {
														Main.getUiApplication()
																.popScreen(
																		Menu_Newspaper.this);
													}
												}, 1 * 1000, false);
									} else {
										Config_GlobalFunction.Message(
												Config_GlobalFunction.nohphone,
												1);
									}
								} else {
									Config_GlobalFunction.Message(
											Config_GlobalFunction.noemail, 1);
								}
							} else {
								Config_GlobalFunction.Message(
										Config_GlobalFunction.noaddress, 1);
							}
						} else {
							Config_GlobalFunction.Message(
									Config_GlobalFunction.noname, 1);
						}
					} else
						Config_GlobalFunction.Message(
								Config_GlobalFunction.nosubmit, 1);
				}
			});
			add(submitbtn);

			cancelbtn = new ButtonField(Config_GlobalFunction.cancel,
					Field.FOCUSABLE | Field.FIELD_HCENTER | DrawStyle.VCENTER
							| DrawStyle.HCENTER | ButtonField.CONSUME_CLICK) {
				public int getPreferredWidth() {
					return editwidth * 2 / 5;
				}
			};
			cancelbtn.setChangeListener(new FieldChangeListener() {
				public void fieldChanged(Field field, int context) {
					if (Config_GlobalFunction
							.Dialog(Config_GlobalFunction.alertnewspaper)) {
						Main.getUiApplication().popScreen(Menu_Newspaper.this);
						Config_GlobalFunction.CloseDialog();
					} else
						Config_GlobalFunction.CloseDialog();
				}
			});
			add(cancelbtn);
		}

		protected void sublayout(int width, int height) {
			Field field = getField(0);
			layoutChild(field, labelwidth, Font.getDefault().getHeight());
			setPositionChild(field, 5, 30);

			field = getField(6);
			layoutChild(field, editwidth, Font.getDefault().getHeight());
			setPositionChild(field, labelwidth + 2, 30);

			field = getField(1);
			layoutChild(field, labelwidth, Font.getDefault().getHeight());
			setPositionChild(field, 5, (getField(6).getHeight() + 10) + 30);

			field = getField(2);
			layoutChild(field, labelwidth, Font.getDefault().getHeight());
			setPositionChild(field, 5, (getField(6).getHeight() + 10) * 2 + 30);

			field = getField(3);
			layoutChild(field, labelwidth, Font.getDefault().getHeight());
			setPositionChild(field, 5, (getField(6).getHeight() + 10) * 3 + 30);

			field = getField(4);
			layoutChild(field, labelwidth, Font.getDefault().getHeight());
			setPositionChild(field, 5, (getField(6).getHeight() + 10) * 4 + 30);

			field = getField(5);
			layoutChild(field, labelwidth, Font.getDefault().getHeight());
			setPositionChild(field, 5, (getField(6).getHeight() + 10) * 5 + 30);

			field = getField(7);
			layoutChild(field, editwidth, Font.getDefault().getHeight());
			setPositionChild(field, labelwidth + 2,
					(getField(6).getHeight() + 10) + 30);

			field = getField(8);
			layoutChild(field, editwidth, Font.getDefault().getHeight());
			setPositionChild(field, labelwidth + 2,
					(getField(6).getHeight() + 10) * 2 + 30);

			field = getField(9);
			layoutChild(field, editwidth, Font.getDefault().getHeight());
			setPositionChild(field, labelwidth + 2,
					(getField(6).getHeight() + 10) * 3 + 30);

			field = getField(10);
			layoutChild(field, editwidth, Font.getDefault().getHeight());
			setPositionChild(field, labelwidth + 2,
					(getField(6).getHeight() + 10) * 4 + 30);

			field = getField(11);
			layoutChild(field, editwidth, Font.getDefault().getHeight() * 3);
			setPositionChild(field, labelwidth + 2,
					(getField(6).getHeight() + 10) * 5 + 30);

			field = getField(12);
			layoutChild(field, editwidth * 2 / 5,
					Font.getDefault().getHeight() + 15);
			setPositionChild(
					field,
					(getPreferredWidth() - field.getWidth() * 2) / 3,
					(getField(6).getHeight() + 10) * 5
							+ remark.getContentHeight() + 40);

			field = getField(13);
			layoutChild(field, editwidth * 2 / 5,
					Font.getDefault().getHeight() + 15);
			setPositionChild(field,
					(getPreferredWidth() - field.getWidth() * 2) * 2 / 3
							+ field.getWidth(), (getField(6).getHeight() + 10)
							* 5 + remark.getContentHeight() + 40);

			width = Math.min(width, getPreferredWidth());
			height = Math.min(height, getPreferredHeight());
			setExtent(width, height);
		}

		public int getPreferredHeight() {
			return getField(6).getHeight() + getField(7).getHeight()
					+ getField(8).getHeight() + getField(9).getHeight()
					+ getField(10).getHeight() + getField(11).getHeight()
					+ getField(12).getHeight() + 100;
		}

		public int getPreferredWidth() {
			return Display.getWidth();
		}

		protected void paint(Graphics graphics) {
			int rectHeight = getPreferredHeight();
			int rectWidth = getPreferredWidth();
			graphics.drawRect(0, 0, rectWidth, rectHeight);
			super.paint(graphics);
		}

		protected boolean touchEvent(TouchEvent event) {
			int eventCode = event.getEvent();
			if ((eventCode == TouchEvent.UNCLICK)
					|| (eventCode == TouchEvent.DOWN)) {
				int x = event.getX(1);
				int y = event.getY(1);

				if ((x >= 0) && (y >= 0) && (x < getWidth())
						&& (y < getHeight())) {
					int field = getFieldAtLocation(x, y);
					if (field >= 0) {
						return super.touchEvent(event);
					} else {
						if (eventCode == TouchEvent.UNCLICK) {
							Config_GlobalFunction.hideKeyboard();
						} else {
							setFocus();
						}
						return true;
					}
				}
			}
			return super.touchEvent(event);
		}
	}
}
