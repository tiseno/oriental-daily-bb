package mypackage;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.XYEdges;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;
import net.rim.device.api.ui.decor.Border;
import net.rim.device.api.ui.decor.BorderFactory;

public class Menu_PopupMenu extends PopupScreen {
	private Bitmap icon0 = Config_GlobalFunction.Bitmap("icon_0.png");
	private Bitmap iconactive0 = Config_GlobalFunction
			.Bitmap("icon_0_active.png");
	private Bitmap icon1 = Config_GlobalFunction.Bitmap("icon_1.png");
	private Bitmap iconactive1 = Config_GlobalFunction
			.Bitmap("icon_1_active.png");
	private Bitmap icon2 = Config_GlobalFunction.Bitmap("icon_2.png");
	private Bitmap iconactive2 = Config_GlobalFunction
			.Bitmap("icon_2_active.png");
	private Bitmap icon3 = Config_GlobalFunction.Bitmap("icon_3.png");
	private Bitmap iconactive3 = Config_GlobalFunction
			.Bitmap("icon_3_active.png");
	private Bitmap icon4 = Config_GlobalFunction.Bitmap("icon_4.png");
	private Bitmap iconactive4 = Config_GlobalFunction
			.Bitmap("icon_4_active.png");
	private Bitmap icon5 = Config_GlobalFunction.Bitmap("icon_5.png");
	private Bitmap iconactive5 = Config_GlobalFunction
			.Bitmap("icon_5_active.png");
	private Bitmap icon6 = Config_GlobalFunction.Bitmap("icon_6.png");
	private Bitmap iconactive6 = Config_GlobalFunction
			.Bitmap("icon_6_active.png");
	private Bitmap icon7 = Config_GlobalFunction.Bitmap("icon_7.png");
	private Bitmap iconactive7 = Config_GlobalFunction
			.Bitmap("icon_7_active.png");
	private Bitmap icon8 = Config_GlobalFunction.Bitmap("icon_8.png");
	private Bitmap iconactive8 = Config_GlobalFunction
			.Bitmap("icon_8_active.png");
	private Bitmap icon9 = Config_GlobalFunction.Bitmap("icon_9.png");
	private Bitmap iconactive9 = Config_GlobalFunction
			.Bitmap("icon_9_active.png");
	private Bitmap icon10 = Config_GlobalFunction.Bitmap("icon_10.png");
	private Bitmap iconactive10 = Config_GlobalFunction
			.Bitmap("icon_10_active.png");

	private int columnwidth[] = { Display.getWidth() / 5,
			Display.getWidth() / 5, Display.getWidth() / 5,
			Display.getWidth() / 5 };
	private int rowheight = Display.getHeight() / 4;
	private Custom_GridField menu;
	private Database_Webservice webservice;
	private int[] catnewsid, id, homeid = { 0 };
	private String[] cattitle, title,
			hometitle = { Config_GlobalFunction.menu_home };
	private int total;
	private List_CategoryNews news;

	public Menu_PopupMenu(final int position) {
		super(new VerticalFieldManager(), NO_SYSTEM_MENU_ITEMS);
		setBackground(BackgroundFactory.createSolidTransparentBackground(
				0x000000, 255));
		setBorder(BorderFactory.createSimpleBorder(new XYEdges(),
				Border.STYLE_TRANSPARENT));

		menu = new Custom_GridField(this, position, columnwidth, rowheight,
				Field.FIELD_HCENTER | Field.FIELD_VCENTER | Field.FOCUSABLE);
		menu.add(new Custom_ButtonField(icon10, iconactive10, iconactive10));
		menu.add(new Custom_ButtonField(icon0, iconactive0, iconactive0));
		menu.add(new Custom_ButtonField(icon1, iconactive1, iconactive1));
		menu.add(new Custom_ButtonField(icon2, iconactive2, iconactive2));
		menu.add(new Custom_ButtonField(icon3, iconactive3, iconactive3));
		menu.add(new Custom_ButtonField(icon4, iconactive4, iconactive4));
		menu.add(new Custom_ButtonField(icon5, iconactive5, iconactive5));
		menu.add(new Custom_ButtonField(icon6, iconactive6, iconactive6));
		menu.add(new Custom_ButtonField(icon7, iconactive7, iconactive7));
		menu.add(new Custom_ButtonField(icon8, iconactive8, iconactive8));
		menu.add(new Custom_ButtonField(icon9, iconactive9, iconactive9));
		add(menu);
	}

	public boolean keyDown(int keycode, int status) {
		if (Keypad.key(keycode) == Keypad.KEY_ESCAPE) {
			Main.getUiApplication().popScreen(this);
			return true;
		}
		return super.keyDown(keycode, status);
	}

	public class Custom_GridField extends Manager {
		private int[] columnWidths;
		private int position;
		private int allRowHeight = -1;
		private PopupScreen mainscreen;

		public Custom_GridField(PopupScreen mainscreen, int position,
				int[] columnWidths, int rowHeight, long style) {
			super(style | NO_SYSTEM_MENU_ITEMS);
			this.mainscreen = mainscreen;
			this.columnWidths = columnWidths;
			this.allRowHeight = rowHeight;
			this.position = position;

			webservice = new Database_Webservice();
			webservice.LoadCategoryTitle();
			id = new int[webservice.newsCat.size()];
			title = new String[webservice.newsCat.size()];
			for (int i = 0; i < webservice.newsCat.size(); i++) {
				news = (List_CategoryNews) webservice.newsCat.elementAt(i);
				id[i] = news.getCatID();
				title[i] = news.getNewCatName();
			}

			total = id.length + homeid.length;
			catnewsid = new int[total];
			cattitle = new String[total];
			for (int i = 0; i < homeid.length; i++) {
				catnewsid[i] = homeid[i];
				cattitle[i] = hometitle[i];
			}
			for (int i = 0; i < id.length; i++) {
				catnewsid[homeid.length + i] = id[i];
				cattitle[homeid.length + i] = title[i];
			}

		}

		protected boolean navigationMovement(int dx, int dy, int status,
				int time) {
			int focusIndex = getFieldWithFocusIndex();

			while (dy > 0) {
				if (focusIndex + columnwidth.length >= getFieldCount()) {
					return false;
				} else {
					Field f = getField(focusIndex + columnwidth.length);
					if (f.isFocusable()) {
						f.setFocus();
						dy--;
					}
				}
			}

			while (dy < 0) {
				if (focusIndex - columnwidth.length < 0) {
					return false;
				} else {
					Field f = getField(focusIndex - columnwidth.length);

					if (f.isFocusable()) {
						f.setFocus();
						dy++;
					}
				}
			}

			while (dx > 0) {
				focusIndex++;

				if (focusIndex >= getFieldCount()) {
					return false;
				} else {
					Field f = getField(focusIndex);

					if (f.isFocusable()) {
						f.setFocus();
						dx--;
					}
				}
			}

			while (dx < 0) {
				focusIndex--;

				if (focusIndex < 0) {
					return false;
				} else {
					Field f = getField(focusIndex);

					if (f.isFocusable()) {
						f.setFocus();
						dx++;
					}
				}
			}

			return true;
		}

		protected void sublayout(int width, int height) {
			int y = 0;

			Field[] fields = new Field[columnWidths.length];
			int currentColumn = 0;
			int rowHeight = 0;

			for (int i = 0; i < getFieldCount(); i++) {
				fields[currentColumn] = getField(i);
				fields[currentColumn]
						.setChangeListener(new FieldChangeListener() {
							public void fieldChanged(Field field, int context) {
								final int focusIndex = getFieldWithFocusIndex();
								if (focusIndex == position) {
									Main.getUiApplication().popScreen(
											mainscreen);
								} else {
									Main.getUiApplication().popScreen(
											mainscreen);
									Main.getUiApplication().pushScreen(
											new Custom_LoadingScreen(1));
									Main.getUiApplication().invokeLater(
											new Runnable() {
												public void run() {
													if (focusIndex == 0)
														Main.getUiApplication()
																.pushScreen(
																		new Main_AllLatestNews());
													else
														Main.getUiApplication()
																.pushScreen(
																		new Main_ParticularCategoryAllNews(
																				catnewsid[focusIndex],
																				focusIndex,
																				cattitle[focusIndex]));
												}
											}, 1 * 1000, false);
								}
							}
						});
				layoutChild(fields[currentColumn], columnWidths[currentColumn],
						height - y);

				if (fields[currentColumn].getHeight() > rowHeight) {
					rowHeight = fields[currentColumn].getHeight() + 10;
				}

				currentColumn++;

				if ((currentColumn == columnWidths.length)
						|| (i == (getFieldCount() - 1))) {
					int x = 0;

					if (this.allRowHeight >= 0) {
						rowHeight = this.allRowHeight;
					}

					for (int c = 0; c < currentColumn; c++) {
						long fieldStyle = fields[c].getStyle();
						int fieldXOffset = 0;
						long fieldHalign = fieldStyle & Field.FIELD_HALIGN_MASK;

						if (fieldHalign == Field.FIELD_RIGHT) {
							fieldXOffset = columnWidths[c]
									- fields[c].getWidth();
						} else if (fieldHalign == Field.FIELD_HCENTER) {
							fieldXOffset = (columnWidths[c] - fields[c]
									.getWidth()) / 2;
						}

						int fieldYOffset = 0;
						long fieldValign = fieldStyle & Field.FIELD_VALIGN_MASK;

						if (fieldValign == Field.FIELD_BOTTOM) {
							fieldYOffset = rowHeight - fields[c].getHeight();
						} else if (fieldValign == Field.FIELD_VCENTER) {
							fieldYOffset = (rowHeight - fields[c].getHeight()) / 2;
						}

						setPositionChild(fields[c], x + fieldXOffset, y
								+ fieldYOffset);
						x += columnWidths[c];
					}

					currentColumn = 0;
					y += rowHeight;
				}

				if (y >= height) {
					break;
				}
			}

			int totalWidth = 0;

			for (int i = 0; i < columnWidths.length; i++) {
				totalWidth += columnWidths[i];
			}

			if (position > -1) {
				Field f = getField(position);
				f.setFocus();
			}

			setExtent(totalWidth, Math.min(y, height));
		}
	}
}
