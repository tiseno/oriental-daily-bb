package mypackage;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.Menu;
import net.rim.device.api.ui.component.SeparatorField;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class Menu_SMS extends MainScreen {
	private Custom_LabelField digilabel, maxislabel, celcomlabel, umobilelabel,
			digititle, maxistitle, celcomtitle, umobiletitle;
	private int fonttitle, fontcontent;
	private Custom_ButtonField financebtn, specialbtn, forumbtn, discussionbtn,
			otherbtn;
	private Custom_ButtonField downloadbtn, refreshbtn, backbtn, newsbtn;

	public Menu_SMS() {
		super(USE_ALL_WIDTH | NO_SYSTEM_MENU_ITEMS);

		if (Display.getWidth() > 480) {
			fonttitle = 30;
			fontcontent = 25;
		} else if (Display.getWidth() < 481 && Display.getWidth() > 320) {
			fonttitle = 25;
			fontcontent = 20;
		} else {
			fonttitle = 20;
			fontcontent = 15;
		}

		add(new Custom_TopField(this, -1, -1, "", 1, 0));
		add(new Custom_BottomField(this, -1));
		add(new Custom_HeaderField(Config_GlobalFunction.sms));

		VerticalFieldManager vrt = new VerticalFieldManager(USE_ALL_WIDTH
				| VERTICAL_SCROLL | VERTICAL_SCROLLBAR) {
			protected void sublayout(int maxWidth, int maxHeight) {

				super.sublayout(Display.getWidth(), getPreferredHeight());
				setExtent(Display.getWidth(), getPreferredHeight());
			}

			public int getPreferredHeight() {
				return Display.getHeight();
			}
		};
		vrt.setBackground(Config_GlobalFunction
				.loadbackground("background.png"));

		digititle = new Custom_LabelField(Config_GlobalFunction.digilabel,
				LabelField.USE_ALL_WIDTH | DrawStyle.LEFT | Field.FOCUSABLE,
				Color.BLACK, Color.BLACK);
		digititle.setFont(Font.getDefault().derive(Font.BOLD, fonttitle));
		vrt.add(digititle);

		digilabel = new Custom_LabelField(Config_GlobalFunction.digi,
				LabelField.USE_ALL_WIDTH | DrawStyle.LEFT, Color.BLACK,
				Color.BLACK);
		digilabel.setFont(Font.getDefault().derive(Font.PLAIN, fontcontent));
		vrt.add(digilabel);

		vrt.add(new SeparatorField());

		maxistitle = new Custom_LabelField(Config_GlobalFunction.maxislabel,
				LabelField.USE_ALL_WIDTH | DrawStyle.LEFT | Field.FOCUSABLE,
				Color.BLACK, Color.BLACK);
		maxistitle.setFont(Font.getDefault().derive(Font.BOLD, fonttitle));
		vrt.add(maxistitle);

		maxislabel = new Custom_LabelField(Config_GlobalFunction.maxis,
				LabelField.USE_ALL_WIDTH | DrawStyle.LEFT, Color.BLACK,
				Color.BLACK);
		maxislabel.setFont(Font.getDefault().derive(Font.PLAIN, fontcontent));
		vrt.add(maxislabel);

		vrt.add(new SeparatorField());

		celcomtitle = new Custom_LabelField(Config_GlobalFunction.celcomlabel,
				LabelField.USE_ALL_WIDTH | DrawStyle.LEFT | Field.FOCUSABLE,
				Color.BLACK, Color.BLACK);
		celcomtitle.setFont(Font.getDefault().derive(Font.BOLD, fonttitle));
		vrt.add(celcomtitle);

		celcomlabel = new Custom_LabelField(Config_GlobalFunction.celcom,
				LabelField.USE_ALL_WIDTH | DrawStyle.LEFT, Color.BLACK,
				Color.BLACK);
		celcomlabel.setFont(Font.getDefault().derive(Font.PLAIN, fontcontent));
		vrt.add(celcomlabel);

		vrt.add(new SeparatorField());

		umobiletitle = new Custom_LabelField(
				Config_GlobalFunction.umobilelabel, LabelField.USE_ALL_WIDTH
						| DrawStyle.LEFT | Field.FOCUSABLE, Color.BLACK,
				Color.BLACK);
		umobiletitle.setFont(Font.getDefault().derive(Font.BOLD, fonttitle));
		vrt.add(umobiletitle);

		umobilelabel = new Custom_LabelField(Config_GlobalFunction.umobile,
				LabelField.USE_ALL_WIDTH | DrawStyle.LEFT, Color.BLACK,
				Color.BLACK);
		umobilelabel.setFont(Font.getDefault().derive(Font.PLAIN, fontcontent));
		vrt.add(umobilelabel);

		add(vrt);
	}

	protected void makeMenu(Menu menu, int instance) {

	}

	public class Custom_TopField extends Manager {
		private Bitmap download = Config_GlobalFunction
				.Bitmap("btn_download.png");
		private Bitmap downloadactive = Config_GlobalFunction
				.Bitmap("btn_download_active.png");
		private Bitmap refresh = Config_GlobalFunction
				.Bitmap("icon_refresh.png");
		private Bitmap refreshactive = Config_GlobalFunction
				.Bitmap("icon_refresh_active.png");
		private Bitmap back = Config_GlobalFunction.Bitmap("btn_back.png");
		private Bitmap backctive = Config_GlobalFunction
				.Bitmap("btn_back_active.png");
		private Bitmap news = Config_GlobalFunction.Bitmap("icon_news.png");
		private Bitmap newsactive = Config_GlobalFunction
				.Bitmap("icon_news_active.png");
		private Bitmap home = Config_GlobalFunction.Bitmap("icon_home.png");

		private BitmapField homeimage;
		private Custom_LabelField title;
		private int left, right, fontsize, position;
		private Database_Webservice webservice;

		Custom_TopField(final MainScreen mainscreen, final int position,
				final int catsid, final String header, int left, int right) {
			super(Manager.USE_ALL_WIDTH | Manager.NO_VERTICAL_SCROLL
					| Manager.NO_HORIZONTAL_SCROLL);
			this.left = left;
			this.right = right;
			this.position = position;

			if (Display.getWidth() > 480)
				fontsize = 43;
			else if (Display.getWidth() < 481 && Display.getWidth() > 320)
				fontsize = 33;
			else
				fontsize = 23;

			webservice = new Database_Webservice();
			setBackground(Config_GlobalFunction.loadbackground(Display
					.getWidth() + "_" + "header_bar.png"));

			if (left == 1) {
				newsbtn = new Custom_ButtonField(news, newsactive, newsactive);
				newsbtn.setChangeListener(new FieldChangeListener() {
					public void fieldChanged(Field field, int context) {
						Main.getUiApplication().pushScreen(
								new Menu_PopupMenu(position));
					}
				});
				add(newsbtn);
			} else if (left == 2) {
				backbtn = new Custom_ButtonField(back, backctive, backctive);
				backbtn.setChangeListener(new FieldChangeListener() {
					public void fieldChanged(Field field, int context) {
						Main.getUiApplication().popScreen(mainscreen);
					}
				});
				add(backbtn);
			}

			if (position != 0) {
				title = new Custom_LabelField(Config_GlobalFunction.maintitle,
						DrawStyle.ELLIPSIS | LabelField.USE_ALL_WIDTH
								| DrawStyle.VCENTER | Field.FOCUSABLE
								| DrawStyle.LEFT | ButtonField.CONSUME_CLICK,
						Color.WHITE) {
					protected boolean navigationClick(int status, int time) {
						Main.getUiApplication().pushScreen(
								new Custom_LoadingScreen(1));
						Main.getUiApplication().invokeLater(new Runnable() {
							public void run() {
								Main.getUiApplication().pushScreen(
										new Main_AllLatestNews());
							}
						}, 1 * 1000, false);
						return true;
					}
				};
			} else {
				title = new Custom_LabelField(Config_GlobalFunction.maintitle,
						DrawStyle.ELLIPSIS | LabelField.USE_ALL_WIDTH
								| DrawStyle.HCENTER | DrawStyle.VCENTER,
						Color.WHITE);
			}
			title.setFont(Font.getDefault().derive(Font.BOLD, fontsize));
			add(title);

			if (right == 1) {
				downloadbtn = new Custom_ButtonField(download, downloadactive,
						downloadactive);
				downloadbtn.setChangeListener(new FieldChangeListener() {
					public void fieldChanged(Field field, int context) {
						if (Config_GlobalFunction
								.Dialog(Config_GlobalFunction.alertdownload)) {
							if (Config_GlobalFunction.isConnected()) {
								Config_GlobalFunction.Message(
										Config_GlobalFunction.downloading, 10);
								Main.getUiApplication().invokeLater(
										new Runnable() {
											public void run() {
												webservice.UpdateAllCatNews(
														false, -1);
											}
										}, 1 * 1000, false);

							} else
								Config_GlobalFunction.Message(
										Config_GlobalFunction.nowifi, 1);
						} else
							Config_GlobalFunction.CloseDialog();
					}
				});
				add(downloadbtn);
			} else if (right == 2) {
				refreshbtn = new Custom_ButtonField(refresh, refreshactive,
						refreshactive);
				refreshbtn.setChangeListener(new FieldChangeListener() {
					public void fieldChanged(Field field, int context) {
						if (Config_GlobalFunction.isConnected()) {
							Config_GlobalFunction.Message(
									Config_GlobalFunction.refreshing, 10);
							Main.getUiApplication().invokeLater(new Runnable() {
								public void run() {
									webservice.refreshCatNewsindex(catsid,
											position, header);
								}
							});
						} else
							Config_GlobalFunction.Message(
									Config_GlobalFunction.nowifi, 1);
					}
				});
				add(refreshbtn);
			}

			if (position != 0) {
				homeimage = new BitmapField(home);
				add(homeimage);
			}
		}

		protected void sublayout(int width, int height) {
			int Height = (getPreferredHeight() - back.getHeight()) / 2;
			Field field = getField(0);

			if (left == 1) {
				layoutChild(field, back.getWidth(), back.getHeight());
				setPositionChild(field, 5, Height);
			} else if (left == 2) {
				layoutChild(field, news.getWidth(), news.getHeight());
				setPositionChild(field, 5, Height);
			}

			field = getField(1);
			if (position != 0) {
				layoutChild(field, getPreferredWidth() / 3,
						getPreferredHeight() - 10);
				setPositionChild(field, (getPreferredWidth()
						- getField(1).getWidth() + homeimage.getBitmapWidth())
						/ 2 + homeimage.getBitmapWidth(),
						(getPreferredHeight() - getField(1).getHeight()) / 2);

				field = getField(getFieldCount() - 1);
				layoutChild(field, homeimage.getBitmapWidth(),
						homeimage.getBitmapHeight());
				setPositionChild(
						field,
						(getPreferredWidth() - getField(1).getWidth() + homeimage
								.getBitmapWidth()) / 2,
						(getPreferredHeight() - homeimage.getBitmapHeight()) / 2);
			} else {
				layoutChild(field, getPreferredWidth() / 3,
						getPreferredHeight() - 10);
				setPositionChild(field, (getPreferredWidth() - getField(1)
						.getWidth()) / 2, (getPreferredHeight() - getField(1)
						.getHeight()) / 2);
			}

			field = getField(2);
			if (right == 1) {
				layoutChild(field, download.getWidth(), download.getHeight());
				setPositionChild(field,
						getPreferredWidth() - (download.getWidth() + 5), Height);
			} else if (right == 2) {
				layoutChild(field, refresh.getWidth(), refresh.getHeight());
				setPositionChild(field,
						getPreferredWidth() - (refresh.getWidth() + 5), Height);
			}

			width = Math.min(width, getPreferredWidth());
			height = Math.min(height, getPreferredHeight());
			setExtent(width, height);
		}

		public int getPreferredHeight() {
			return Config_GlobalFunction.Bitmap("header_bar.png").getHeight();
		}

		public int getPreferredWidth() {
			return Display.getWidth();
		}

		public void paint(Graphics graphics) {
			int rectHeight = getPreferredHeight();
			int rectWidth = getPreferredWidth();

			graphics.drawRect(0, 0, rectWidth, rectHeight);
			super.paint(graphics);
		}

		protected boolean navigationMovement(int dx, int dy, int status,
				int time) {
			int focusIndex = getFieldWithFocusIndex();

			while (dy > 0) {
				Field f = financebtn;
				if (f.isFocusable()) {
					f.setFocus();
					dy--;
				}
			}

			while (dy < 0) {
				return false;
			}

			while (dx > 0) {
				focusIndex++;

				if (focusIndex >= getFieldCount()) {
					return false;
				} else {
					Field f = getField(focusIndex);

					if (f.isFocusable()) {
						f.setFocus();
						dx--;
					}
				}
			}

			while (dx < 0) {
				focusIndex--;

				if (focusIndex < 0) {
					return false;
				} else {
					Field f = getField(focusIndex);

					if (f.isFocusable()) {
						f.setFocus();
						dx++;
					}
				}
			}

			return true;
		}
	}

	public class Custom_BottomField extends Manager {
		private Bitmap finance = Config_GlobalFunction
				.Bitmap("icon_economy.png");
		private Bitmap financeactive = Config_GlobalFunction
				.Bitmap("icon_economy_active.png");
		private Bitmap special = Config_GlobalFunction.Bitmap("icon_title.png");
		private Bitmap specialactive = Config_GlobalFunction
				.Bitmap("icon_title_active.png");
		private Bitmap forum = Config_GlobalFunction.Bitmap("icon_forum.png");
		private Bitmap forumactive = Config_GlobalFunction
				.Bitmap("icon_forum_active.png");
		private Bitmap discussion = Config_GlobalFunction
				.Bitmap("icon_discussion.png");
		private Bitmap discussionactive = Config_GlobalFunction
				.Bitmap("icon_discussion_active.png");
		private Bitmap other = Config_GlobalFunction.Bitmap("icon_other.png");
		private Bitmap otheractive = Config_GlobalFunction
				.Bitmap("icon_other_active.png");

		Custom_BottomField(final MainScreen mainscreen, final int thisid) {
			super(Manager.USE_ALL_WIDTH | Manager.NO_VERTICAL_SCROLL
					| Manager.NO_HORIZONTAL_SCROLL);
			setBackground(Config_GlobalFunction.loadbackground(Display
					.getWidth() + "_" + "footer_bar.png"));
			if (thisid != Config_GlobalFunction.menu_financecat) {
				financebtn = new Custom_ButtonField(finance, financeactive,
						financeactive);
				financebtn.setChangeListener(new FieldChangeListener() {
					public void fieldChanged(Field field, int context) {
						Main.getUiApplication().pushScreen(
								new Custom_LoadingScreen(1));
						Main.getUiApplication().invokeLater(new Runnable() {
							public void run() {
								Main.getUiApplication()
										.pushScreen(
												new Main_ParticularCategoryAllNews(
														Config_GlobalFunction.menu_financecat,
														2,
														Config_GlobalFunction.menu_finance));
							}
						}, 1 * 1000, false);
					}
				});
			} else
				financebtn = new Custom_ButtonField(financeactive,
						financeactive, financeactive);
			add(financebtn);

			if (thisid != Config_GlobalFunction.menu_specialcat) {
				specialbtn = new Custom_ButtonField(special, specialactive,
						specialactive);
				specialbtn.setChangeListener(new FieldChangeListener() {
					public void fieldChanged(Field field, int context) {
						Main.getUiApplication().pushScreen(
								new Custom_LoadingScreen(1));
						Main.getUiApplication().invokeLater(new Runnable() {
							public void run() {
								Main.getUiApplication()
										.pushScreen(
												new Main_ParticularCategoryAllNews(
														Config_GlobalFunction.menu_specialcat,
														9,
														Config_GlobalFunction.menu_special));
							}
						}, 1 * 1000, false);
					}
				});
			} else
				specialbtn = new Custom_ButtonField(specialactive,
						specialactive, specialactive);
			add(specialbtn);

			if (thisid != Config_GlobalFunction.menu_forumcat) {
				forumbtn = new Custom_ButtonField(forum, forumactive,
						forumactive);
				forumbtn.setChangeListener(new FieldChangeListener() {
					public void fieldChanged(Field field, int context) {
						Main.getUiApplication().pushScreen(
								new Custom_LoadingScreen(1));
						Main.getUiApplication().invokeLater(new Runnable() {
							public void run() {
								Main.getUiApplication()
										.pushScreen(
												new Main_ParticularCategoryAllNews(
														Config_GlobalFunction.menu_forumcat,
														-1,
														Config_GlobalFunction.menu_forum));
							}
						}, 1 * 1000, false);
					}
				});
			} else
				forumbtn = new Custom_ButtonField(forumactive, forumactive,
						forumactive);
			add(forumbtn);

			if (thisid != Config_GlobalFunction.menu_discussioncat) {
				discussionbtn = new Custom_ButtonField(discussion,
						discussionactive, discussionactive);
				discussionbtn.setChangeListener(new FieldChangeListener() {
					public void fieldChanged(Field field, int context) {
						Main.getUiApplication().pushScreen(
								new Custom_LoadingScreen(1));
						Main.getUiApplication().invokeLater(new Runnable() {
							public void run() {
								Main.getUiApplication()
										.pushScreen(
												new Main_ParticularCategoryAllNews(
														Config_GlobalFunction.menu_discussioncat,
														5,
														Config_GlobalFunction.menu_discussion));
							}
						}, 1 * 1000, false);
					}
				});
			} else
				discussionbtn = new Custom_ButtonField(discussionactive,
						discussionactive, discussionactive);
			add(discussionbtn);

			if (thisid != 20) {
				otherbtn = new Custom_ButtonField(other, otheractive,
						otheractive);
				otherbtn.setChangeListener(new FieldChangeListener() {
					public void fieldChanged(Field field, int context) {
						Main.getUiApplication().pushScreen(
								new Custom_LoadingScreen(1));
						Main.getUiApplication().invokeLater(new Runnable() {
							public void run() {
								Main.getUiApplication().pushScreen(
										new Menu_Others());
							}
						}, 1 * 1000, false);
					}
				});
			} else
				otherbtn = new Custom_ButtonField(otheractive, otheractive,
						otheractive);
			add(otherbtn);
		}

		protected void sublayout(int width, int height) {
			int Height = (getPreferredHeight() - finance.getHeight()) / 2;

			Field field = getField(0);
			layoutChild(field, finance.getWidth(), finance.getHeight());
			setPositionChild(field, getGap(), Height);
			field = getField(1);
			layoutChild(field, special.getWidth(), special.getHeight());
			setPositionChild(field, getPreferredWidth() / getFieldCount()
					+ getGap(), Height);

			field = getField(2);
			layoutChild(field, forum.getWidth(), forum.getHeight());
			setPositionChild(field, getPreferredWidth() * 2 / getFieldCount()
					+ getGap(), Height);

			field = getField(3);
			layoutChild(field, discussion.getWidth(), discussion.getHeight());
			setPositionChild(field, getPreferredWidth() * 3 / getFieldCount()
					+ getGap(), Height);

			field = getField(4);
			layoutChild(field, other.getWidth(), other.getHeight());
			setPositionChild(field, getPreferredWidth() * 4 / getFieldCount()
					+ getGap(), Height);

			width = Math.min(width, getPreferredWidth());
			height = Math.min(height, getPreferredHeight());
			setExtent(width, height);
		}

		public int getPreferredHeight() {
			return Config_GlobalFunction.Bitmap("footer_bar.png").getHeight();
		}

		public int getPreferredWidth() {
			return Display.getWidth();
		}

		protected void paint(Graphics graphics) {
			int rectHeight = getPreferredHeight();
			int rectWidth = getPreferredWidth();
			graphics.drawRect(0, 0, rectWidth, rectHeight);
			super.paint(graphics);
		}

		private int getGap() {
			return ((getPreferredWidth() / getFieldCount()) - finance
					.getWidth()) / 2;
		}

		protected boolean navigationMovement(int dx, int dy, int status,
				int time) {
			int focusIndex = getFieldWithFocusIndex();

			while (dy > 0) {
				Field f = digititle;
				if (f.isFocusable()) {
					f.setFocus();
					dy--;
				}
			}

			while (dy < 0) {
				Field f = newsbtn;
				if (f.isFocusable()) {
					f.setFocus();
					dy++;
				}
			}

			while (dx > 0) {
				focusIndex++;

				if (focusIndex >= getFieldCount()) {
					return false;
				} else {
					Field f = getField(focusIndex);

					if (f.isFocusable()) {
						f.setFocus();
						dx--;
					}
				}
			}

			while (dx < 0) {
				focusIndex--;

				if (focusIndex < 0) {
					return false;
				} else {
					Field f = getField(focusIndex);

					if (f.isFocusable()) {
						f.setFocus();
						dx++;
					}
				}
			}

			return true;
		}
	}
}
