package mypackage;

import java.util.Calendar;
import java.util.Vector;

import net.rim.device.api.i18n.DateFormat;
import net.rim.device.api.i18n.SimpleDateFormat;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.CoverageInfo;
import net.rim.device.api.system.Display;
import net.rim.device.api.system.WLANInfo;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.VirtualKeyboard;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.Status;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

public final class Config_GlobalFunction {
	// Link
	public static final String officiallink = "http://www.orientaldaily.com.my/";
	//public static final String sharelink = "http://www.orientaldaily.com.my/index.php?option=com_k2&view=item&id=";

	// Dialog Title
	public static final String alertdownload = "需時下載\n確定下載？";
	public static final String alertsuggestion = "確定退出意見箱？";
	public static final String alertnewspaper = "確定取消訂閱報章？";
	public static final String alertadvertisement = "確定取消刊登廣告？";
	public static final String alertmsgleave = "確定離開？";
	public static final String alertmsgpushleave = "確定離開推送新聞？";

	// Top Title
	public static final String maintitle = "東方日報";
	public static final String formtitle = "首次登錄";
	public static final String pushtitle = "東方推送新聞";

	// Header Title
	public static final String latest = "即時新聞";
	public static final String comments = "留言";
	public static final String others = "其它";
	public static final String sms = "訂購快訊";
	public static final String newspaper = "訂購報紙";
	public static final String advertisement = "訂購廣告";
	public static final String aboutus = "東方新聞網";
	public static final String suggestion = "意見回饋";

	// CategoryNews Header Title & ID
	public static final String menu_home = "主頁";
	public static final String menu_finance = "財經";
	public static final String menu_special = "專題";
	public static final String menu_forum = "評論";
	public static final String menu_discussion = "言論";

	public static final int menu_financecat = 2;
	public static final int menu_specialcat = 7;
	public static final int menu_forumcat = 10;
	public static final int menu_discussioncat = 11;

	// Comment
	public static final String commentgmcontent = "暫無留言";
	public static final String commentgmname = "管理員";
	public static final String commentposter = "匿名者";
	public static final String nocomment = "留言欄不能空白！";
	public static final String commentapprove = "留言待批";

	// Date
	private static DateFormat dateFormat = new SimpleDateFormat(
			"yyyy-MM-dd hh:mm:ss");
	private static Calendar cal = Calendar.getInstance();
	public static final String date = dateFormat.format(cal.getTime())
			.toString();

	// Warning Message For Empty Field
	public static final String noname = "請填寫姓名！";
	public static final String noemail = "請填寫電郵！";
	public static final String noaddress = "請填寫地址！";
	public static final String nohphone = "請填寫電話號碼！";
	public static final String noadsdetail = "請填寫廣告詳情！";
	public static final String nosuggestion = "請填寫意見";
	public static final String nointerested = "請選擇興趣！";

	// Button Submit
	public static final String suggestionsubmit = "謝謝意見！";
	public static final String advertisementsubmit = "謝謝刊登廣告！";
	public static final String newspapersubmit = "謝謝訂紙！";
	public static final String surveysubmit = "谢谢填写！";
	public static final String more = "更多";
	public static final String back = "返回";

	// No Wifi
	public static final String nowifi = "手機還未連接網絡\n請檢查手機網絡！";
	public static final String nosharenews = "手機還未連接網絡，無法分享新聞！";
	public static final String noaccesscomment = "手機還未連接網絡，無法進入留言區！";
	public static final String nopostcomment = "手機還未連接網絡，無法留言！";
	public static final String nosubmit = "手機還未連接網絡，無法提交！";

	// Download and Update Message
	public static final String downloading = "正在下載最新資料。。。";
	public static final String retrieving = "正在下載更多最新資料。。。";
	public static final String refreshing = "正在下載最新資料。。。";
	public static final String getting = "正在下載最新資料。。。";
	public static final String nodownloading = "無須下載最新資料。。。";
	public static final String refreshfailed = "下載失敗。。。";

	// Button Field
	public static final String submit = "提交";
	public static final String cancel = "取消";

	// Form Label Field
	public static final String cnamelabel = "中文姓名：";
	public static final String enamelabel = "英文姓名：";
	public static final String pemaillabel = "個人電郵：";
	public static final String addresslabel = "通訊地址：";
	public static final String suggestionlabel = "個人意見：";
	public static final String hplabel = "手機號碼：";
	public static final String remarklabel = "特別注明：";
	public static final String adsdetaillabel = "廣告詳情：";

	// NewsDetail Label Field
	public static final String commentlabel = "留言：";
	public static final String authorlabel = "作者：";
	public static final String reporterlabel = "報導：";
	public static final String imagelabel = "點擊照片放大";

	// NewUser Form Label Field
	public static final String titlelabel = "歡迎您首次登入《東方日報》手機版應用程序。這是免費註冊和使用，讓您體驗全新的閱報樂趣。希望您將這資訊推薦給朋友，大家告訴大家，一起選擇此程序。歡迎您把意見看法電郵至enews@orientaldaily.com.my 祝您閱讀愉快。";
	public static final String namelabel = "姓名：";
	public static final String emaillabel = "電郵：";
	public static final String agelabel = "年齡：";
	public static final String genderlabel = "性別：";
	public static final String joblabel = "職業：";
	public static final String salarylabel = "收入：";
	public static final String interestedlabel = "興趣：";
	public static final String interestedclicklabel = "可選多項";
	public static final String interestedmenulabel = "興趣（點擊返回鍵完成選項）";

	// SMS Label Field
	public static final String digi = "欲订阅服務，輸入BUY《空格》ODGEN，傳送到2000\n欲終止服務，輸入STOP《空格》ODGEN，傳送到2000";
	public static final String maxis = "欲订阅服務，輸入BUY《空格》ODNEWS，傳送到26000\n欲終止服務，輸入STOP《空格》ODNEWS，傳送到26000";
	public static final String celcom = "欲订阅服務，輸入BUY《空格》ODNEWS，傳送到28800\n欲終止服務，輸入STOP《空格》ODNEWS，傳送到28800";
	public static final String umobile = "欲订阅服務，輸入BUY《空格》NW《空格》ORIENTAL，傳送到28118\n欲終止服務，輸入STOP《空格》NW《空格》ORIENTAL，傳送到28118";
	public static final String digilabel = "Digi用户\n\n";
	public static final String maxislabel = "Maxis用户\n\n";
	public static final String umobilelabel = "U-Mobile用户\n\n";
	public static final String celcomlabel = "Celcom用户\n\n";

	// Dropdown Content Array;
	public static final String[] agetext = { "15 - 19", "20 - 29", "30 - 39",
			"40 - 49", "50 以上" };
	public static final String[] gendertext = { "男", "女" };
	public static final String[] jobtext = { "專業領域", "商務企業", "財經金融", "市場行銷",
			"媒體廣告", "執行人員", "公共服務", "自由職業", "在籍學生", "家庭主婦", "自僱人士", "退休人士",
			"其他" };
	public static final String[] salarytext = { "1500 - 3000", "3001 - 4000",
			"4001 - 5000", "超過5000", "超過8000", "超過10000" };
	public static final String[] interestedtext = { "保健/醫療", "體育/戶外", "投資/財經",
			"旅遊/休閒", "服裝/時尚", "電腦/科技", "汽車/產業", "購物/音樂" };

	// ==========================Internal Use==============================//

	// ==========================Dialog, Message==============================//
	private static Dialog d;
	private static Bitmap logo;

	private static String yes = "是";
	private static String no = "否";

	public static void Message(final String string, final int time) {
		Main.getUiApplication().invokeLater(new Runnable() {
			public void run() {
				Status.show(string, Bitmap("logo_icon.png"), time * 1000);
			}
		});
	}

	public static boolean Dialog(String string) {
		logo = Bitmap.getBitmapResource("icon.png");
		d = new Dialog(string, new String[] { yes, no }, new int[] { Dialog.OK,
				Dialog.CANCEL }, Dialog.OK, logo);
		return (d.doModal() == Dialog.OK);
	}

	public static void CloseDialog() {
		d.close();
	}

	// =====================================================================//

	// ==========================Auto New Line =============================//
	public static Vector wrap(String text, int width) {
		Vector result = new Vector();

		String remaining = text;

		while (remaining.length() >= 0) {
			int index = getSplitIndex(remaining, width);
			if (index == -1)
				break;

			result.addElement(remaining.substring(0, index));
			remaining = remaining.substring(index);

			if (index == 0)
				break;
		}
		return result;
	}

	private static int getSplitIndex(String bigString, int width) {
		int index = -1;
		int lastSpace = -1;
		String smallString = "";
		boolean spaceEncountered = false;
		boolean maxWidthFound = false;

		for (int i = 0; i < bigString.length(); i++) {
			char current = bigString.charAt(i);
			smallString += current;
			if (current == ' ') {
				lastSpace = i;
				spaceEncountered = true;
			}

			int linewidth = Font.getDefault().getAdvance(smallString, 0,
					smallString.length());

			if (linewidth > width) {
				if (spaceEncountered)
					index = lastSpace + 1;
				else
					index = i;
				maxWidthFound = true;
				break;
			}
		}

		if (!maxWidthFound)
			index = bigString.length();

		return index;
	}

	// =====================================================================//

	// ==============================Bitmap=================================//
	public static Background loadbackground(String path) {
		Background background = BackgroundFactory.createBitmapBackground(Bitmap
				.getBitmapResource(path));
		return background;
	}

	public static Bitmap Bitmap(String path) {
		Bitmap bitmap = Bitmap.getBitmapResource(Display.getWidth() + "_"
				+ path);
		return bitmap;
	}

	// =====================================================================//

	// =========================Virtual Keyboard============================//
	public static void hideKeyboard() {
	      VirtualKeyboard kb = Main.getUiApplication().getActiveScreen().getVirtualKeyboard();
	      if (kb != null) {
	         kb.setVisibility(VirtualKeyboard.HIDE);
	      }
	   }
	// =====================================================================//
	
	// ========================Checking Internet============================//
	public static boolean isConnected() {
		if (WLANInfo.getWLANState() == WLANInfo.WLAN_STATE_CONNECTED)
			return true;
		else if (CoverageInfo.getCoverageStatus() == CoverageInfo.COVERAGE_NONE)
			return false;
		else
			return true;

	}
}
