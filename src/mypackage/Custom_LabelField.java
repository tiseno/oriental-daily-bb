package mypackage;

import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.LabelField;

public class Custom_LabelField extends LabelField {
	private int _fontColor = -1, focuscolor = 0x540604;
	private boolean isfocus;

	public Custom_LabelField(String text, long style) {
		super(text, style);
	}

	public Custom_LabelField(String text, long style, int color) {
		super(text, style);
		_fontColor = color;
	}

	public Custom_LabelField(String text, long style, int color, int focuscolor) {
		super(text, style);
		_fontColor = color;
		this.focuscolor = focuscolor;
	}

	public void setFontColor(int fontColor) {
		_fontColor = fontColor;
	}

	protected void onFocus(int direction) {
		super.onFocus(direction);
		isfocus = true;
		this.invalidate();
	}

	public void onUnfocus() {
		super.onUnfocus();
		isfocus = false;
		this.invalidate();
	}

	protected void paint(Graphics graphics) {
		if (-1 != _fontColor)
			graphics.setColor(_fontColor);
		if (isfocus)
			graphics.setColor(focuscolor);
		else
			graphics.setColor(_fontColor);
		super.paint(graphics);
	}

	protected void drawFocus(Graphics graphics, boolean on) {

	}
}
