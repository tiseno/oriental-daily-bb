package mypackage;

import net.rim.device.api.system.GIFEncodedImage;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;

public class Util_AnimateGifField extends BitmapField {
	private GIFEncodedImage _image;
	private int _currentFrame;
	private AnimatorThread _animatorThread;

	public Util_AnimateGifField(GIFEncodedImage image) {
		this(image, 0);
	}

	public Util_AnimateGifField(GIFEncodedImage image, long style) {
		super(image.getBitmap(), style);

		_image = image;

		_animatorThread = new AnimatorThread(this);
		_animatorThread.start();
	}

	protected void paint(Graphics graphics) {
		super.paint(graphics);
		if (_currentFrame != 0) {
			graphics.drawImage(_image.getFrameLeft(_currentFrame),
					_image.getFrameTop(_currentFrame),
					_image.getFrameWidth(_currentFrame),
					_image.getFrameHeight(_currentFrame), _image,
					_currentFrame, 0, 0);
		}
	}

	protected void onUndisplay() {
		_animatorThread.stop();
		super.onUndisplay();
	}

	private class AnimatorThread extends Thread {
		private Util_AnimateGifField _theField;
		private boolean _keepGoing = true;
		private int _totalFrames;
		private int _loopCount;
		private int _totalLoops;

		public AnimatorThread(Util_AnimateGifField theField) {
			_theField = theField;
			_totalFrames = _image.getFrameCount();
			_totalLoops = _image.getIterations();
		}

		public synchronized void stop() {
			_keepGoing = false;
		}

		public void run() {
			while (_keepGoing) {
				UiApplication.getUiApplication().invokeAndWait(new Runnable() {
					public void run() {
						_theField.invalidate();
					}
				});

				try {
					sleep(_image.getFrameDelay(_currentFrame) * 10);
				} catch (InterruptedException iex) {
				}

				++_currentFrame;

				if (_currentFrame == _totalFrames) {
					_currentFrame = 0;
					++_loopCount;
					if (_loopCount == _totalLoops) {
						_keepGoing = false;
					}
				}
			}
		}
	}
}
