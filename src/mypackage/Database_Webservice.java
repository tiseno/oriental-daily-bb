package mypackage;

import java.util.Vector;

import net.rim.device.api.ui.component.Dialog;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransport;

import java.io.InputStream;
import java.io.OutputStream;

import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;

import org.kxml2.io.KXmlParser;
import org.xmlpull.v1.XmlPullParser;

public final class Database_Webservice {
	public final static String NAMESPACE = "urn:OrientalServices1";
	public final static String URL = "http://www.orientaldaily.com.my/ftp/tiseno/ws/OrientalServices1.php?wsdl";
	public final static String ht_params = ";ConnectionTimeout=60000;deviceside=true;apn=orientaldaily.com.my";
	//public final static String ht_params = ";ConnectionTimeout=60000;deviceside=true";
	
	private final static String Cat_SOAP_ACTION = "urn:OrientalServices1#Get_Category";
	private final static String Cat_METHOD_NAME = "Get_Category";

	private final static String get_moreNews_SOAP_ACTION = "urn:OrientalServices1#GetTopNewsByCategory";
	private final static String get_moreNews_METHOD_NAME = "GetTopNewsByCategory";

	private final static String ALLTopNews_SOAP_ACTION = "urn:OrientalServices1#GetTopNewsByCategoryOverall";
	private final static String ALLTopNews_METHOD_NAME = "GetTopNewsByCategoryOverall";

	private final static String Survey_SOAP_ACTION = "urn:OrientalServices1#Insert_survey";
	private final static String Survey_METHOD_NAME = "Insert_survey";

	private final static String Order_SOAP_ACTION = "urn:OrientalServices1#Insert_order";
	private final static String Order_METHOD_NAME = "Insert_order";

	private final static String Enquiry_SOAP_ACTION = "urn:OrientalServices1#Insert_enquiry";
	private final static String Enquiry_METHOD_NAME = "Insert_enquiry";

	private final static String Comment_SOAP_ACTION = "urn:OrientalServices1#Insert_Comment";
	private final static String Comment_METHOD_NAME = "Insert_Comment";

	private String get_Comment_SOAP_ACTION = "urn:OrientalServices1#Get_CommentByNewsID";
	private String get_Comment_METHOD_NAME = "Get_CommentByNewsID";

	private String strXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
			+ "<soap:Envelope xmlns:soap=\"h t t p : / / s c h e m a s . x m l s o a p . o r g / s o a p / envelope/\" xmlns:xsi=\"h t t p : / / w w w . w 3 . o r g / 2 0 0 1 /XMLSchema-instance\" xmlns:xsd=\"h t t p : / / w w w . w 3 . o r g / 2 0 0 1/XMLSchema\">"
			+ "<soap:Body>";

	public Vector newsList, sallCommentList, AbtImg, news, newsCat, form;

	private Database_DataBaseHelper dbhelper;

	public Database_Webservice() {
		dbhelper = new Database_DataBaseHelper();
	}

	/* Online Web Services */
	public void GetNewsCategory(boolean ispush, int newsid) {
	
		SoapObject rpc = new SoapObject(NAMESPACE, Cat_METHOD_NAME);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.bodyOut = rpc;
		envelope.dotNet = false;

		HttpTransport ht = new HttpTransport(URL + ht_params);
		ht.debug = true;

		try {
			ht.call(Cat_SOAP_ACTION, envelope);

			SoapObject result = (SoapObject) envelope.bodyIn;

			Vector vector = (Vector) result.getProperty(0);

			for (int i = 0; i < vector.size(); i++) 
			{
				SoapObject getPropertyD = (SoapObject) vector.elementAt(i);

				String getPropertyID = getPropertyD.getProperty("id")
						.toString();
				String getPropertyName = getPropertyD.getProperty("name")
						.toString();

				List_CategoryNews addNewsCat = new List_CategoryNews(
						Integer.parseInt(getPropertyID), getPropertyName);

				dbhelper.AddCatNews(addNewsCat);
			}

			refreshTopNews(ispush, newsid);
		} catch (Exception ex) {
		}
	}

	public void GetMoreCatNews(final int CatID, final int IndexID,
			int position, String header) {
		SoapObject request = new SoapObject(NAMESPACE, get_moreNews_METHOD_NAME);
		request.addProperty("id", new Integer(CatID));
		request.addProperty("fromIndex", new Integer(IndexID));

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.bodyOut = request;
		envelope.dotNet = false;

		HttpTransport ht = new HttpTransport(URL + ht_params);
		ht.debug = true;

		try {
			ht.call(get_moreNews_SOAP_ACTION, envelope);

			SoapObject result = (SoapObject) envelope.bodyIn;

			RetrieveMoreCatNewsFromSoap(result, CatID, position, header);
		} catch (Exception ex) {
			Main.getUiApplication()
					.pushScreen(
							new Main_ParticularCategoryAllNews(CatID, position,
									header));
			Config_GlobalFunction.Message(Config_GlobalFunction.refreshfailed,
					1);
		}
	}

	public void GetAllTopNews(boolean ispush, int newsid) {
		SoapObject request = new SoapObject(NAMESPACE, ALLTopNews_METHOD_NAME);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.bodyOut = request;
		envelope.dotNet = false;

		HttpTransport ht = new HttpTransport(URL + ht_params);
		ht.debug = true;

		try {
			ht.call(ALLTopNews_SOAP_ACTION, envelope);
			SoapObject result = (SoapObject) envelope.bodyIn;

			RetrieveAllTopNewsFromSoap(result, ispush, newsid);
		} catch (Exception ex) {
			Main.getUiApplication().pushScreen(new Main_AllLatestNews());
			Config_GlobalFunction.Message(Config_GlobalFunction.refreshfailed,
					1);
		}
	}

	public void UpdateAllCatNews(boolean ispush, int newsid) {
		try {
			dbhelper.DeleteNewsCatFT();
			Vector newsCat = dbhelper.getAllNewsCategories();

			if (newsCat.size() == 0) {
				
				GetNewsCategory(ispush, newsid);
			}
		} catch (Exception ex) {
		}
	}

	public void UpdateMoreCatNews(int NewsCatID, int position, String header) {
		try {
			news = dbhelper.getSingleCatNews(NewsCatID);
		} catch (Exception e) {

		}
		GetMoreCatNews(NewsCatID, news.size(), position, header);
	}

	// ==========Refresh Webservice=============//
	public void refreshCatNewsindex(int NewsCatID, int position, String header) {
		try {
			dbhelper.DeleteNewsByCatID(NewsCatID);
			news = dbhelper.getSingleCatNews(NewsCatID);
		} catch (Exception e) {
			Main.getUiApplication().pushScreen(
					new Main_ParticularCategoryAllNews(NewsCatID, position,
							header));
			Config_GlobalFunction.Message(Config_GlobalFunction.refreshfailed,
					1);
		}
		GetMoreCatNews(NewsCatID, news.size(), position, header);
	}

	public void refreshTopNews(boolean ispush, int newsid) {
		try {
			dbhelper.DeleteNewsFT();
			news = dbhelper.getAllTodaysNews();

			if (news.size() == 0) {
				GetAllTopNews(ispush, newsid);
			}
		} catch (Exception e) {
		}
	}

	// ==========Offline Webservice=============//
	public void LoadCategoryTitle() {
		try {
			newsCat = dbhelper.getAllNewsCategories();
		} catch (Exception ex) {
		}
	}

	public void DeleteDB() 
	{
		try 
		{
			dbhelper.deleteDatabase();
		} 
		catch (Exception ex) 
		{
		}
	}

	public void LoadCatNews(int NewsCatID) {
		try {
			news = dbhelper.getSingleCatNews(NewsCatID);
		} catch (Exception e) {
		}
	}

	public void LoadtodayNews() 
	{
		try 
		{
			news = dbhelper.getAllTodaysNews();
		} 
		catch (Exception e) 
		{
			
		}
	}

	public void LoadSingleNews(int NewsID) {
		try 
		{
			news = dbhelper.getSingleNews(NewsID);
		} 
		catch (Exception e) 
		{
		}
	}

	public void LoadAllNews() {
		try {
			news = dbhelper.getAllNews();
		} catch (Exception e) {
		}
	}

	// ==========Insert Webservice=============//
	public void InsertSurvey(final String Name, final String Email,
			final String Age, final String Gender, final String Job,
			final String income, final String Interest) {
		SoapObject request = new SoapObject(NAMESPACE, Survey_METHOD_NAME);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.setOutputSoapObject(request);
		envelope.bodyOut = request;
		envelope.dotNet = false;
		envelope.encodingStyle = SoapSerializationEnvelope.ENC;

		String testBody = strXml; // Initialization
		testBody += "<methodName xmlns=\"" + NAMESPACE + "\">";
		testBody += "<name>" + Name + "</name >";
		testBody += "<email>" + Email + "</email >";
		testBody += "<age>" + Age + "</age >";
		testBody += "<gender>" + Gender + "</gender>";
		testBody += "<job>" + Job + "</job >";
		testBody += "<income>" + income + "</income >";
		testBody += "<interest>" + Interest + "</interest>";
		testBody += "</methodName>";
		testBody += "</soap:Body>";
		testBody += "</soap:Envelope>";

		try {
			// Open Connection
			HttpConnection connection = (HttpConnection) Connector.open(URL
					+ ht_params, Connector.READ_WRITE, true);
			connection.setRequestProperty("SOAPAction", Survey_SOAP_ACTION);
			connection.setRequestProperty("Content-Type",
					"text/xml; charset=utf-8");
			connection.setRequestProperty("Content-Length",
					"" + testBody.getBytes().length);
			connection.setRequestProperty("User-Agent", "kSOAP/2.0");
			connection.setRequestMethod(HttpConnection.POST);

			OutputStream os = connection.openOutputStream();
			os.write(testBody.getBytes("UTF-8"), 0,
					testBody.getBytes("UTF-8").length);
			os.flush();
			os.close();

			InputStream is = connection.openInputStream();

			XmlPullParser xp = new KXmlParser();
			xp.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, true);
			xp.setInput(is, null);

			envelope.parse(xp);

			SoapObject result = (SoapObject) envelope.bodyIn;

			Vector vector = (Vector) result.getProperty(0);

			String[] results2 = null;

			for (int i = 0; i < vector.size(); i++) {
				results2 = new String[vector.size()];
				results2[i] = vector.elementAt(i).toString();
			}
		} catch (Exception ex) {
		}
	}

	public void InsertOrder(final String Type, final String CName,
			final String Name, final String Address, final String Email,
			final String ContactNo, final String Remark) {
		try {
			SoapObject request = new SoapObject(NAMESPACE, Order_METHOD_NAME);

			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.bodyOut = request;
			envelope.dotNet = false;
			envelope.encodingStyle = SoapSerializationEnvelope.ENC;

			String testBody = strXml;
			testBody += "<methodName xmlns=\"" + NAMESPACE + "\">";
			testBody += "<type>" + Type + "</type >";
			testBody += "<chinese_name>" + CName + "</chinese_name >";
			testBody += "<name>" + Name + "</name >";
			testBody += "<address>" + Address + "</address>";
			testBody += "<email>" + Email + "</email >";
			testBody += "<contact_no>" + ContactNo + "</contact_no >";
			testBody += "<remark>" + Remark + "</remark>";
			testBody += "</methodName>";
			testBody += "</soap:Body>";
			testBody += "</soap:Envelope>";

			HttpConnection connection = (HttpConnection) Connector.open(URL
					+ ht_params, Connector.READ_WRITE, true);
			connection.setRequestProperty("SOAPAction", Order_SOAP_ACTION);
			connection.setRequestProperty("Content-Type",
					"text/xml; charset=utf-8");
			connection.setRequestProperty("Content-Length",
					"" + testBody.getBytes().length);
			connection.setRequestProperty("User-Agent", "kSOAP/2.0");
			connection.setRequestMethod(HttpConnection.POST);

			OutputStream os = connection.openOutputStream();
			os.write(testBody.getBytes("UTF-8"), 0,
					testBody.getBytes("UTF-8").length);
			os.flush();
			os.close();

			InputStream is = connection.openInputStream();

			XmlPullParser xp = new KXmlParser();
			xp.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, true);
			xp.setInput(is, null);

			envelope.parse(xp);

			SoapObject result = (SoapObject) envelope.bodyIn;

			Vector vector = (Vector) result.getProperty(0);

			String[] results2 = null;

			for (int i = 0; i < vector.size(); i++) {
				results2 = new String[vector.size()];
				results2[i] = vector.elementAt(i).toString();
			}
		} catch (Exception ex) {
		}
	}

	public void InsertEnquiry(final String EnquiryName,
			final String Enquiryemail, final String EnquirySuggestion,
			final String EnquiryRemark) {
		try {
			SoapObject request = new SoapObject(NAMESPACE, Enquiry_METHOD_NAME);

			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.bodyOut = request;
			envelope.dotNet = false;
			envelope.encodingStyle = SoapSerializationEnvelope.ENC;

			String testBody = strXml;
			testBody += "<methodName xmlns=\"" + NAMESPACE + "\">";
			testBody += "<name>" + EnquiryName + "</name >";
			testBody += "<email>" + Enquiryemail + "</email >";
			testBody += "<suggestion>" + EnquirySuggestion + "</suggestion >";
			testBody += "<remark>" + EnquiryRemark + "</remark>";
			testBody += "</methodName>";
			testBody += "</soap:Body>";
			testBody += "</soap:Envelope>";

			HttpConnection connection = (HttpConnection) Connector.open(URL
					+ ht_params, Connector.READ_WRITE, true);
			connection.setRequestProperty("SOAPAction", Enquiry_SOAP_ACTION);
			connection.setRequestProperty("Content-Type",
					"text/xml; charset=utf-8");
			connection.setRequestProperty("Content-Length",
					"" + testBody.getBytes().length);
			connection.setRequestProperty("User-Agent", "kSOAP/2.0");
			connection.setRequestMethod(HttpConnection.POST);

			OutputStream os = connection.openOutputStream();
			os.write(testBody.getBytes("UTF-8"), 0,
					testBody.getBytes("UTF-8").length);
			os.flush();
			os.close();

			InputStream is = connection.openInputStream();

			XmlPullParser xp = new KXmlParser();
			xp.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, true);
			xp.setInput(is, null);

			envelope.parse(xp);

			SoapObject result = (SoapObject) envelope.bodyIn;

			Vector vector = (Vector) result.getProperty(0);

			String[] results2 = null;

			for (int i = 0; i < vector.size(); i++) {
				results2 = new String[vector.size()];
				results2[i] = vector.elementAt(i).toString();
			}
		} catch (Exception ex) {
		}
	}

	public void InsertComment(final int CommentNewsID,
			final String CommentUserName, final String Comment,
			final int CommentApprovalStatus) {
		SoapObject request = new SoapObject(NAMESPACE, Comment_METHOD_NAME);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.setOutputSoapObject(request);
		envelope.bodyOut = request;
		envelope.dotNet = false;
		envelope.encodingStyle = SoapSerializationEnvelope.ENC;

		String testBody = strXml;
		testBody += "<methodName xmlns=\"" + NAMESPACE + "\">";
		testBody += "<itemid>" + CommentNewsID + "</itemid >";
		testBody += "<name>" + CommentUserName + "</name >";
		testBody += "<comment>" + Comment + "</comment >";
		testBody += "<approvalStatus>" + CommentApprovalStatus
				+ "</approvalStatus>";
		testBody += "</methodName>";
		testBody += "</soap:Body>";
		testBody += "</soap:Envelope>";

		try {
			HttpConnection connection = (HttpConnection) Connector.open(URL
					+ ht_params, Connector.READ_WRITE, true);
			connection.setRequestProperty("SOAPAction", Comment_SOAP_ACTION);
			connection.setRequestProperty("Content-Type",
					"text/xml; charset=utf-8");
			connection.setRequestProperty("Content-Length",
					"" + testBody.getBytes().length);
			connection.setRequestProperty("User-Agent", "kSOAP/2.0");
			connection.setRequestMethod(HttpConnection.POST);

			OutputStream os = connection.openOutputStream();
			os.write(testBody.getBytes("UTF-8"), 0,
					testBody.getBytes("UTF-8").length);
			os.flush();
			os.close();

			InputStream is = connection.openInputStream();

			XmlPullParser xp = new KXmlParser();
			xp.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, true);
			xp.setInput(is, null);

			envelope.parse(xp);

			SoapObject result = (SoapObject) envelope.bodyIn;

			Vector vector = (Vector) result.getProperty(0);

			String[] results2 = null;

			for (int i = 0; i < vector.size(); i++) {
				results2 = new String[vector.size()];
				results2[i] = vector.elementAt(i).toString();
			}
		} catch (Exception ex) {
		}
	}

	public void InsertFormSubmit(final int NewsSubmit, final int notification,
			final int sound, final int vibrate, final int popup) 
	{
		try 
		{
			List_UserDetails SubmitForm = new List_UserDetails(NewsSubmit,
					notification, sound, vibrate, popup);

			dbhelper.AddSurveyForm(SubmitForm);
		} 
		catch (Exception ex) 
		{
		}
	}

	// ==========Check First Time User=============//
	public int CheckSurveySubmit() 
	{
		int checkSubmit = 0;

		try 
		{
			form = dbhelper.getSurveyFormSubmit();
			
			for (int i = 0; i < form.size(); i++) 
			{
				List_UserDetails nw = (List_UserDetails) form.elementAt(i);

				if (nw.getFormSubmit() != 0) 
				{
					checkSubmit = nw.getFormSubmit();
				}
			}
		} 
		catch (Exception e) 
		{

		}
		
		return checkSubmit;
	}

	// ===========Font Setting================//
	public void insertFontSize(int FontSize) 
	{
		try 
		{
			dbhelper.AddFontSize(FontSize);
		} 
		catch (Exception ex) 
		{
		}
	}

	public void UpdateFontSize(int FontSize) {
		try {
			dbhelper.updateFontSize(FontSize);
		} catch (Exception ex) {
		}
	}

	public int ReadFontSize() 
	{
		int getfontsize = 15;

		try 
		{
			getfontsize = dbhelper.getFontSize();
		} 
		catch (Exception ex) 
		{
		}
		return getfontsize;
	}

	// ==========SOAP WebService=============//
	public Vector RetrieveMoreCatNewsFromSoap(SoapObject soap, int CatID,
			int position, String header) {
		Vector vector = (Vector) soap.getProperty(0);
		newsList = new Vector();

		for (int i = 0; i < vector.size(); i++) {
			SoapObject getPropertyD = (SoapObject) vector.elementAt(i);

			String getPropertyID = getPropertyD.getProperty("id").toString();
			String getPropertyTitle = getPropertyD.getProperty("title")
					.toString();
			String getPropertyimagePathsave;
			String getPropertyimagePath = getPropertyD.getProperty("imagePath")
					.toString();

			getPropertyimagePathsave = getPropertyimagePath;

			String getPropertyIntrotext = getPropertyD.getProperty("introtext")
					.toString();
			getPropertyIntrotext = replace(getPropertyIntrotext, "。</p>",
					"。\n");
			getPropertyIntrotext = replace(getPropertyIntrotext, "。」</p>",
					"。」\n");
			getPropertyIntrotext = replace(getPropertyIntrotext, "？</p>",
					"？\n");
			getPropertyIntrotext = replace(getPropertyIntrotext, "？」</p>",
					"？」\n");
			getPropertyIntrotext = replace(getPropertyIntrotext, "！</p>",
					"！\n");
			getPropertyIntrotext = replace(getPropertyIntrotext, "！」</p>",
					"！」\n");
			getPropertyIntrotext = replace(getPropertyIntrotext, "：</p>",
					"：\n");
			getPropertyIntrotext = replace(getPropertyIntrotext, "：」</p>",
					"：」\n");
			getPropertyIntrotext = replace(getPropertyIntrotext, "”</p>",
					"”\n");
			getPropertyIntrotext = replace(getPropertyIntrotext, "”」</p>",
					"”」\n");
			getPropertyIntrotext = replace(getPropertyIntrotext, "</p>", "");
			getPropertyIntrotext = replace(getPropertyIntrotext, "。 ", "。");
			getPropertyIntrotext = replace(getPropertyIntrotext, "<br />", "\n");
			getPropertyIntrotext = replace(getPropertyIntrotext, "'", "\"");

			String getPropertyFulltext = getPropertyD.getProperty("fulltext")
					.toString();
			getPropertyFulltext = replace(getPropertyFulltext, "。</p>", "。\n");
			getPropertyFulltext = replace(getPropertyFulltext, "。」</p>",
					"。」\n");
			getPropertyFulltext = replace(getPropertyFulltext, "？</p>", "？\n");
			getPropertyFulltext = replace(getPropertyFulltext, "？」</p>",
					"？」\n");
			getPropertyFulltext = replace(getPropertyFulltext, "！</p>", "！\n");
			getPropertyFulltext = replace(getPropertyFulltext, "！」</p>",
					"！」\n");
			getPropertyFulltext = replace(getPropertyFulltext, "：</p>", "：\n");
			getPropertyFulltext = replace(getPropertyFulltext, "：」</p>",
					"：」\n");
			getPropertyFulltext = replace(getPropertyFulltext, "”</p>", "”\n");
			getPropertyFulltext = replace(getPropertyFulltext, "”」</p>",
					"”」\n");
			getPropertyFulltext = replace(getPropertyFulltext, "</p>", "");
			getPropertyFulltext = replace(getPropertyFulltext, "<br />", "\n");
			getPropertyFulltext = replace(getPropertyFulltext, "'", "\"");

			String getauthor = getPropertyD.getProperty("author").toString();

			int indexOf = 0;
			indexOf = getPropertyIntrotext.lastIndexOf('>');

			if (indexOf != -1) {
				String subStr = getPropertyIntrotext.substring(indexOf + 1,
						getPropertyIntrotext.length() - 1);

				getPropertyIntrotext = "";
				getPropertyIntrotext = "\n\n" + subStr;
			}

			String getPropertyArticleDate = getPropertyD
					.getProperty("articleDate").toString().substring(0, 10);

			List_News insertNews = new List_News(
					Integer.parseInt(getPropertyID), CatID, getPropertyTitle,
					getPropertyIntrotext, getPropertyFulltext,
					getPropertyArticleDate, getPropertyimagePathsave, getauthor);
			dbhelper.AddNews(insertNews);

			List_News addnews = new List_News();
			addnews.SetID(Integer.parseInt(getPropertyD.getProperty("id")
					.toString()));
			addnews.SetCatID(CatID);
			addnews.setNtitle(getPropertyD.getProperty("title").toString());
			addnews.setImagePaht(getPropertyD.getProperty("imagePath")
					.toString());
			addnews.setNintroText(getPropertyIntrotext);
			addnews.setFullText(getPropertyFulltext);
			addnews.setNArtical(getPropertyD.getProperty("articleDate")
					.toString());
			addnews.setAuthor(getPropertyD.getProperty("author").toString());

			newsList.addElement(addnews);
		}

		Main.getUiApplication().pushScreen(
				new Main_ParticularCategoryAllNews(CatID, position, header));

		return newsList;
	}

	public Vector RetrieveAllTopNewsFromSoap(SoapObject soap, boolean ispush,
			int newsid) {
		Vector vector = (Vector) soap.getProperty(0);
		newsList = new Vector();

		Vector vectorToDB = new Vector();

		try {
			for (int i = 0; i < vector.size(); i++) {
				SoapObject getPropertyD = (SoapObject) vector.elementAt(i);

				String getPropertyID = getPropertyD.getProperty("id")
						.toString();
				System.out.println(getPropertyID);
				String getCatID = getPropertyD.getProperty("catid").toString();
				String getPropertyTitle = getPropertyD.getProperty("title")
						.toString();
				System.out.println(getPropertyTitle);
				String getPropertyimagePathsave = "";
				String getPropertyimagePath = getPropertyD.getProperty(
						"imagePath").toString();
				getPropertyimagePathsave = getPropertyimagePath;

				String getPropertyIntrotext = getPropertyD.getProperty(
						"introtext").toString();
				getPropertyIntrotext = replace(getPropertyIntrotext, "。</p>",
						"。\n");
				getPropertyIntrotext = replace(getPropertyIntrotext,
						"。」</p>", "。」\n");
				getPropertyIntrotext = replace(getPropertyIntrotext, "？</p>",
						"？\n");
				getPropertyIntrotext = replace(getPropertyIntrotext,
						"？」</p>", "？」\n");
				getPropertyIntrotext = replace(getPropertyIntrotext, "！</p>",
						"！\n");
				getPropertyIntrotext = replace(getPropertyIntrotext,
						"！」</p>", "！」\n");
				getPropertyIntrotext = replace(getPropertyIntrotext, "：</p>",
						"：\n");
				getPropertyIntrotext = replace(getPropertyIntrotext,
						"：」</p>", "：」\n");
				getPropertyIntrotext = replace(getPropertyIntrotext, "”</p>",
						"”\n");
				getPropertyIntrotext = replace(getPropertyIntrotext,
						"”」</p>", "”」\n");
				getPropertyIntrotext = replace(getPropertyIntrotext, "</p>", "");
				getPropertyIntrotext = replace(getPropertyIntrotext, "。 ",
						"。");
				getPropertyIntrotext = replace(getPropertyIntrotext, "<br />",
						"\n");
				getPropertyIntrotext = replace(getPropertyIntrotext, "'", "\"");

				String getPropertyFulltext = getPropertyD.getProperty(
						"fulltext").toString();
				getPropertyFulltext = replace(getPropertyFulltext, "。</p>",
						"。\n");
				getPropertyFulltext = replace(getPropertyFulltext, "。」</p>",
						"。」\n");
				getPropertyFulltext = replace(getPropertyFulltext, "？</p>",
						"？\n");
				getPropertyFulltext = replace(getPropertyFulltext, "？」</p>",
						"？」\n");
				getPropertyFulltext = replace(getPropertyFulltext, "！</p>",
						"！\n");
				getPropertyFulltext = replace(getPropertyFulltext, "！」</p>",
						"！」\n");
				getPropertyFulltext = replace(getPropertyFulltext, "：</p>",
						"：\n");
				getPropertyFulltext = replace(getPropertyFulltext, "：」</p>",
						"：」\n");
				getPropertyFulltext = replace(getPropertyFulltext, "”</p>",
						"”\n");
				getPropertyFulltext = replace(getPropertyFulltext, "”」</p>",
						"”」\n");
				getPropertyFulltext = replace(getPropertyFulltext, "</p>", "");
				getPropertyFulltext = replace(getPropertyFulltext, "<br />",
						"\n");
				getPropertyFulltext = replace(getPropertyFulltext, "'", "\"");

				String getauthor = getPropertyD.getProperty("author")
						.toString();

				int indexOf = 0;
				indexOf = getPropertyIntrotext.lastIndexOf('>');

				if (indexOf != -1) {
					String subStr = getPropertyIntrotext.substring(indexOf + 1,
							getPropertyIntrotext.length() - 1);

					getPropertyIntrotext = "";
						getPropertyIntrotext = "\n\n" + subStr;
				}
				
				String getPropertyArticleDate = getPropertyD
						.getProperty("articleDate").toString().substring(0, 10);

				List_News insertNews = new List_News(
						Integer.parseInt(getPropertyID),
						Integer.parseInt(getCatID), getPropertyTitle,
						getPropertyIntrotext, getPropertyFulltext,
						getPropertyArticleDate, getPropertyimagePathsave,
						getauthor);

				vectorToDB.addElement(insertNews);

				List_News addnews = new List_News();
				addnews.SetID(Integer.parseInt(getPropertyD.getProperty("id")
						.toString()));
				addnews.SetCatID(Integer.parseInt(getPropertyD.getProperty(
						"catid").toString()));
				addnews.setNtitle(getPropertyD.getProperty("title").toString());
				addnews.setImagePaht(getPropertyD.getProperty("imagePath")
						.toString());
				addnews.setNintroText(getPropertyIntrotext);
				addnews.setFullText(getPropertyFulltext);
				addnews.setNArtical(getPropertyD.getProperty("articleDate")
						.toString());
				addnews.setAuthor(getPropertyD.getProperty("author").toString());

				newsList.addElement(addnews);
			}
		} catch (Exception ex) {
		}

		dbhelper.AddNewsBatch(vectorToDB);

		if (ispush)
			Main.getUiApplication().pushScreen(
					new Main_PushNotificationNews(newsid));
		else
			Main.getUiApplication().pushScreen(new Main_AllLatestNews());

		return newsList;
	}

	public void GetCommentNews(final int gCommentNewsID, final boolean ispush) {
		SoapObject request = new SoapObject(NAMESPACE, get_Comment_METHOD_NAME);
		request.addProperty("itemid", new Integer(gCommentNewsID));

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.bodyOut = request;
		envelope.dotNet = false;

		HttpTransport ht = new HttpTransport(URL + ht_params);
		ht.debug = true;

		try {
			ht.call(get_Comment_SOAP_ACTION, envelope);

			SoapObject result = (SoapObject) envelope.bodyIn;

			RetrieveFromSoap(result, gCommentNewsID, ispush);
		} catch (Exception ex) {
		}
	}

	public void RetrieveFromSoap(SoapObject soap, int newsid, boolean ispush) {
		sallCommentList = new Vector();

		Vector vector = (Vector) soap.getProperty(0);

		for (int i = 0; i < vector.size(); i++) {
			SoapObject getPropertyD = (SoapObject) vector.elementAt(i);

			List_NewsComment addcomment = new List_NewsComment();
			addcomment.setcommentID(Integer.parseInt(getPropertyD.getProperty(
					"id").toString()));
			addcomment.setcommentDate(getPropertyD.getProperty("date")
					.toString());
			addcomment.setItemID(Integer.parseInt(getPropertyD.getProperty(
					"item_id").toString()));
			addcomment.setUserName(getPropertyD.getProperty("name").toString());
			addcomment.setCommentContent(getPropertyD.getProperty("comment")
					.toString());
			addcomment.setAproveStauts(Integer.parseInt(getPropertyD
					.getProperty("approval_status").toString()));

			sallCommentList.addElement(addcomment);
		}

		if (ispush)
			Main.getUiApplication().pushScreen(
					new Main_PushNotificationComments(newsid, sallCommentList));
		else
			Main.getUiApplication().pushScreen(
					new Main_Comments(newsid, sallCommentList));
	}

	public String replace(String source, String pattern, String replacement) {
		if (source == null) {
			return "";
		}

		StringBuffer sb = new StringBuffer();
		int idx = -1;
		int patIdx = 0;

		while ((idx = source.indexOf(pattern, patIdx)) != -1) {
			sb.append(source.substring(patIdx, idx));
			sb.append(replacement);
			patIdx = idx + pattern.length();
		}
		sb.append(source.substring(patIdx));
		return sb.toString();
	}
}