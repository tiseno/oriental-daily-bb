package mypackage;

import net.rim.device.api.system.Application;
import net.rim.device.api.system.ApplicationManager;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.system.GlobalEventListener;
import net.rim.device.api.system.SystemListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

public class Main extends UiApplication implements GlobalEventListener, SystemListener 
{
	public static void main(String[] args) 
	{
		String param = "";

		if (args.length == 0) 
		{
			param = "gui";
		} 
		else 
		{
			param = args[0];
		}

		Main theApp = new Main();

		if (ApplicationManager.getApplicationManager().inStartup()) 
		{
			theApp.addSystemListener(theApp);
		} 
		else 
		{
			theApp.addGlobalEventListener(theApp);
			theApp.doStartUpWorkLater(param);
		}
		
		theApp.enterEventDispatcher();
	}

	private void doStartUpWorkLater(final String str) 
	{
		invokeLater(new Runnable() 
		{
			public void run() 
			{
				doStartUpWork(str);
			}
		});
	}

	private void doStartUpWork(String str) 
	{
		if (str != null && str != "gui") 
		{
			Main.this.pushScreen(new Main_PushNotificationNews(Integer.parseInt(str)));
		} 
		else 
		{
			pushScreen(new MyScreen());
		}
	}

	public final class MyScreen extends MainScreen 
	{
		private Database_Webservice webservice;
		String path;

		public MyScreen() 
		{
			webservice = new Database_Webservice();

			if (Display.getWidth() > Display.getHeight())
				path = "land_logo_background.png";
			else
				path = "port_logo_background.png";

			this.getMainManager().setBackground(
					BackgroundFactory.createBitmapBackground(
							Bitmap.getBitmapResource(path),
							Background.POSITION_X_LEFT,
							Background.POSITION_Y_TOP,
							Background.REPEAT_SCALE_TO_FIT));
			
			Main.this.invokeLater(new Runnable() 
			{
				public void run() 
				{
					if (webservice.CheckSurveySubmit() == 1) 
					{
						webservice.LoadtodayNews();
						
						if (webservice.news.size() < 10 || webservice.news == null) 
						{	
							webservice.DeleteDB();
							webservice = new Database_Webservice();
							webservice.InsertFormSubmit(1, 1, 1, 1, 1);
							webservice.insertFontSize(25);
						}
						
						Main.this.pushScreen(new Main_AllLatestNews(true));
						 //Main.this.pushScreen(new
						 //Main_PushNotificationNews(21229));
					} 
					else if (webservice.CheckSurveySubmit() == 0) 
					{	
						Main.this.pushScreen(new Menu_NewUserForm());
					}
					
					Main.getUiApplication().popScreen(MyScreen.this);
				}
			}, 3 * 1000, false);
		}
	}

	public void batteryGood() {
	}

	public void batteryLow() {
	}

	public void batteryStatusChange(int arg0) {
	}

	public void powerOff() {
	}

	public void powerUp() {
		removeSystemListener(this);
		doStartUpWork(null);
	}

	public void eventOccurred(long guid, int data0, int data1, Object object0,
			Object object1) {
		if (guid == 0x947cb23a75c60e2dL) {
			if (object0 instanceof String) {
				String id = (String) object0;
				Main.this.pushScreen(new Main_PushNotificationNews(Integer
						.parseInt(id)));
				Application.getApplication().requestForeground();
			}
		}
	}
}