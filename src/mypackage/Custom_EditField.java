package mypackage;

import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.VirtualKeyboard;
import net.rim.device.api.ui.component.EditField;

public class Custom_EditField extends EditField {
	private int width, row, color;

	Custom_EditField(long style, int width, int row) {
		super(style);
		this.width = width;
		this.row = row;
	}

	public int getPreferredHeight() {
		return Font.getDefault().getHeight() * row;
	}

	public int getPreferredWidth() {
		return width;
	}

	protected void onFocus(int direction) {
		if (VirtualKeyboard.isSupported())
			Main.getUiApplication().getActiveScreen().getVirtualKeyboard()
					.setVisibility(VirtualKeyboard.SHOW_FORCE);
		invalidate();
		super.onFocus(direction);
	}

	protected void onUnfocus() {
		if (VirtualKeyboard.isSupported())
			Main.getUiApplication().getActiveScreen().getVirtualKeyboard()
					.setVisibility(VirtualKeyboard.HIDE_FORCE);
		invalidate();
		super.onUnfocus();
	}

	public boolean isFocusable() {
		return true;
	}

	protected void layout(int maxWidth, int maxHeight) {
		super.layout(maxWidth,
				Math.min(maxHeight, Font.getDefault().getHeight() * row));
		super.setExtent(maxWidth,
				Math.min(maxHeight, Font.getDefault().getHeight() * row));
	}

	protected void paint(Graphics graphics) {
		int rectHeight = getPreferredHeight();
		int rectWidth = getPreferredWidth();
		try {
			color = Color.BLACK;
			graphics.setColor(color);
			graphics.drawRect(0, 0, rectWidth, rectHeight);
			super.paint(graphics);
		} finally {
			graphics.setColor(color);
		}
	}
}
