package mypackage;

import java.util.Vector;

import net.rim.device.api.database.Cursor;
import net.rim.device.api.database.Database;
import net.rim.device.api.database.DatabaseFactory;
import net.rim.device.api.database.Row;
import net.rim.device.api.database.Statement;
import net.rim.device.api.io.URI;
import net.rim.device.api.ui.component.Dialog;

public class Database_DataBaseHelper {
	private static final String dbName = "OrientalDaily.db";

	/* ========================================== */
	private static final String NewsCategoryTable = "tblCatNews";
	private static final String colNewsCatinid = "id";
	private static final String colNewsCatid = "News_Cat_ID";
	private static final String colNewsCatNames = "News_Cat_Name";

	private static final String NewsTable = "tblNews";
	private static final String colNewsinid = "id";
	private static final String colNewsid = "News_id";
	private static final String colTitle = "News_Title";
	private static final String colIntroText = "News_IntroText";
	private static final String colFullText = "News_FullText";
	private static final String colArticalDate = "News_ArticalDate";
	private static final String colImagePath = "News_ImagePath";
	private static final String colNewsCat = "News_Cat_ID";
	private static final String colNewsAuthor = "News_Author";

	static final String SurveyFormTable = "tblSurveyForm";
	static final String colFormid = "Form_id";
	static final String colFormUserName = "UserName";
	static final String colFormUserEmail = "UserEmail";
	static final String colFormSubmit = "SurveyForm_Submit";
	static final String colnotification = "notificationsetting";
	static final String colsound = "notificationsound";
	static final String colvibrate = "notificationvibrate";
	static final String colpopup = "notificationpopup";

	static final String FontSettingTable = "tblFontSetting";
	static final String colFontid = "FontSetting_id";
	static final String colFontsize = "FontSetting_size";

	public Database sqliteDB;
	URI uri;

	/* ============================================= */
	public Database_DataBaseHelper() 
	{
		try 
		{
			uri = URI.create("file:///SDCard/Databases/Oriental_02/" + dbName);

			if (detectDatabaseExistence() == false) 
			{
				createDatabase();
				createTables();
			}
		} 
		catch (Exception e) 
		{

		}
	}

	public void deleteDatabase() 
	{
		try 
		{
			DatabaseFactory.delete(uri);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	/* reset !!! */
	public void resetDatabaseFunction()
	{	
		try 
		{
			// delete the database first
			URI myURI = URI.create("file:///SDCard/Databases/Oriental_02/" + dbName);
			
			DatabaseFactory.delete(myURI);
			
			// create a new database and the related table again
			createDatabase();
			createTables();
			
			List_UserDetails SubmitForm = new List_UserDetails(1, 1, 1, 1, 1);
			
			//initialize survey table and font table
			AddFontSize(15);
			AddSurveyForm(SubmitForm);
			
			//System.exit(0);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	/* reset !!! */

	/* ================ New functions for BB =================== */
	public boolean detectDatabaseExistence() 
	{
		try 
		{
			boolean dbStatus = DatabaseFactory.exists(uri);

			if (dbStatus) 
			{
				return true;
			} 
			else 
			{
				return false;
			}
		} 
		catch (Exception e) 
		{
		}

		return false;
	}

	public void createDatabase() {
		try {
			sqliteDB = DatabaseFactory.openOrCreate(uri);
			sqliteDB.close();
		} catch (Exception e) {
		}
	}

	public void createTables() {
		try {
			sqliteDB = DatabaseFactory.open(uri);

			String[] commands = new String[] {
					"CREATE TABLE " + NewsCategoryTable + " ( "
							+ colNewsCatinid
							+ " INTEGER PRIMARY KEY AUTOINCREMENT, "
							+ colNewsCatid + " INTEGER, " + colNewsCatNames
							+ " TEXT )",
					"CREATE TABLE " + NewsTable + " ( " + colNewsinid
							+ " INTEGER PRIMARY KEY AUTOINCREMENT, "
							+ colNewsid + " INTEGER, " + colNewsCat
							+ " INTEGER, " + colTitle + " TEXT, "
							+ colIntroText + " TEXT, " + colFullText
							+ " TEXT, " + colArticalDate + " TEXT, "
							+ colImagePath + " TEXT, " + colNewsAuthor
							+ " TEXT )",
					"CREATE TABLE " + SurveyFormTable + " ( " + colFormid
							+ " INTEGER PRIMARY KEY AUTOINCREMENT, "
							+ colFormSubmit + " INTEGER, " + colnotification
							+ " INTEGER, " + colsound + " INTEGER, "
							+ colvibrate + " INTEGER, " + colpopup
							+ " INTEGER )",
					"CREATE TABLE " + FontSettingTable + " ( " + colFontid
							+ " INTEGER PRIMARY KEY AUTOINCREMENT, "
							+ colFontsize + " INTEGER )" };

			for (int i = 0; i < commands.length; i++) {
				Statement st = sqliteDB.createStatement(commands[i]);
				st.prepare();
				st.execute();
				st.close();
			}

			sqliteDB.close();
		} catch (Exception e) {
		}
	}

	/* ================Category News==================== */
	public void AddCatNews(List_CategoryNews NewsCats) 
	{
		Statement st = null;
		
		try 
		{
			sqliteDB = DatabaseFactory.open(uri);

			st = sqliteDB.createStatement("INSERT INTO "
					+ NewsCategoryTable + "(" + colNewsCatid + ", "
					+ colNewsCatNames + ") " + "VALUES (" + NewsCats.getCatID()
					+ ", '" + NewsCats.getNewCatName() + "')");

			st.prepare();
			st.execute();
		} 
		catch (Exception ex) 
		{
		}finally {
			try {
				st.close();
				sqliteDB.close();
			} catch (Exception e) {

			}
		}
	}

	public Vector getAllNewsCategories() 
	{
		Statement st = null;
		Cursor cur = null;
		
		Vector newNewsCat = new Vector();

		String selectQuery = "SELECT * FROM " + NewsCategoryTable + " WHERE "
				+ colNewsCatid + " != 10";
		try {
			sqliteDB = DatabaseFactory.open(uri);
			st = sqliteDB.createStatement(selectQuery);
			st.prepare();

			cur = st.getCursor();

			if (cur.first()) {
				do {
					Row r1 = cur.getRow();

					List_CategoryNews Catnews = new List_CategoryNews();
					Catnews.SetCatID(r1.getInteger(1));
					Catnews.setNewsCatName(r1.getString(2));

					newNewsCat.addElement(Catnews);
				} while (cur.next());
			}
		} 
		catch (Exception ex) 
		{
		}finally {
			try {
				cur.close();
				st.close();
				sqliteDB.close();
			} catch (Exception e) {

			}
		}
		return newNewsCat;
	}

	public void DeleteNewsCatFT() 
	{
		Statement st = null;
		
		try {
			sqliteDB = DatabaseFactory.open(uri);

			st = sqliteDB.createStatement("DELETE From "
					+ NewsCategoryTable + "");
			st.prepare();
			st.execute();
		} 
		catch (Exception ex) {
		}finally {
			try {
				st.close();
				sqliteDB.close();
			} catch (Exception e) {

			}
		}
	}

	/* ================News==================== */
	public void AddNews(List_News addNews) {
		Statement st = null;

		try {
			sqliteDB = DatabaseFactory.open(uri);

			st = sqliteDB.createStatement("INSERT INTO " + NewsTable + "("
					+ colNewsid + ", " + colNewsCatid + ", " + colTitle + ", "
					+ colIntroText + ", " + colFullText + ", " + colArticalDate
					+ ", " + colImagePath + ", " + colNewsAuthor + ") "
					+ "VALUES (" + addNews.getID() + ", " + addNews.getCatid()
					+ ", '" + addNews.getNtitle() + "', '"
					+ addNews.getNintrotext() + "', '" + addNews.getFullText()
					+ "', '" + addNews.getNArticalD() + "', '"
					+ addNews.getImagePath() + "', '" + addNews.getAuthor()
					+ "')");

			st.prepare();
			st.execute();

		} catch (Exception e) {
		} finally {
			try {
				st.close();
				sqliteDB.close();
				
				//reset functions 
				//resetDatabaseFunction();
			} catch (Exception e) {

			}
		}
	}

	public void AddNewsBatch(Vector vector) {
		System.gc();
		Statement st = null;
		try {
			sqliteDB = DatabaseFactory.open(uri);

			st = sqliteDB.createStatement("INSERT INTO " + NewsTable + "("
					+ colNewsid + ", " + colNewsCatid + ", " + colTitle + ", "
					+ colIntroText + ", " + colFullText + ", " + colArticalDate
					+ ", " + colImagePath + ", " + colNewsAuthor + ") "
					+ "VALUES (?,?,?,?,?,?,?,?)");

			st.prepare();

			for (int i = 0; i < vector.size(); i++) {
				List_News addNews = (List_News) vector.elementAt(i);
				st.bind(1, addNews.getID());
				st.bind(2, addNews.getCatid());
				st.bind(3, addNews.getNtitle());
				st.bind(4, addNews.getNintrotext());
				st.bind(5, addNews.getFullText());
				st.bind(6, addNews.getNArticalD());
				st.bind(7, addNews.getImagePath());
				st.bind(8, addNews.getAuthor());
				st.execute();
				st.reset();
			}
		} catch (Exception e) {
		} finally {
			try {
				st.close();
				sqliteDB.close();
				
				// reset !!
				//resetDatabaseFunction();
			} catch (Exception e) {

			}
		}
	}

	public Vector getAllNews() 
	{
		Statement st = null;
		Cursor cur = null;
		
		Vector newNews = new Vector();

		String selectQuery = "SELECT * FROM " + NewsTable + " ORDER BY "
				+ colNewsid + " DESC ";

		try {
			sqliteDB = DatabaseFactory.open(uri);

			st = sqliteDB.createStatement(selectQuery);
			st.prepare();

			cur = st.getCursor();

			if (cur.first()) {
				do {
					Row r1 = cur.getRow();

					List_News news = new List_News();
					news.SetID(r1.getInteger(1));
					news.SetCatID(r1.getInteger(2));
					news.setNtitle(r1.getString(3));
					news.setNintroText(r1.getString(4));
					news.setFullText(r1.getString(5));
					news.setNArtical(r1.getString(6));
					news.setImagePaht(r1.getString(7));
					news.setAuthor(r1.getString(8));

					newNews.addElement(news);

				} while (cur.next());
			}
		} 
		catch (Exception ex) 
		{
		}
		finally {
			try {
				cur.close();
				st.close();
				sqliteDB.close();
			} catch (Exception e) {
			}
		}

		return newNews;
	}

	public Vector getAllTodaysNews() 
	{
		Statement st = null;
		Cursor cur = null;
		
		Vector newNews = new Vector();

		String selectQuery = "SELECT * FROM " + NewsTable + " LEFT JOIN "
				+ NewsCategoryTable + " ON " + NewsTable + "." + colNewsCat
				+ "=" + NewsCategoryTable + "." + colNewsCatid + " ORDER BY "
				+ NewsTable + "." + colNewsid + " DESC limit 10";

		try 
		{
			sqliteDB = DatabaseFactory.open(uri);

			st = sqliteDB.createStatement(selectQuery);
			st.prepare();
			cur = st.getCursor();

			if (cur.first()) 
			{
				do 
				{
					Row r1 = cur.getRow();

					List_News news = new List_News();
					news.SetID(r1.getInteger(1));
					news.SetCatID(r1.getInteger(2));
					news.setNtitle(r1.getString(3));
					news.setNintroText(r1.getString(4));
					news.setFullText(r1.getString(5));
					news.setNArtical(r1.getString(6));
					news.setImagePaht(r1.getString(7));
					news.setAuthor(r1.getString(8));
					news.SetCatIDs(r1.getInteger(10));
					news.setNewsCatName(r1.getString(11));

					newNews.addElement(news);
				} while (cur.next());
			}
		} 
		catch (Exception ex) 
		{
			//Dialog.inform("getAllTodaysNews error");
		}
		finally 
		{
			try 
			{
				cur.close();
				st.close();
				sqliteDB.close();
	
			} catch (Exception e) {

			}
		}
		return newNews;
	}

	public Vector getSingleCatNews(int NewsCatID) 
	{
		Statement st = null;
		Cursor cur = null;
		Vector newNews = new Vector();
		String selectQuery = "SELECT * FROM " + NewsTable + " LEFT JOIN "
				+ NewsCategoryTable + " ON " + NewsTable + "." + colNewsCat
				+ "=" + NewsCategoryTable + "." + colNewsCatid + " WHERE "
				+ NewsTable + "." + colNewsCat + " = " + NewsCatID
				+ " ORDER BY " + NewsTable + "." + colNewsid + " DESC ";

		try {
			sqliteDB = DatabaseFactory.open(uri);

			st = sqliteDB.createStatement(selectQuery);
			st.prepare();

			cur = st.getCursor();

			if (cur.first()) {
				do {
					Row r1 = cur.getRow();

					List_News news = new List_News();
					news.SetID(r1.getInteger(1));
					news.SetCatID(r1.getInteger(2));
					news.setNtitle(r1.getString(3));
					news.setNintroText(r1.getString(4));
					news.setFullText(r1.getString(5));
					news.setNArtical(r1.getString(6));
					news.setImagePaht(r1.getString(7));
					news.setAuthor(r1.getString(8));
					news.SetCatIDs(r1.getInteger(10));
					news.setNewsCatName(r1.getString(11));

					newNews.addElement(news);

				} while (cur.next());
			}
		} catch (Exception ex) 
		{
		}finally {
			try {
				cur.close();
				st.close();
				sqliteDB.close();
			} catch (Exception e) {

			}
		}

		return newNews;
	}

	public Vector getSingleNews(int getNews) 
	{
		Statement st = null;
		Cursor cur = null;
		
		Vector newNews = new Vector();

		String selectQuery = "SELECT * FROM " + NewsTable + " LEFT JOIN "
				+ NewsCategoryTable + " ON " + NewsTable + "." + colNewsCat
				+ "=" + NewsCategoryTable + "." + colNewsCatid + " WHERE "
				+ NewsTable + "." + colNewsid + " = " + getNews;

		try {
			
			sqliteDB = DatabaseFactory.open(uri);

			st = sqliteDB.createStatement(selectQuery);
			st.prepare();

			cur = st.getCursor();

			if (cur.first()) {
				do {
					Row r1 = cur.getRow();

					List_News news = new List_News();
					news.SetID(r1.getInteger(1));
					news.SetCatID(r1.getInteger(2));
					news.setNtitle(r1.getString(3));
					news.setNintroText(r1.getString(4));
					news.setFullText(r1.getString(5));
					news.setNArtical(r1.getString(6));
					news.setImagePaht(r1.getString(7));
					news.setAuthor(r1.getString(8));
					news.SetCatIDs(r1.getInteger(10));
					news.setNewsCatName(r1.getString(11));

					newNews.addElement(news);
				} while (cur.next());
			}
		} catch (Exception ex) 
		{
			//Dialog.inform("getSingleNews exception");
		}
		finally {
			try {
				cur.close();
				st.close();
				sqliteDB.close();
			} catch (Exception e) {

			}
		}

		return newNews;
	}

	public void DeleteNewsByCatID(int CatNewsid) 
	{
		Statement st = null;
		
		try {
			sqliteDB = DatabaseFactory.open(uri);

			st = sqliteDB.createStatement("DELETE From " + NewsTable
					+ " WHERE " + colNewsCatid + "=" + CatNewsid + "");
			st.prepare();
			st.execute();
		} catch (Exception ex) 
		{
		}finally {
			try {
				st.close();
				sqliteDB.close();
			} catch (Exception e) {

			}
		}
	}

	public void DeleteNewsFT() 
	{
		Statement st = null;
		
		try {
			sqliteDB = DatabaseFactory.open(uri);

			st = sqliteDB.createStatement("DELETE From " + NewsTable
					+ "");
			st.prepare();
			st.execute();
		} 
		catch (Exception ex) {
		}finally {
			try {
				st.close();
				sqliteDB.close();
			} catch (Exception e) {

			}
		}
	}

	/* ================SurveyForm==================== */
	public void AddSurveyForm(List_UserDetails addsubmit) 
	{
		try 
		{
			sqliteDB = DatabaseFactory.open(uri);

			Statement st = sqliteDB.createStatement("INSERT INTO "
					+ SurveyFormTable + "(" + colFormSubmit + ", "
					+ colnotification + ", " + colsound + ", " + colvibrate
					+ ", " + colpopup + ") " + "VALUES ("
					+ addsubmit.getFormSubmit() + ", "
					+ addsubmit.getnotification() + ", " + addsubmit.getsound()
					+ ", " + addsubmit.getvibrate() + ", "
					+ addsubmit.getpopup() + ")");

			st.prepare();
			st.execute();
			st.close();
			sqliteDB.close();
		} 
		catch (Exception e) 
		{
			
		}
	}

	public Vector getSurveyFormSubmit() 
	{
		Vector SurveySubmit = new Vector();

		String selectQuery = "SELECT * FROM " + SurveyFormTable;

		try 
		{
			sqliteDB = DatabaseFactory.open(uri);

			Statement st = sqliteDB.createStatement(selectQuery);
			st.prepare();

			Cursor cur = st.getCursor();

			if (cur.first()) 
			{
				do 
				{
					Row r1 = cur.getRow();

					List_UserDetails newsSurvey = new List_UserDetails();
					newsSurvey.SetID(r1.getInteger(0));
					newsSurvey.setFormSubmit(r1.getInteger(1));
					newsSurvey.setnotification(r1.getInteger(2));
					newsSurvey.setsound(r1.getInteger(3));
					newsSurvey.setvibrate(r1.getInteger(4));
					newsSurvey.setpopup(r1.getInteger(5));
					
					SurveySubmit.addElement(newsSurvey);

				} while (cur.next());
			}

			cur.close();
			st.close();
			sqliteDB.close();
		} catch (Exception ex) {
		}

		return SurveySubmit;
	}

	/*
	 * // ===============notification Setting================== // public int
	 * getnotification() { int number = 2;
	 * 
	 * String selectQuery = "SELECT * FROM " + SurveyFormTable;
	 * 
	 * try { sqliteDB = DatabaseFactory.open(uri);
	 * 
	 * Statement st = sqliteDB.createStatement(selectQuery); st.prepare();
	 * 
	 * Cursor cur = st.getCursor();
	 * 
	 * if (cur.first()) { do { Row r1 = cur.getRow();
	 * 
	 * number = r1.getInteger(2);
	 * 
	 * } while (cur.next()); }
	 * 
	 * cur.close(); st.close(); sqliteDB.close(); } catch (Exception ex) { }
	 * 
	 * return number; }
	 * 
	 * public void updatenotification(int notification) { try { sqliteDB =
	 * DatabaseFactory.open(uri);
	 * 
	 * Statement st = sqliteDB.createStatement("UPDATE " + SurveyFormTable +
	 * " SET " + colnotification + "=" + notification + ""); st.prepare();
	 * st.execute();
	 * 
	 * st.close(); sqliteDB.close(); } catch (Exception e) { } }
	 * 
	 * public int getsound() { int number = 2;
	 * 
	 * String selectQuery = "SELECT * FROM " + SurveyFormTable;
	 * 
	 * try { sqliteDB = DatabaseFactory.open(uri);
	 * 
	 * Statement st = sqliteDB.createStatement(selectQuery); st.prepare();
	 * 
	 * Cursor cur = st.getCursor();
	 * 
	 * if (cur.first()) { do { Row r1 = cur.getRow();
	 * 
	 * number = r1.getInteger(3);
	 * 
	 * } while (cur.next()); }
	 * 
	 * cur.close(); st.close(); sqliteDB.close(); } catch (Exception ex) { }
	 * 
	 * return number; }
	 * 
	 * public void updatesound(int sound) { try { sqliteDB =
	 * DatabaseFactory.open(uri);
	 * 
	 * Statement st = sqliteDB.createStatement("UPDATE " + SurveyFormTable +
	 * " SET " + colsound + "=" + sound + ""); st.prepare(); st.execute();
	 * 
	 * st.close(); sqliteDB.close(); } catch (Exception e) { } }
	 * 
	 * public int getvibration() { int number = 2;
	 * 
	 * String selectQuery = "SELECT * FROM " + SurveyFormTable;
	 * 
	 * try { sqliteDB = DatabaseFactory.open(uri);
	 * 
	 * Statement st = sqliteDB.createStatement(selectQuery); st.prepare();
	 * 
	 * Cursor cur = st.getCursor();
	 * 
	 * if (cur.first()) { do { Row r1 = cur.getRow();
	 * 
	 * number = r1.getInteger(4);
	 * 
	 * } while (cur.next()); }
	 * 
	 * cur.close(); st.close(); sqliteDB.close(); } catch (Exception ex) { }
	 * return number; }
	 * 
	 * public void updatevibration(int vibration) { try { sqliteDB =
	 * DatabaseFactory.open(uri);
	 * 
	 * Statement st = sqliteDB.createStatement("UPDATE " + SurveyFormTable +
	 * " SET " + colvibrate + "=" + vibration + ""); st.prepare(); st.execute();
	 * 
	 * st.close(); sqliteDB.close(); } catch (Exception e) { } }
	 * 
	 * public int getpopup() { int number = 2;
	 * 
	 * String selectQuery = "SELECT * FROM " + SurveyFormTable;
	 * 
	 * try { sqliteDB = DatabaseFactory.open(uri);
	 * 
	 * Statement st = sqliteDB.createStatement(selectQuery); st.prepare();
	 * 
	 * Cursor cur = st.getCursor();
	 * 
	 * if (cur.first()) { do { Row r1 = cur.getRow();
	 * 
	 * number = r1.getInteger(5);
	 * 
	 * } while (cur.next()); }
	 * 
	 * cur.close(); st.close(); sqliteDB.close(); } catch (Exception ex) { }
	 * return number; }
	 * 
	 * public void updatepopup(int popup) { try { sqliteDB =
	 * DatabaseFactory.open(uri);
	 * 
	 * Statement st = sqliteDB.createStatement("UPDATE " + SurveyFormTable +
	 * " SET " + colpopup + "=" + popup + ""); st.prepare(); st.execute();
	 * 
	 * st.close(); sqliteDB.close(); } catch (Exception e) { } }
	 */

	/* =============Font Setting=============== */
	public void AddFontSize(int fontSize) 
	{
		//Dialog.inform("fontsize ... " + fontSize);
		
		try 
		{
			sqliteDB = DatabaseFactory.open(uri);

			Statement st = sqliteDB.createStatement("INSERT INTO "
					+ FontSettingTable + "(" + colFontsize + ") " + "VALUES ("
					+ fontSize + ")");

			st.prepare();
			st.execute();
			st.close();
			sqliteDB.close();
		} 
		catch (Exception e) 
		{
		}
	}

	public void updateFontSize(int FontSize) {
		try {
			sqliteDB = DatabaseFactory.open(uri);

			Statement st = sqliteDB.createStatement("UPDATE "
					+ FontSettingTable + " SET " + colFontsize + "=" + FontSize
					+ "");
			st.prepare();
			st.execute();

			st.close();
			sqliteDB.close();
		} catch (Exception e) {
		}
	}

	public int getFontSize() {
		int FontSize = 0;

		String selectQuery = "SELECT * FROM " + FontSettingTable;

		try {
			sqliteDB = DatabaseFactory.open(uri);

			Statement st = sqliteDB.createStatement(selectQuery);
			st.prepare();

			Cursor cur = st.getCursor();

			if (cur.first()) 
			{
				do 
				{
					Row r1 = cur.getRow();

					FontSize = r1.getInteger(1);

				} while (cur.next());
			}

			cur.close();
			st.close();
			sqliteDB.close();
		} catch (Exception ex) {
		}
		return FontSize;
	}
}
