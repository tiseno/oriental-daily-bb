package mypackage;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.XYEdges;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.decor.BackgroundFactory;
import net.rim.device.api.ui.decor.BorderFactory;

public class Custom_ButtonField extends ButtonField {
	Bitmap mNormal;
	Bitmap mFocused;
	Bitmap mActive;

	int mWidth;
	int mHeight;

	private int color = -1;
	String text;

	public Custom_ButtonField(Bitmap normal, Bitmap focused, Bitmap active) {
		super(CONSUME_CLICK | Field.FOCUSABLE | Field.FIELD_HCENTER
				| Field.FIELD_VCENTER);
		mNormal = normal;
		mFocused = focused;
		mActive = active;
		mWidth = mNormal.getWidth();
		mHeight = mNormal.getHeight();
		setMargin(0, 0, 0, 0);
		setPadding(0, 0, 0, 0);
		setBorder(BorderFactory.createSimpleBorder(new XYEdges(0, 0, 0, 0)));
		setBorder(VISUAL_STATE_ACTIVE,
				BorderFactory.createSimpleBorder(new XYEdges(0, 0, 0, 0)));
		setBackground(VISUAL_STATE_NORMAL,
				BackgroundFactory.createBitmapBackground(normal));
		setBackground(VISUAL_STATE_FOCUS,
				BackgroundFactory.createBitmapBackground(focused));
		setBackground(VISUAL_STATE_ACTIVE,
				BackgroundFactory.createBitmapBackground(active));
	}

	public Custom_ButtonField(String text, Bitmap normal, Bitmap focused,
			Bitmap active, int color) {
		super(CONSUME_CLICK | Field.FOCUSABLE | Field.FIELD_HCENTER
				| Field.FIELD_VCENTER);
		this.color = color;
		this.text = text;
		mNormal = normal;
		mFocused = focused;
		mActive = active;
		mWidth = mNormal.getWidth();
		mHeight = mNormal.getHeight();
		setMargin(0, 0, 0, 0);
		setPadding(0, 0, 0, 0);
		setBorder(BorderFactory.createSimpleBorder(new XYEdges(0, 0, 0, 0)));
		setBorder(VISUAL_STATE_ACTIVE,
				BorderFactory.createSimpleBorder(new XYEdges(0, 0, 0, 0)));
		setBackground(VISUAL_STATE_NORMAL,
				BackgroundFactory.createBitmapBackground(normal));
		setBackground(VISUAL_STATE_FOCUS,
				BackgroundFactory.createBitmapBackground(focused));
		setBackground(VISUAL_STATE_ACTIVE,
				BackgroundFactory.createBitmapBackground(active));
	}

	public Custom_ButtonField(String text, Bitmap normal, Bitmap focused,
			Bitmap active, int color, long style) {
		super(style);
		this.color = color;
		this.text = text;
		mNormal = normal;
		mFocused = focused;
		mActive = active;
		mWidth = mNormal.getWidth();
		mHeight = mNormal.getHeight();
		setMargin(0, 0, 0, 0);
		setPadding(0, 0, 0, 0);
		setBorder(BorderFactory.createSimpleBorder(new XYEdges(0, 0, 0, 0)));
		setBorder(VISUAL_STATE_ACTIVE,
				BorderFactory.createSimpleBorder(new XYEdges(0, 0, 0, 0)));
		setBackground(VISUAL_STATE_NORMAL,
				BackgroundFactory.createBitmapBackground(normal));
		setBackground(VISUAL_STATE_FOCUS,
				BackgroundFactory.createBitmapBackground(focused));
		setBackground(VISUAL_STATE_ACTIVE,
				BackgroundFactory.createBitmapBackground(active));
	}

	public void setText(String text) {
		this.text = text;
		invalidate();
	}

	public String getText() {
		return text;
	}

	public void setColor(int color) {
		this.color = color;
	}

	protected void onFocus(int direction) {
		super.onFocus(direction);
		color = 0xFF0000;
		this.invalidate();
	}

	protected void onUnfocus() {
		super.onUnfocus();
		color = Color.WHITE;
		this.invalidate();
	}

	protected void paint(Graphics graphics) {
		int fontcontent;
		if (Display.getWidth() > 480)
			fontcontent = 28;
		else if (Display.getWidth() < 481 && Display.getWidth() > 320)
			fontcontent = 23;
		else
			fontcontent = 18;

		graphics.setFont(Font.getDefault().derive(Font.PLAIN, fontcontent));
		graphics.setColor(color);
		graphics.drawText(text, (mNormal.getWidth() - Font.getDefault()
				.getAdvance(text)) / 2, ((mNormal.getHeight() - Font
				.getDefault().getHeight()) / 2) + 10, DrawStyle.HCENTER
				| DrawStyle.VCENTER);
	}

	public int getPreferredWidth() {
		return mWidth;
	}

	public int getPreferredHeight() {
		return mHeight;
	}

	protected void layout(int width, int height) {
		setExtent(mWidth, mHeight);
	}

	protected void drawFocus(Graphics graphics, boolean on) {

	}
}
