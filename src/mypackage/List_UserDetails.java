package mypackage;

public class List_UserDetails 
{
	int _UserID;
	int _FormSubmit;
	int soundon, vibrateon, notificationon, popupon;

	List_UserDetails() {
	}

	List_UserDetails(int FormSubmit, int notificationon, int soundon,
			int vibrateon, int popupon) {
		this._FormSubmit = FormSubmit;
		this.soundon = soundon;
		this.vibrateon = vibrateon;
		this.notificationon = notificationon;
		this.popupon = popupon;
	}

	public void SetID(int NewsID) {
		this._UserID = NewsID;
	}

	public int getFormSubmit() {
		return this._FormSubmit;
	}

	public void setFormSubmit(int FormSubmit) {
		this._FormSubmit = FormSubmit;

	}

	public int getnotification() {
		return this.notificationon;
	}

	public void setnotification(int notificationon) {
		this.notificationon = notificationon;
	}

	public int getvibrate() {
		return this.vibrateon;
	}

	public void setvibrate(int vibrateon) {
		this.vibrateon = vibrateon;
	}

	public int getsound() {
		return this.soundon;
	}

	public void setsound(int soundon) {
		this.soundon = soundon;
	}

	public int getpopup() {
		return this.popupon;
	}

	public void setpopup(int popupon) {
		this.popupon = popupon;
	}
}
