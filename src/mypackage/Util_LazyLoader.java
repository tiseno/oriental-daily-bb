package mypackage;

import java.io.InputStream;

import javax.microedition.io.HttpConnection;

import net.rim.device.api.io.IOUtilities;
import net.rim.device.api.io.transport.ConnectionFactory;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.EncodedImage;

public class Util_LazyLoader implements Runnable {
	String url = null;
	Util_BitmapDowloadListener listener = null;

	public Util_LazyLoader(String url, Util_BitmapDowloadListener listener) {
		this.url = url;
		this.listener = listener;
	}

	public void run() {
		Bitmap bmpImage = getImageFromWeb(url);
		listener.ImageDownloadCompleted(bmpImage);
	}

	private Bitmap getImageFromWeb(String url) {
		HttpConnection connection = null;
		InputStream inputStream = null;
		EncodedImage bitmap;
		byte[] dataArray = null;

		try {
			connection = (HttpConnection) (new ConnectionFactory())
					.getConnection(url + Database_Webservice.ht_params)
					.getConnection();

			int responseCode = connection.getResponseCode();
			if (responseCode == HttpConnection.HTTP_OK) {
				inputStream = connection.openDataInputStream();
				dataArray = IOUtilities.streamToBytes(inputStream);
			}
		} catch (Exception ex) {
		} finally {
			try {
				inputStream.close();
				connection.close();
			} catch (Exception e) {
			}
		}

		if (dataArray != null) {
			bitmap = EncodedImage.createEncodedImage(dataArray, 0,
					dataArray.length);
			return bitmap.getBitmap();
		} else {
			return null;
		}
	}
}
